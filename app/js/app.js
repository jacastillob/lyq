/*!
 * 
 * Angle - Bootstrap Admin App + AngularJS
 * 
 * Version: 3.0.0
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: https://wrapbootstrap.com/help/licenses
 * 
 */

// APP START
// ----------------------------------- 

(function() {
    'use strict';

    angular
        .module('lyqApp', [
            'app.core',
            'app.routes',
            'app.sidebar',
            'app.navsearch',
            'app.preloader',
            'app.translate',
            'app.settings',
            'app.pages',    
            'app.utils'
        ]);
})();


(function() {
    'use strict';

    angular
        .module('app.agenda', []);
})();
(function() {
    'use strict';

    angular
        .module('app.calendar', []);
})();
(function() {
    'use strict';

    angular
        .module('app.colors', []);
})();
(function() {
    'use strict';

    angular
        .module('app.compromiso', []);
})();
(function() {
    'use strict';

    angular
        .module('app.contrato', []);
})();
(function() {
  'use strict';
  angular
    .module('app.core', [
    'ngRoute',
    'ngAnimate',
    'ngStorage',
    'ngCookies',
    'pascalprecht.translate',
    'ui.bootstrap',
    'ui.router',
    'oc.lazyLoad',
    'cfp.loadingBar',
    'ngSanitize',
    'ngResource',
    'tmh.dynamicLocale',
    'ui.utils',
    'app.fileManager',
    'app.routes',  
    'app.security',
    'app.mailbox',  
    'ngImgCrop',    
    'app.setMenu',
	'app.funcionario',
    'app.tercero',
    'app.oportunidad',
    'app.contrato',
    'app.fileManager',
    'app.seguimiento',
    'app.producto',
    'app.calendar',
    'app.agenda',
    'app.consecutivo',
    'app.rol',
    'app.setMenu',   
    'app.factura',
    'app.mensajeria',
    'app.usuario',
    'app.reporte',
    'app.compromiso'
  ]);

})();
(function() {
    'use strict';

    angular
        .module('app.consecutivo', []);
})();
(function() {
    'use strict';

    angular
        .module('app.factura', []);
})();
(function() {
    'use strict';

    angular
        .module('app.fileManager', []);
})();
(function() {
    'use strict';

    angular
        .module('app.funcionario', []);
})();
(function() {
    'use strict';

    angular
        .module('app.loadingbar', []);
})();
(function() {
    'use strict';

    angular
        .module('app.lazyload', []);
})();
(function() {
    'use strict';

    angular
        .module('app.mailbox', []);
})();
(function() {
    'use strict';

    angular
        .module('app.mensajeria', []);
})();
(function() {
    'use strict';

    angular
        .module('app.setMenu', []);
})();
(function() {
    'use strict';

    angular
        .module('app.navsearch', []);
})();
(function() {
    'use strict';

    angular
        .module('app.oportunidad', []);
})();
(function() {
    'use strict';

    angular
        .module('app.pages', []);
})();
(function() {
    'use strict';

    angular
        .module('app.preloader', []);
})();


(function() {
    'use strict';

    angular
        .module('app.producto', []);
})();
(function() {
    'use strict';

    angular
        .module('app.reporte', []);
})();
(function() {
    'use strict';

    angular
        .module('app.rol', []);
})();
(function() {
    'use strict';

    angular
        .module('app.routes', [
            'app.lazyload'
        ]);
})();
(function() {
    'use strict';

    angular
        .module('app.security', []);
})();
(function() {
    'use strict';

    angular
        .module('app.seguimiento', []);
})();
(function() {
    'use strict';

    angular
        .module('app.settings', []);
})();
(function() {
    'use strict';

    angular
        .module('app.sidebar', []);
})();
(function() {
    'use strict';

    angular
        .module('app.tercero', []);
})();
(function() {
    'use strict';

    angular
        .module('app.translate', []);
})();
(function() {
    'use strict';

    angular
        .module('app.usuario', []);
})();
(function() {
    'use strict';

    angular
        .module('app.utils', [
          'app.colors'
          ]);
})();

/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .controller('agendaController', agendaController);

  agendaController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'agendaHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window','$modal','$filter'];


  function agendaController($scope, $rootScope, $state, $modalInstance, agendaHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window, $modal,$filter) {    
      
    $scope.agenda = {      
      model : {        
        id : 0,
        tipoId : 0,
        referenciaId : 0,
        tipoReferencia : '',
        asunto : '',
        descripcion : '',
        fechaInicial: '',
        fechaFinal: '',
        estado: '',          
        fechaAct: '',
        usuarioAct: '',
        fechaReg: '',
        usuarioReg: '',
        responsables : []
      },        
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
          
        },
        open : function($event) {
          $scope.agenda.fechaInicial.isOpen = true;
        }
      },
        horaInicial : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        horaFinal : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.agenda.fechaFinal.isOpen = true;
        }
      },
        
      paginations : {
        maxSize : 3,
        itemsPerPage : 3,
        currentPage : 0,
        totalItems : 0
      },
      filterText : '',
      dataSource : [],
      selectedItems : [],
      data : [],
      noData : true,
      loading : false,
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.agenda.filterText,
          "precision": false
        }];
        $scope.fileselectedItems = $filter('arrayFilter')($scope.agenda.dataSource, paramFilter);
        $scope.agenda.paginations.totalItems = $scope.agenda.selectedItems.length;
        $scope.agenda.paginations.currentPage = 1;
        $scope.agenda.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.agenda.paginations.currentPage == 1 ) ? 0 : ($scope.agenda.paginations.currentPage * $scope.agenda.paginations.itemsPerPage) - $scope.agenda.paginations.itemsPerPage;
        $scope.agenda.data = $scope.agenda.selectedItems.slice(firstItem , $scope.agenda.paginations.currentPage * $scope.agenda.paginations.itemsPerPage);
      },
      refresh : function() {
        var params = {
          idReferencia : $scope.agenda.model.referenciaId,
          tipoReferencia : $scope.agenda.model.tipoReferencia
        };
        $scope.agenda.getData(params);
      },
      setData : function(data) {
        $scope.agenda.selectedItems = data;
        $scope.agenda.dataSource = data;
        $scope.agenda.paginations.totalItems = $scope.agenda.selectedItems.length;
        $scope.agenda.paginations.currentPage = 1;
        $scope.agenda.changePage();
        $scope.agenda.noData = false;
        $scope.agenda.loading = false;
        ($scope.agenda.dataSource.length < 1) ? $scope.agenda.noData = true : null;
      },
      getData : function(params) {
        $scope.agenda.data = [];
        $scope.agenda.loading = true;
        $scope.agenda.noData = false;

        agendaHttp.getAgendas({}, params, function(response) {
          $scope.agenda.setData(response);
        })
      },
      editar : function(id) {
        var params = {
          id : id 
        };
          
        agendaHttp.getAgenda({}, params, function(response) {
            $scope.agenda.model=response;
            
            
             $scope.modoEdicion=true;
               
			if($scope.agenda.model.fechaInicial){
                
                $scope.agenda.fechaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
                $scope.agenda.horaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
            }    
			if($scope.agenda.model.fechaFinal){
                
                $scope.agenda.fechaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
                $scope.agenda.horaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
            }  
            
            $scope.estados.current = $filter('filter')($scope.estados.data, { value : $scope.agenda.model.estado })[0];
            $scope.tipoAgenda.current = $filter('filter')($scope.tipoAgenda.data, { id : $scope.agenda.model.tipoId })[0];
            
        }, function() {
          message.show("error", "Ocurrio un error al intentar cargar la agenda");
          $rootScope.loadingVisible = false;
        }) 
      },
      eliminar : function(id) {
        var params = {
          id : id 
        };

        $rootScope.loadingVisible = true;

        agendaHttp.removeDocumento(params, function(response) {
          $scope.agenda.refresh();
          message.show("info", "Adjunto eliminado correctamente");
          $rootScope.loadingVisible = false;
        }, function() {
          message.show("error", "Ocurrio un error al intentar eliminar el adjunto");
          $rootScope.loadingVisible = false;
        });
      },    
      close : function() {
        $modalInstance.dismiss('cancel');
      }, seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        save : function() {
       
         
            if($scope.agenda.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
            else{$scope.agenda.model.fechaInicial=Date.parse(new Date($scope.agenda.fechaInicial.value));}	

            if($scope.agenda.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
            else{$scope.agenda.model.fechaFinal=Date.parse(new Date($scope.agenda.fechaFinal.value));}	
            
            if($scope.agenda.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
            if($scope.agenda.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
            
            if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
            else{$scope.agenda.model.estado= $scope.estados.current.value;}

            if(!$scope.tipoAgenda.current){message.show("warning", "Tipo agenda requerido");return;}
            else{$scope.agenda.model.tipoId= $scope.tipoAgenda.current.id;}	
            
            if($scope.agenda.model.responsables.length==0){message.show("warning", "Debe especificar un responsable");return;}
            
            
             $scope.agenda.model.referenciaId = parameters.id;
             $scope.agenda.model.tipoReferencia = parameters.referencia;
            
            
            var _http = new agendaHttp($scope.agenda.model);
             if(  $scope.agenda.model.id==0){
                 
                  _http.$addAgenda(function(response){                  
                      message.show("success", "Agenda creada satisfactoriamente!!");           
                      
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editAgenda(function(response){                  
                      message.show("success", "Agenda actualizada satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }
            
            $scope.agenda.cancel();
            $scope.agenda.refresh();
            $scope.estados.current=$scope.estados.data[0]; 
            $scope.tipoAgenda.current=$scope.tipoAgenda.data[0]; 
            
      },
        cancel : function(){
            
             var   model = {        
                id : 0,              
                asunto : '',
                tipoId : 0,
                descripcion : '',
                fechaInicial: '',
                referenciaId : parameters.id,
                tipoReferencia : parameters.referencia,
                fechaFinal: '',
                estado: '',          
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: '',
                responsables : []
            };
            
           $scope.agenda.fechaInicial.value='';
           $scope.agenda.horaInicial.value='';
            
            $scope.agenda.model=model;
            $scope.modoEdicion= false;
            $scope.agenda.refresh();
            
            $scope.estados.current=$scope.estados.data[0]; 
            $scope.tipoAgenda.current=$scope.tipoAgenda.data[0]; 
            
        },
        removeResponsable: function(item){          
                    
            
            for (var i = 0; i < $scope.agenda.model.responsables.length; i++) { 
                
                    if($scope.agenda.model.responsables[i].funcionarioId ==item.funcionarioId){
                        
                        $scope.agenda.model.responsables.splice(i, 1);
                        break;
                    }
                
                }
        }  ,        
        addResponsable: function(){  
          
         if(!$scope.funcionarios.current){message.show("warning", "Resposable requerido");return;}
            

            
            var guarda=0;
            for (var i = 0; i < $scope.agenda.model.responsables.length; i++) { 
                
                    if($scope.agenda.model.responsables[i].funcionarioId ==$scope.funcionarios.current.id){
                        
                      guarda++;
                    }
                
                }
            
            if(guarda==0){
                
                var nuevoResponsable={
                    agendaId:$scope.agenda.model.id,
                    funcionarioId: $scope.funcionarios.current.funcionarioId,
                    nombre: $scope.funcionarios.current.nombre                    
                }
                
                $scope.agenda.model.responsables.push(nuevoResponsable);
                
                
            }else{                
                message.show("warning", "El responsable ya se encuentra asociado en la agenda");
            }
        }
    }
    
      //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'A', descripcion: 'Abierta'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrada'});
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelada'}); 
            $scope.estados.data.push({value:'AN', descripcion: 'Anulada'});  
            
            if(!$scope.agenda.model){$scope.estados.current = $scope.estados.data[0];}
            else{$scope.estados.current = $filter('filter')($scope.estados.data, { id : $scope.agenda.model.estado })[0];}
                        
        }        
    }
    
        
    //TIPO AGENDA
    $scope.tipoAgenda = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        agendaHttp.getTipoAgenda({}, {tipoAgenda:parameters.referencia }, function(response) {
            $scope.tipoAgenda.data = $filter('orderBy')(response, 'codigo');
            if(!$scope.agenda.model){$scope.tipoAgenda.current = $scope.tipoAgenda.data[0];}
            else{$scope.tipoAgenda.current = $filter('filter')($scope.tipoAgenda.data, { id : $scope.agenda.model.tipoId })[0];}
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setTipo : function(){
          $scope.agenda.model.agenda=$scope.tipoAgenda.current.id;
    }}
    
    
     //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            var obj= {
                    idProceso : parameters.id,
                    proceso : parameters.referencia
                }  
            agendaHttp.getResponsablesProceso({}, obj, function(response) {
            
            $scope.funcionarios.data = response;                    
            $scope.funcionarios.current=$scope.funcionarios.data[0];
           

            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }   
    $scope.tipoAgenda.getData();
    $scope.funcionarios.getData();
    $scope.estados.getData();
    $scope.modoEdicion= false;
    $scope.agenda.model.referenciaId = parameters.id;
    $scope.agenda.model.tipoReferencia = parameters.referencia;
      
      
    $scope.estados.current = $filter('filter')($scope.estados.data, { value : $scope.agenda.model.estado })[0];
    $scope.tipoAgenda.current = $filter('filter')($scope.tipoAgenda.data, { id : $scope.agenda.model.tipoId })[0];
      
    //CARGAMOS LOS DATOS
     $scope.hideDelete = (parameters.hideDelete) ? parameters.hideDelete : false;

    $scope.agenda.refresh();

  }

})();
/**=========================================================
 * Module: app.forma30.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .service('agendaHttp', agendaHttp);

  agendaHttp.$inject = ['$resource', 'END_POINT'];


  function agendaHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      getAgendas : {
        method : 'POST',
        isArray : true
      },
      getAgenda : {
        method : 'GET',
        url : END_POINT + '/General.svc/agenda/:id'
      },
      removeAgenda : {
        method : 'DELETE',
        isArray : true,
        url : END_POINT + '/General.svc/agenda/:id'
      },
      addAgenda: {
        method:'POST',
        url : END_POINT + '/General.svc/addAgenda'
      },
      editAgenda: {
        method:'PUT',
        url : END_POINT + '/General.svc/editAgenda'
      },
      removeAgenda: {
        method:'DELETE',
        params : {
            agendaId : '@agendaId'           
          },
        url : END_POINT + '/General.svc/agenda/:agendaId'
      },
     getFuncionarios : {
        method : 'GET',
        isArray : true,
        url : END_POINT + '/General.svc/funcionarioBusqueda'
      },        
        'getTipoAgenda' : {
        'method' : 'GET',
        'isArray' : true,
         'params' : {
            tipoAgenda : '@tipoAgenda'           
          },        
        'url' : END_POINT + '/General.svc/tipoAgenda/:tipoAgenda'
      },
        'getProcesosAgenda' : {
        'method' : 'GET',        
        'isArray' : true,
        'url' : END_POINT + '/General.svc/procesoAgenda'
      } ,'getConsecutivos' : {
        'method' : 'GET',        
        'isArray' : true,
        'url' : END_POINT + '/General.svc/consecutivoBusqueda'
      } , 'getProcesosAsociado': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            proceso : '@proceso'           
          },
          'url' : END_POINT + '/General.svc/procesoAsociado/:proceso'
      }, 
        'getResponsablesProceso': {
          'method' : 'POST',
          'isArray' : true,          
          'url' : END_POINT + '/General.svc/procesoAsociado/responsable'
      },
        'getListaAgendas' : {
        'method': 'POST',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/listaAgenda'
      } 
        
        
    };

    return $resource(END_POINT + '/General.svc/agenda', paramDefault, actions);
  }

})();

/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .controller('agendaFormController', agendaFormController);

  agendaFormController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'agendaHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function agendaFormController($scope, $filter, $state, LDataSource, agendaHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      

     
      var agenda = $state.params.agenda;
      var referenciaId = agenda.referenciaId;
      var tipoReferencia = agenda.tipoReferencia;
 
      
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.agenda = {
		model : {
            id : 0,
            tipoId : 0,
            referenciaId : 0,
            tipoReferencia : '',
            asunto : '',
            descripcion : '',
            fechaInicial: '',
            fechaFinal: '',
            estado: '',          
            fechaAct: '',
            usuarioAct: '',
            fechaReg: '',
            usuarioReg: '',
            responsables : [],
            consecutivos : []	
		}, 
      fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
          
        },
        open : function($event) {
          $scope.agenda.fechaInicial.isOpen = true;
        }
      },
        horaInicial : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        horaFinal : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.agenda.fechaFinal.isOpen = true;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.agenda', params : { filters : {objFiltro:agenda.objFiltro}, data : []} });
        $state.go('app.agenda');
          
      },
      save : function() {		  
       
          if($scope.agenda.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
            else{$scope.agenda.model.fechaInicial=Date.parse(new Date($scope.agenda.fechaInicial.value));}	

            if($scope.agenda.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
            else{$scope.agenda.model.fechaFinal=Date.parse(new Date($scope.agenda.fechaFinal.value));}	
            
            if($scope.agenda.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
            if($scope.agenda.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
            
            if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
            else{$scope.agenda.model.estado= $scope.estados.current.value;}

            if(!$scope.tipoAgenda.current){message.show("warning", "Tipo agenda requerido");return;}
            else{$scope.agenda.model.tipoId= $scope.tipoAgenda.current.id;}	
            
            if($scope.agenda.model.responsables.length==0){message.show("warning", "Debe especificar un responsable");return;}
            
            
             $scope.agenda.model.referenciaId = referenciaId;
             $scope.agenda.model.tipoReferencia = tipoReferencia;
            
            
            var _http = new agendaHttp($scope.agenda.model);
          
             if(  $scope.agenda.model.id==0){
                 
                  _http.$addAgenda(function(response){                  
                      message.show("success", "Agenda creada satisfactoriamente!!"); 
                      $scope.modoCreacion=true;
                      $scope.agenda.model=response;
                      
                      
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editAgenda(function(response){                  
                      message.show("success", "Agenda actualizada satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }
             
        
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
          seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        cancel : function(){
            
             var   model = {        
                id : 0,              
                asunto : '',
                tipoId : 0,
                descripcion : '',
                fechaInicial: '',
                referenciaId : referenciaId,
                tipoReferencia : tipoReferencia,
                fechaFinal: '',
                estado: '',          
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: '',
                responsables : [],
                consecutivos : []
            };
            
           $scope.agenda.fechaInicial.value='';
           $scope.agenda.horaInicial.value='';
            
            $scope.agenda.model=model;
            //$scope.modoEdicion= false;
           
            
            $scope.estados.current=$scope.estados.data[0]; 
            $scope.tipoAgenda.current=$scope.tipoAgenda.data[0]; 
            
        },
        removeResponsable: function(item){          
                    
            
            for (var i = 0; i < $scope.agenda.model.responsables.length; i++) { 
                
                    if($scope.agenda.model.responsables[i].funcionarioId ==item.funcionarioId){
                        
                        $scope.agenda.model.responsables.splice(i, 1);
                        break;
                    }
                
                }
        }  ,        
        addResponsable: function(){  
          
         if(!$scope.funcionarios.current){message.show("warning", "Resposable requerido");return;}
            
            var guarda=0;
            for (var i = 0; i < $scope.agenda.model.responsables.length; i++) { 
                
                    if($scope.agenda.model.responsables[i].funcionarioId ==$scope.funcionarios.current.id){
                        
                      guarda++;
                    }
                
                }
            
            if(guarda==0){
                
                var nuevoResponsable={
                    agendaId:$scope.agenda.model.id,
                    funcionarioId: $scope.funcionarios.current.funcionarioId,
                    nombre: $scope.funcionarios.current.nombre                    
                }
                
                $scope.agenda.model.responsables.push(nuevoResponsable);
                
                
            }else{                
                message.show("warning", "El responsable ya se encuentra asociado en la agenda");
            }
        },          
        removeConsecutivo: function(item){          
                    
            
            for (var i = 0; i < $scope.agenda.model.consecutivos.length; i++) { 
                
                    if($scope.agenda.model.consecutivos[i].consecutivoId ==item.consecutivoId){
                        
                        $scope.agenda.model.consecutivos.splice(i, 1);
                        break;
                    }
                
                }
        }  ,        
        addConsecutivo: function(){  
          
         if(!$scope.consecutivos.current){message.show("warning", "Consecutivo requerido");return;}
           if(!$scope.agenda.model.consecutivos){$scope.agenda.model.consecutivos=[]} 
            var guarda=0;
            for (var i = 0; i < $scope.agenda.model.consecutivos.length; i++) { 
                
                    if($scope.agenda.model.consecutivos[i].consecutivoId ==$scope.consecutivos.current.consecutivoId){
                        
                      guarda++;
                    }
                
                }
            
            if(guarda==0){
                
                var nuevoConsecutivo={
                    agendaId:$scope.agenda.model.id,
                    consecutivoId: $scope.consecutivos.current.consecutivoId,
                    consecutivo: $scope.consecutivos.current.consecutivo                    
                }
                
                $scope.agenda.model.consecutivos.push(nuevoConsecutivo);
                
                
            }else{                
                message.show("warning", "El consecutivo ya se encuentra asociado en la agenda");
            }
        }
          
        
      }
   
         //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'A', descripcion: 'Abierta'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrada'});
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelada'}); 
            $scope.estados.data.push({value:'AN', descripcion: 'Anulada'});  
            
            if(!$scope.agenda.model){$scope.estados.current = $scope.estados.data[0];}
            else{$scope.estados.current = $filter('filter')($scope.estados.data, { id : $scope.agenda.model.estado })[0];}
                        
        }        
    }
    
        
    //TIPO AGENDA
    $scope.tipoAgenda = {
      current : {},
      data:[],
      getData : function() {
            $scope.tipoAgenda.data=agenda.tipoAgenda;          
         
                for (var i = 0; i < $scope.tipoAgenda.data.length; i++) { 

                    if($scope.tipoAgenda.data[i].codigo ==-1){
                        $scope.tipoAgenda.data.splice(i, 1);
                        break;
                    }

                }
      },
      setTipo : function(){
          $scope.agenda.model.agenda=$scope.tipoAgenda.current.id;
    }}
    
    
     //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $scope.funcionarios.data = agenda.participantes;
          
           for (var i = 0; i < $scope.funcionarios.data.length; i++) { 

                    if($scope.funcionarios.data[i].id ==-1){
                        $scope.funcionarios.data.splice(i, 1);
                        break;
                    }

                }
      }
    } 
    
       //FUNCIONARIOS
    $scope.consecutivos = {
      current : {}, data:[],
      getData : function() {
                      
           $rootScope.loadingVisible = true;
            agendaHttp.getConsecutivos({}, {}, function(response) {                    
            $scope.consecutivos.data = response ;                  
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }   
    
    
    $scope.tipoAgenda.getData();
    $scope.funcionarios.getData();
    $scope.estados.getData();
      $scope.consecutivos.getData();
    
	//CARGAMOS LOS DATOS DEL agenda	
	
	if(agenda.id==0){
    
        $scope.estados.current=$scope.estados.data[0]; 
        $scope.tipoAgenda.current = $scope.tipoAgenda.data[0];
        $scope.funcionarios.current= $scope.funcionarios.data[0];        
        $scope.modoCreacion=false;
        
        
      //$scope.objetoagenda.current = $scope.objetoagenda.data[0];
      //$scope.tipoProceso.current= $filter('filter')($scope.tipoProceso.data, {value : agenda.tipo})[0]; 
      //$scope.agenda.model.tipo=agenda.tipo;    
        
        
    }
    else{   
        $scope.modoCreacion=true;
        $rootScope.loadingVisible = true;        
        //$scope.divParticipanteagenda=true;
		agendaHttp.getAgenda({},$state.params.agenda, function (data) { 
            $scope.agenda.model = data;	
            $scope.funcionarios.current= $scope.funcionarios.data[0];

            if($scope.agenda.model.fechaInicial){

                    $scope.agenda.fechaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
                    $scope.agenda.horaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
                }    
                if($scope.agenda.model.fechaFinal){

                    $scope.agenda.fechaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
                    $scope.agenda.horaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
                }  

            $scope.estados.current = $filter('filter')($scope.estados.data, { value : $scope.agenda.model.estado })[0];
            $scope.tipoAgenda.current = $filter('filter')($scope.tipoAgenda.data, { id : $scope.agenda.model.tipoId })[0];   
            
                var obj= {
                    idProceso : data.referenciaId,
                    proceso : data.tipoReferencia
                }  
              
                $rootScope.loadingVisible = true;
                agendaHttp.getResponsablesProceso({}, obj, function(response) {                      
                    
                    
                    if(response){                        
                        
                        agenda.participantes = response;                          
                        $scope.funcionarios.getData();              
                    
                    }
                    else{
                            message.show("warning", "El proceso asociado no tiene responsables posibles");  
                    }
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });  
            
       
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      
    
  }
})();
/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .controller('agendaListController', agendaListController);

  agendaListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'agendaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function agendaListController($scope, $filter, $state, ngDialog, tpl, agendaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {   
	
	
    var objFiltro= $state.params.filters.objFiltro;        
        
     var objF={              

          idTipoAgenda:0,
          fechaInicial:'',
          fechaFinal:'',
          idResponsable:0,
          idReferencia:0,
          tipoReferencia:'',
          estado:''
      }          
          
        
	$scope.ESTADO_ABIERTA = 'A';    
    $scope.ESTADO_CERRADO= 'C';	    
    $scope.ESTADO_APROBADA= 'AP';	       
        
        
    $scope.agendas = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      procesoAsociado:'',
      fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
          
        },
        open : function($event) {
          $scope.agendas.fechaInicial.isOpen = true;
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.agendas.fechaFinal.isOpen = true;
        }
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreagendas:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.agendas.selectedAll = !$scope.agendas.selectedAll; 
        for (var key in $scope.agendas.selectedItems) {
          $scope.agendas.selectedItems[key].check = $scope.agendas.selectedAll;
        }
      },
      add : function() {           
          
          if($scope.procesoAsociado.current.id!=-1)
          {
              var cont = {
                    id : 0,
                    tipoId : 0,
                    referenciaId :$scope.procesoAsociado.current.id ,
                    tipoReferencia : $scope.procesoAgenda.current.proceso,
                    asunto : '',
                    descripcion : '',
                    fechaInicial: '',
                    fechaFinal: '',
                    estado: '',          
                    fechaAct: '',
                    usuarioAct: '',
                    fechaReg: '',
                    usuarioReg: '',
                    responsables : [],
                    tipoAgenda : $scope.tipoAgenda.data,
                    participantes : $scope.responsableProceso.data,
                    consecutivos : []
              };
            
              cont.objFiltro=objF;
            parametersOfState.set({ name : 'app.agenda_add', 
               params : { agenda: cont } 
                });
            $state.go('app.agenda_add'); 
              }
          else            
          {
                 message.show("warning", "Debe seleccionar un proceso asociado");
          }
      },
      edit : function(item) {
          
          item.objFiltro=objF;
          item.tipoAgenda = $scope.tipoAgenda.data;
          item.participantes = [];
          item.consecutivos = [];
            parametersOfState.set({ name : 'app.agenda_edit', params : { agenda: item } });
            $state.go('app.agenda_edit');            
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            agendaHttp.remove({}, { id: id }, function(response) {
                $scope.agendas.getData();
                message.show("success", "agenda eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.agendas.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    agendaHttp.remove({}, { id: id }, function(response) {
                        $scope.agendas.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.agendas.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.agendas.filterText,
          "precision": false
        }];
        $scope.agendas.selectedItems = $filter('arrayFilter')($scope.agendas.dataSource, paramFilter);
        $scope.agendas.paginations.totalItems = $scope.agendas.selectedItems.length;
        $scope.agendas.paginations.currentPage = 1;
        $scope.agendas.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.agendas.paginations.currentPage == 1 ) ? 0 : ($scope.agendas.paginations.currentPage * $scope.agendas.paginations.itemsPerPage) - $scope.agendas.paginations.itemsPerPage;
        $scope.agendas.data = $scope.agendas.selectedItems.slice(firstItem , $scope.agendas.paginations.currentPage * $scope.agendas.paginations.itemsPerPage);
      },
      getData : function() {
          
       
          
            if($scope.agendas.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
				else{objF.fechaInicial=Date.parse(new Date($scope.agendas.fechaInicial.value));}	
          
            if($scope.agendas.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
				else{objF.fechaFinal=Date.parse(new Date($scope.agendas.fechaFinal.value));}	  
          
              
              objF.tipoReferencia=$scope.procesoAgenda.current.proceso;
              objF.idResponsable=$scope.responsableProceso.current.id;
              objF.estado=$scope.estadoAgenda.current.value;
              objF.idReferencia=$scope.procesoAsociado.current.id;              
              objF.idTipoAgenda=$scope.tipoAgenda.current.codigo;
       
          
            $scope.agendas.data = [];
            $scope.agendas.loading = true;
            $scope.agendas.noData = false;             
          
         agendaHttp.getListaAgendas( {},objF,function(response) {
             
              $scope.agendas.selectedItems = response;
              $scope.agendas.dataSource = response;
              for(var i=0; i<$scope.agendas.dataSource.length; i++){
                $scope.agendas.nombreagendas.push({id: i, nombre: $scope.agendas.dataSource[i]});
              }
              $scope.agendas.paginations.totalItems = $scope.agendas.selectedItems.length;
              $scope.agendas.paginations.currentPage = 1;
              $scope.agendas.changePage();
              $scope.agendas.loading = false;
              ($scope.agendas.dataSource.length < 1) ? $scope.agendas.noData = true : null;
              $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
        
    }  
    
      //CARGAMOS PROCESOS
    $scope.procesoAgenda = {
      current : {},
      data:[],
      getData : function() {
        $rootScope.loadingVisible = true;
        agendaHttp.getProcesosAgenda({}, { }, function(response) { 

        $scope.procesoAgenda.data = $filter('orderBy')(response, 'proceso');  
        if(objFiltro){

            $scope.procesoAgenda.current=  $filter('filter')($scope.procesoAgenda.data, {proceso : objFiltro.tipoReferencia})[0]; 
            $scope.procesoAgenda.setProceso();  
        }
        else{
            $scope.procesoAgenda.current =$scope.procesoAgenda.data[0];
            $scope.procesoAgenda.setProceso();  
        }           
        $rootScope.loadingVisible = false;
            
            
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
         'setProceso' : function() {
                 
                  
                  $scope.tipoAgenda.getData();
                  $scope.procesoAsociado.getData();    

                  if($scope.procesoAgenda.current.proceso=='CONTRATO')
                        $scope.agendas.procesoAsociado='Contratos:';   
           
      }
    }
    //CARGAMOS TIPO AGENDAMIENTOS POR PROCESO
    $scope.tipoAgenda = {
        current : {},
        data:[],
        getData : function() {
            var pAgen="";
            
            
            if(objFiltro){                
                pAgen=objFiltro.tipoReferencia;                
            }
            else{                
                pAgen=$scope.procesoAgenda.current.proceso;                
            }            
            
            
            if(pAgen!=""){
                
                $rootScope.loadingVisible = true;
                agendaHttp.getTipoAgenda({}, {tipoAgenda:pAgen }, function(response) {
                    
                    
                    $scope.tipoAgenda.data = $filter('orderBy')(response, 'codigo'); 
                    $scope.tipoAgenda.data.push({codigo:(-1), descripcion:"Todos"});
                    $scope.tipoAgenda.data=$filter('orderBy')($scope.tipoAgenda.data, 'codigo');
                    
                    if(objFiltro){
                
                        $scope.tipoAgenda.current=  $filter('filter')($scope.tipoAgenda.data, {codigo : objFiltro.idTipoAgenda})[0]; 
                        
                    }
                    else{

                        $scope.tipoAgenda.current = $scope.tipoAgenda.data[0];
                    }                     
                    
                    $rootScope.loadingVisible = false;

                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", faild.Message);
                });
            }  
        }
    }
    //ESTADOS
    $scope.estadoAgenda = {
        current : {}, data:[],
        getData : function() {
            $scope.estadoAgenda.data.push({value:'', descripcion: 'Todos'});
            $scope.estadoAgenda.data.push({value:'A', descripcion: 'Abierta'});
            $scope.estadoAgenda.data.push({value:'C', descripcion: 'Cerrada'});
            $scope.estadoAgenda.data.push({value:'CA', descripcion: 'Cancelada'}); 
            $scope.estadoAgenda.data.push({value:'AN', descripcion: 'Anulada'});  
            $scope.estadoAgenda.data.push({value:'AP', descripcion: 'Aprobada'});   
            
            if(objFiltro){            
                $scope.estadoAgenda.current=  $filter('filter')($scope.estadoAgenda.data, {value : objFiltro.estado})[0];
            }
            else{
                
                $scope.estadoAgenda.current = $scope.estadoAgenda.data[0];
            }
                        
        }        
    }  
    
    $scope.procesoAsociado = {
      current : {}, data:[],
      getData : function() {
         
          
          if($scope.tipoAgenda.current){
              
                $rootScope.loadingVisible = true;
                agendaHttp.getProcesosAsociado({}, {proceso:$scope.procesoAgenda.current.proceso}, function(response) {  
                    
                   
                    $scope.procesoAsociado.data = response ;   
                    $scope.procesoAsociado.data.push({id:(-1), descripcion:"Todos",proceso:""});
                    $scope.procesoAsociado.data=$filter('orderBy')($scope.procesoAsociado.data, 'id');  
                    $scope.procesoAsociado.current=$scope.procesoAsociado.data[0];  
                     
                    
                  if(objFiltro){            
                    $scope.procesoAsociado.current=  $filter('filter')($scope.procesoAsociado.data, {id : objFiltro.idReferencia})[0]; 
                    $scope.responsableProceso.getData();
                    }
                    else{

                        $scope.procesoAsociado.current = $scope.procesoAsociado.data[0];
                        $scope.responsableProceso.getData();
                    }
                    
                    
                    
                    
                    $rootScope.loadingVisible = false;
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });
          }
      },
        setProcesoAsociado: function() {             
           $scope.responsableProceso.getData();            
      }
    }
    
    
    //RESPONSABLE
    $scope.responsableProceso = {
      current : {}, data:[],
      getData : function() {
        
          if($scope.procesoAsociado.current){              
              
                var obj= {
                    idProceso : $scope.procesoAsociado.current.id,
                    proceso : $scope.procesoAgenda.current.proceso
                }  
              
                $rootScope.loadingVisible = true;
                agendaHttp.getResponsablesProceso({}, obj, function(response) {                      
                    
                    
                    if(response){
                        
                        $scope.responsableProceso.data = response ;   
                        $scope.responsableProceso.data.push({id:(-1), nombre:"Todos"});
                        $scope.responsableProceso.data=$filter('orderBy')($scope.responsableProceso.data, 'id'); 
                        
                        
                    }
                    else{
                         $scope.responsableProceso.data.push({id:(-1), nombre:"Todos"});                         
                    }
                    
                    if(objFiltro){            
                        $scope.responsableProceso.current=  $filter('filter')($scope.responsableProceso.data, {id : objFiltro.idResponsable})[0]; 

                    }
                    else{

                            $scope.responsableProceso.current = $scope.responsableProceso.data[0];

                        }
                    
                    
                    
                    $rootScope.loadingVisible = false;
                    
                    
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });
          }
      }
    }  
    
    
	//CARGAMOS DATA       
    
    $scope.agendas.procesoAsociado='Contratos:';   
        
    var date = new Date();    
    $scope.agendas.fechaInicial.value=new Date(date.getFullYear(), date.getMonth(), 1);
    $scope.agendas.fechaFinal.value=new Date(date.getFullYear(), date.getMonth() + 1, 0);
        
    $scope.procesoAgenda.getData();  
    $scope.estadoAgenda.getData();  
    
        
        if (objFiltro){          

            if(objFiltro.fechaInicial){$scope.agendas.fechaInicial.value=new Date(parseFloat(objFiltro.fechaInicial));}
            if(objFiltro.fechaFinal){$scope.agendas.fechaFinal.value=new Date(parseFloat(objFiltro.fechaFinal));}  
                setTimeout(function() { $scope.agendas.getData(); }, 1000);
            
        } 
     
 
    }
  
  
  })();
/**=========================================================
 * Module: calendar-ui.js
 * This script handle the calendar demo with draggable 
 * events and events creations
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.calendar')
        .directive('calendar', calendar);

    calendar.$inject = [ '$rootScope','calendarHttp','message'];
    function calendar ($rootScope,calendarHttp,message) {
            
        var directive = {
            link: link,            
            
            restrict: 'EA'        
        };  
        
      
        return directive;        
                

        function link(scope, element) {
          
          if(!$.fn.fullCalendar) return;
          
          // The element that will display the calendar           
            
            
         // loadContrato();
          var calendar = element;
          initExternalEvents(calendar);         

          //var demoEvents = createDemoEvents();   
         //initCalendar(calendar, demoEvents, $rootScope.app.layout.isRTL);
           
            var events = getEvents();          
            initCalendar(calendar, events, $rootScope.app.layout.isRTL);
          
        }   
        
        
        function getEvents() {
            var events;
            message.show("success", "Cargando agendas");	
            calendarHttp.getAgendas({}, {}, function(response) {                
                    
                events=response;
                
                //$rootScope.loadingVisible = false;
                
        }, function(faild) {
            //$rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
            
            return events;
   
        }
        

        
        
        
    }


    // global shared var to know what we are dragging
    var draggingEvent = null;


    /**
     * ExternalEvent object
     * @param jQuery Object elements Set of element as jQuery objects
     */
    function ExternalEvent(elements) {
        
        if (!elements) return;
        
        elements.each(function() {
            var $this = $(this);
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var calendarEventObject = {
                title: $.trim($this.text()) // use the element's text as the event title
            };

            // store the Event Object in the DOM element so we can get to it later
            $this.data('calendarEventObject', calendarEventObject);

            // make the event draggable using jQuery UI
            $this.draggable({
                zIndex: 1070,
                revert: true, // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });

        });
    }

    /**
     * Invoke full calendar plugin and attach behavior
     * @param  jQuery [calElement] The calendar dom element wrapped into jQuery
     * @param  EventObject [events] An object with the event list to load when the calendar displays
     */
    function initCalendar(calElement, events, isRTL) {

        // check to remove elements from the list
        var removeAfterDrop = $('#remove-after-drop');

        calElement.fullCalendar({
            isRTL: isRTL,
            header: {
                left:   'prev,next today',
                center: 'title',
                right:  'month,agendaWeek,agendaDay'
            },
            buttonIcons: { // note the space at the beginning
                prev:    ' fa fa-caret-left',
                next:    ' fa fa-caret-right'
            },
            buttonText: {
                today: 'Hoy',
                month: 'Mes',
                week:  'Semana',
                day:   'Dia'
            },
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar 
            drop: function(date, allDay) { // this function is called when something is dropped
                
                var $this = $(this),
                    // retrieve the dropped element's stored Event Object
                    originalEventObject = $this.data('calendarEventObject');

                // if something went wrong, abort
                if(!originalEventObject) return;

                // clone the object to avoid multiple events with reference to the same object
                var clonedEventObject = $.extend({}, originalEventObject);

                // assign the reported date
                clonedEventObject.start = date;
                clonedEventObject.allDay = allDay;
                clonedEventObject.backgroundColor = $this.css('background-color');
                clonedEventObject.borderColor = $this.css('border-color');

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" 
                // (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                calElement.fullCalendar('renderEvent', clonedEventObject, true);
                
                // if necessary remove the element from the list
                if(removeAfterDrop.is(':checked')) {
                  $this.remove();
                }
            },
            eventDragStart: function (event/*, js, ui*/) {
              draggingEvent = event;
            },
            // This array is the events sources
            events: events
        });
    }

    /**
     * Inits the external events panel
     * @param  jQuery [calElement] The calendar dom element wrapped into jQuery
     */
    function initExternalEvents(calElement){
      // Panel with the external events list
      var externalEvents = $('.external-events');

      // init the external events in the panel
      new ExternalEvent(externalEvents.children('div'));

      // External event color is danger-red by default
      var currColor = '#f6504d';
      // Color selector button
      var eventAddBtn = $('.external-event-add-btn');
      // New external event name input
      var eventNameInput = $('.external-event-name');
      // Color switchers
      var eventColorSelector = $('.external-event-color-selector .circle');

      // Trash events Droparea 
      $('.external-events-trash').droppable({
        accept:       '.fc-event',
        activeClass:  'active',
        hoverClass:   'hovered',
        tolerance:    'touch',
        drop: function(event, ui) {
          
          // You can use this function to send an ajax request
          // to remove the event from the repository
          
          if(draggingEvent) {
            var eid = draggingEvent.id || draggingEvent._id;
            // Remove the event
            calElement.fullCalendar('removeEvents', eid);
            // Remove the dom element
            ui.draggable.remove();
            // clear
            draggingEvent = null;
          }
        }
      });

      eventColorSelector.click(function(e) {
          e.preventDefault();
          var $this = $(this);

          // Save color
          currColor = $this.css('background-color');
          // De-select all and select the current one
          eventColorSelector.removeClass('selected');
          $this.addClass('selected');
      });

      eventAddBtn.click(function(e) {
          e.preventDefault();
          
          // Get event name from input
          var val = eventNameInput.val();
          // Dont allow empty values
          if ($.trim(val) === '') return;
          
          // Create new event element
          var newEvent = $('<div/>').css({
                              'background-color': currColor,
                              'border-color':     currColor,
                              'color':            '#fff'
                          })
                          .html(val);

          // Prepends to the external events list
          externalEvents.prepend(newEvent);
          // Initialize the new event element
          new ExternalEvent(newEvent);
          // Clear input
          eventNameInput.val('');
      });
    }
  
    /**
     * Creates an array of events to display in the first load of the calendar
     * Wrap into this function a request to a source to get via ajax the stored events
     * @return Array The array with the events
     */
    function createDemoEvents() {
      // Date for the calendar events (dummy data)
      var date = new Date();
      var d = date.getDate(),
          m = date.getMonth(),
          y = date.getFullYear();

      return  [
                {
                    title: 'All Day Event',
                    start: new Date(y, m, 1),
                    backgroundColor: '#f56954', //red 
                    borderColor: '#f56954' //red
                },
                {
                    title: 'Long Event',
                    start: new Date(y, m, d - 5),
                    end: new Date(y, m, d - 2),
                    backgroundColor: '#f39c12', //yellow
                    borderColor: '#f39c12' //yellow
                },
                {
                    title: 'Meeting',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false,
                    backgroundColor: '#0073b7', //Blue
                    borderColor: '#0073b7' //Blue
                },
                {
                    title: 'Lunch',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false,
                    backgroundColor: '#00c0ef', //Info (aqua)
                    borderColor: '#00c0ef' //Info (aqua)
                },
                {
                    title: 'Birthday Party',
                    start: new Date(y, m, d + 1, 19, 0),
                    end: new Date(y, m, d + 1, 22, 30),
                    allDay: false,
                    backgroundColor: '#00a65a', //Success (green)
                    borderColor: '#00a65a' //Success (green)
                },
                {
                    title: 'Open Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: '//google.com/',
                    backgroundColor: '#3c8dbc', //Primary (light-blue)
                    borderColor: '#3c8dbc' //Primary (light-blue)
                }
            ];
    }

})();

/**=========================================================
 * Module: app.contrato.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.calendar')
    .service('calendarHttp', calendarHttp);

  calendarHttp.$inject = ['$resource', 'END_POINT'];


  function calendarHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {  
      'getAgendas' : {
        'method' : 'GET',
        'isArray' : true
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/agenda/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/agenda'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Cliente.svc/agenda'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/agenda/:id'
      },
        'getContratoBusqueda' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Cliente.svc/contratoBusqueda'
      }
        
     
    };
    return $resource( END_POINT + '/Cliente.svc/agenda', {}, actions, {}); 
  }

})();
(function() {
    'use strict';

    angular
        .module('app.colors')
        .constant('APP_COLORS', {
          'primary':                '#5d9cec',
          'success':                '#27c24c',
          'info':                   '#23b7e5',
          'warning':                '#ff902b',
          'danger':                 '#f05050',
          'inverse':                '#131e26',
          'green':                  '#37bc9b',
          'pink':                   '#f532e5',
          'purple':                 '#7266ba',
          'dark':                   '#3a3f51',
          'yellow':                 '#fad732',
          'gray-darker':            '#232735',
          'gray-dark':              '#3a3f51',
          'gray':                   '#dde6e9',
          'gray-light':             '#e4eaec',
          'gray-lighter':           '#edf1f2'
        })
        ;
})();
/**=========================================================
 * Module: colors.js
 * Services to retrieve global colors
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.colors')
        .service('Colors', Colors);

    Colors.$inject = ['APP_COLORS'];
    function Colors(APP_COLORS) {
        this.byName = byName;

        ////////////////

        function byName(name) {
          return (APP_COLORS[name] || '#fff');
        }
    }

})();

/**=========================================================
 * Module: app.setcompromiso
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.compromiso')
    .controller('compromisoAddController', compromisoAddController);

  compromisoAddController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'parametersOfState', 'REGULAR_EXPRESION', 'compromisoRolHttp', 'message', '$rootScope', 'parameters', '$modalInstance'];


  function compromisoAddController($scope, $filter, $state, LDataSource, $modal, parametersOfState, REGULAR_EXPRESION, compromisoRolHttp, message, $rootScope, parameters, $modalInstance){
    
    $scope.datosParams= parameters;    
    var datos=$scope.datosParams.detalle;    
    $scope.compromiso= {
        model : {
            nombre: '',
            tipo: '',
            tipoPeriodo: '',            
            orden: 0,
            padreId: 0,                
            subCompromiso: null,
            excel:0,
            word:0,
            pdf:0,
            fisica:0
        },
        add: function(){
            
          if(!$scope.compromiso.model.nombre){message.show("error", "Nombre Requerido..");return;}   
          if($scope.tipoPeriodo.current.value==''){message.show("error", "Período Requerido..");return;}   
          else{$scope.compromiso.model.tipoPeriodo= $scope.tipoPeriodo.current.value;}          
           
          $scope.compromiso.model.word= $scope.word.current.value;   
          $scope.compromiso.model.excel= $scope.excel.current.value;    
          $scope.compromiso.model.pdf= $scope.pdf.current.value;   
          $scope.compromiso.model.fisica= $scope.fisica.current.value;              
              
          $rootScope.loadingVisible = true;
          
          var compromisoHttp = new compromisoRolHttp($scope.compromiso.model);
            compromisoHttp.$newCompromiso (function(response) {
                
                $rootScope.loadingVisible = false;   
                $scope.compromiso.close();

                }, function(faild) {
                 $rootScope.loadingVisible = false;
                 message.show("error", "No se creó el compromiso: " + faild.Message);
          })
        },
        edit: function(){
          
            
          if($scope.tipoPeriodo.current.value==''){message.show("error", "Período Requerido..");return;}   
          else{$scope.compromiso.model.tipoPeriodo= $scope.tipoPeriodo.current.value;}     
            
          $scope.compromiso.model.word= $scope.word.current.value;   
          $scope.compromiso.model.excel= $scope.excel.current.value;    
          $scope.compromiso.model.pdf= $scope.pdf.current.value;   
          $scope.compromiso.model.fisica= $scope.fisica.current.value; 
            
          $rootScope.loadingVisible = true;
            
          var compromisoHttp = new compromisoRolHttp($scope.compromiso.model);
          
          compromisoHttp.$updateCompromiso(function(response) {
            $rootScope.loadingVisible = false;           
            $scope.compromiso.close();

          }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", "No se cargaron los compromisos: " + faild.Message);
          })
        },
        dismiss : function() {
            $modalInstance.dismiss('cancel');
        },
        close : function() {
            $modalInstance.close('close');
        }
    }
    
    $scope.excel = {
        current : {}, data:[],
        getData : function() {
            $scope.excel.data.push({value:0, descripcion: 'No'});
            $scope.excel.data.push({value:1, descripcion: 'Si'});   
        }        
    }
    
    $scope.word = {
        current : {}, data:[],
        getData : function() {
            $scope.word.data.push({value:0, descripcion: 'No'});
            $scope.word.data.push({value:1, descripcion: 'Si'});   
        }        
    }
    
    $scope.pdf = {
        current : {}, data:[],
        getData : function() {
            $scope.pdf.data.push({value:0, descripcion: 'No'});
            $scope.pdf.data.push({value:1, descripcion: 'Si'});
        }        
    }
    $scope.fisica = {
        current : {}, data:[],
        getData : function() {
            $scope.fisica.data.push({value:0, descripcion: 'No'});
            $scope.fisica.data.push({value:1, descripcion: 'Si'});
        }        
    }
     $scope.tipoPeriodo = {
        current : {}, data:[],
        getData : function() {
            $scope.tipoPeriodo.data.push({value:'', descripcion: 'Seleccione'});
            $scope.tipoPeriodo.data.push({value:'NA', descripcion: 'N/A'});  
            $scope.tipoPeriodo.data.push({value:'M', descripcion: 'Mensual'});
            $scope.tipoPeriodo.data.push({value:'T', descripcion: 'Trimestral'});
            $scope.tipoPeriodo.data.push({value:'S', descripcion: 'Semestral'});
            $scope.tipoPeriodo.data.push({value:'A', descripcion: 'Anual'});
        }        
    }
    
    $scope.excel.getData();
    $scope.word.getData();
    $scope.pdf.getData();
    $scope.fisica.getData();
    $scope.tipoPeriodo.getData();
    
    $scope.compromiso.model=datos;
      
    $scope.tipoPeriodo.current = $filter('filter')($scope.tipoPeriodo.data, {value : $scope.compromiso.model.tipoPeriodo})[0];
    $scope.excel.current = $filter('filter')($scope.excel.data, {value : $scope.compromiso.model.excel})[0];
    $scope.word.current = $filter('filter')($scope.word.data, {value : $scope.compromiso.model.word})[0];
    $scope.pdf.current = $filter('filter')($scope.pdf.data, {value : $scope.compromiso.model.pdf})[0];
    $scope.fisica.current = $filter('filter')($scope.fisica.data, {value : $scope.compromiso.model.fisica})[0];
          
  }
})();
/**=========================================================
 * Module: app.setcompromiso
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.compromiso')
    .controller('compromisoRolController', compromisoRolController);

  compromisoRolController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'parametersOfState', 'REGULAR_EXPRESION', 'sidebarHttp', 'UsuarioHttp'];


  function compromisoRolController($scope, $filter, $state, LDataSource, $modal, parametersOfState, REGULAR_EXPRESION, sidebarHttp, UsuarioHttp){
    
    $scope.compromiso= {
        current : {},
        data:[],
        getData: function(){
            sidebarHttp.getcompromiso({},{},function(response) {
                $scope.compromiso.data = $filter('orderBy')(response, 'compromisoId');
                for(var i = 0; i < $scope.compromiso.data.length; i++){
                    $scope.compromiso.data[i].subcompromiso = $filter('orderBy')($scope.compromiso.data[i].subcompromiso, 'compromisoId');
                }
            }, function(faild) {
                $scope.compromiso.data = [];
                message.show("error", "No se cargaron los compromisos: " + faild.Message);
            })
        }, edit: function (item){
            var parameter = {id:0, nombreOperadora:$scope.operadora.model.nombre, empresaId:$scope.operadora.model.id};
            var modalInstance = $modal.open({
                templateUrl: 'app/views/compromiso/compromisoRol_edit.html',
                controller: 'compromisoRolController',
                size: 'lg',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {}, function (parameters) {$scope.compromiso.getData();});
        }
    }
    $scope.roles= {
        current : {},
        data:[],
        getData: function(){
            UsuarioHttp.consultarPerfilesAll({}, {}, function(response) {
                $scope.roles.data = response;
            }, function(faild) {
                message.show("error", "No se cargaron los perfiles : " + faild.Message);
            });
        }
    }
    
    $scope.compromiso.getData();
    $scope.roles.getData();
  }
})();
/**=========================================================
 * Module: app.compromiso.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.compromiso')
    .service('compromisoRolHttp', compromisoRolHttp);

  compromisoRolHttp.$inject = ['$resource', 'END_POINT'];


  function compromisoRolHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getListCompromiso' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : {
            tipo : '@tipo'           
          },
        'url' : END_POINT + '/General.svc/compromisos/:tipo'
      },
      'getCompromisoRol' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/compromiso/:id/rol'
      },
      'getRol' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/rol'
      },
      'updateCompromiso' : {///
        'method' : 'PUT',
        'url' : END_POINT + '/General.svc/compromiso'
      },
      'removeCompromiso':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/compromiso/:id'
      },
      'newCompromiso':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/compromiso/'
      },
      'deleteCompromiso': {
          'method':'DELETE',
          'isArray' : true,
          'params' : {
            compromisoId : '@compromisoId'
          },
          'url' : END_POINT + '/General.svc/compromiso/:compromisoId'
      },
      'deleteRol': {
          'method':'DELETE',
          'params' : {
            rolId : '@rolId'
          },
          'url' : END_POINT + '/General.svc/rol/:rolId'
      },
      'assignRol':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/compromiso/rol'
      },
      'unassignRol':   {
        'method':'PUT',
        'url' : END_POINT + '/General.svc/compromiso/rol'
      }
    };
    return $resource( END_POINT + '/General.svc/compromiso', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.setcompromiso
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.compromiso')
    .controller('compromisoController', compromisoController);

  compromisoController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'tpl', 'ngDialog', 'parametersOfState', 'REGULAR_EXPRESION', 'compromisoRolHttp', 'message', '$rootScope'];


  function compromisoController($scope, $filter, $state, LDataSource, $modal, tpl, ngDialog, parametersOfState, REGULAR_EXPRESION, compromisoRolHttp, message, $rootScope){
    $scope.nombre='';
    $scope.compromisoId=0;
    $scope.nombreCompromiso='';
    $scope.compromiso= {
        current : {
          nombre:'',
          compromisoId:0
        },
        data:[],
        getData: function(){
            $scope.nombre='';
            $scope.compromisoId=0;
            $scope.roles.data=[];
            $rootScope.loadingVisible = true;            
           
            var parametros= {
                "tipo":$scope.tipo.current.value                
            };          
            compromisoRolHttp.getListCompromiso({},parametros,function(response) {                
                
                $scope.compromiso.data = $filter('orderBy')(response, 'compromisoId');               
                for(var i = 0; i < $scope.compromiso.data.length; i++){
                    
                    $scope.compromiso.data[i].subCompromiso = $filter('orderBy')($scope.compromiso.data[i].subCompromiso, 'compromisoId');
                    $scope.compromiso.data[i].nombre= $scope.compromiso.data[i].nombre;
                    $scope.compromiso.data[i].show=false;
                    
                    if($scope.compromiso.data[i].subCompromiso!=null){
                        for(var j=0; j<$scope.compromiso.data[i].subCompromiso.length; j++){                            
                            $scope.compromiso.data[i].subCompromiso[j].show=false;
                            if($scope.compromiso.data[i].subCompromiso[j].subCompromiso!=null){
                                $scope.compromiso.data[i].subCompromiso[j].subCompromiso = $filter('orderBy')($scope.compromiso.data[i].subCompromiso[j].subCompromiso, 'compromisoId');
                                
                                for(var k=0; k<$scope.compromiso.data[i].subCompromiso[j].subCompromiso.length; k++){
                                    $scope.compromiso.data[i].subCompromiso[j].subCompromiso[k].show=false;
                                }
                            }
                        }
                    }
                    
                }
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $scope.compromiso.data = [];
                $rootScope.loadingVisible = false;
                message.show("error", "No se cargaron los compromisos: " + faild.Message);
            })
        }, edit: function (item){
            var parametros={
              detalle:{},
              edit:true,
              nombre:'Menú'
            };
            parametros.detalle=item;
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/compromiso/compromiso_add.html',
                controller: 'compromisoAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {}, function (parameters) {});
        }, addsubCompromiso: function (item){
            var parametros={
              detalle:{
                nombre: '',
                tipo: $scope.tipo.current.value,
                tipoPeriodo: '',            
                orden: 0,
                padreId: item.compromisoId,                
                subCompromiso: null,
                excel:0,
                word:0,
                pdf:0
              },
              edit:false,
              nombre:'Submenú'
            };
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/compromiso/compromiso_add.html',
                controller: 'compromisoAddController',
                size: 'md',
                resolve: {
                 parameters: function () { return parameter; }                 
                }
            });
            modalInstance.result.then(function (parameters) {$scope.compromiso.getData();}, function (parameters) {$scope.compromiso.getData();});
            
        }, addCompromiso: function (){
            var parametros={
              detalle:{
                nombre: '',
                tipo: $scope.tipo.current.value,
                tipoPeriodo: '',            
                orden: 0,
                padreId: 0,                
                subCompromiso: null,
                excel:0,
                word:0,
                pdf:0
              },
              edit:false,
              nombre:'Menú',
            };
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/compromiso/compromiso_add.html',
                controller: 'compromisoAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }                  
                }
            });
            modalInstance.result.then(function (parameters) {$scope.compromiso.getData();}, function (parameters) {$scope.compromiso.getData();});
            
        }, getRol: function(item){
            $scope.nombre=item.nombre;
            $scope.compromisoId=item.compromisoId;
            $scope.roles.getData();
            $scope.nombreCompromiso=item.nombre;
        }, remove: function(item){
            ngDialog.openConfirm({
                template: tpl.path,
                className: 'ngdialog-theme-default',
                scope: $scope
            }).then(function (value) {              
                
                var id = item.compromisoId;               
                compromisoRolHttp.removeCompromiso({}, { id: id }, function(response) {
                    $scope.compromiso.getData();                    
                }, function(faild) {
                      $rootScope.loadingVisible = false;
                      message.show("error", "No se eliminó el Compromiso: " + faild.Message);
                });
                
                
            });
        }
    }
    $scope.roles= {
        current : {},
        data:[],
        compromisoId: 0,
        getData: function(){
            $rootScope.loadingVisible = true;
            compromisoRolHttp.getCompromisoRol({},{ id : $scope.compromisoId}, function (response) {
                $scope.roles.data = response;
                $scope.roles.compromisoId = $scope.compromisoId;
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se cargaron los perfiles : " + faild.Message);
            });
        },
        checkRol: function(item){
            var _Http = new compromisoRolHttp(item);
            $rootScope.loadingVisible = true;
            if(item.seleccionado==true){
              $rootScope.loadingVisible = true;
              _Http.$assignRol(function(response) {
                $rootScope.loadingVisible = false;
              }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se Asignó el Rol: " + faild.Message);
              })
            }
            else{
              _Http.$unassignRol(function(response) {
                $rootScope.loadingVisible = false;
              }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se Desasignó el Rol: " + faild.Message);
              })
            }
        }
    }
     //CARGAMOS PROCESOS
    $scope.tipo = {
        current : {}, data:[],
        getData : function() {            
            $scope.tipo.data.push({value:'FI', descripcion: 'Financiera'});
            $scope.tipo.data.push({value:'LE', descripcion: 'Legal'});
            $scope.tipo.data.push({value:'AD', descripcion: 'Administrativa'});       
            $scope.tipo.data.push({value:'SI', descripcion: 'Sistemas'});       
            $scope.tipo.current =$scope.tipo.data[0];
        }        
    }	   
    $scope.tipo.getData();      
    $scope.compromiso.getData();
  }
})();
/**=========================================================
 * Module: app.setcompromiso
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('contratoCompromisoAddController', contratoCompromisoAddController);

  contratoCompromisoAddController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'parametersOfState', 'REGULAR_EXPRESION', 'contratoHttp', 'message', '$rootScope', 'parameters', '$modalInstance'];


  function contratoCompromisoAddController($scope, $filter, $state, LDataSource, $modal, parametersOfState, REGULAR_EXPRESION, contratoHttp, message, $rootScope, parameters, $modalInstance){
    
    $scope.datosParams= parameters;    
    var datos=$scope.datosParams.detalle;    
    $scope.compromiso= {
        model : {
            nombre: '',
            tipo: '',
            tipoPeriodo: '',            
            orden: 0,
            padreId: 0,                
            subCompromiso: null,
            excel:0,
            word:0,
            pdf:0,
            fisica:0
        },
        add: function(){
            
          if(!$scope.compromiso.model.nombre){message.show("error", "Nombre Requerido..");return;}   
          if($scope.tipoPeriodo.current.value==''){message.show("error", "Período Requerido..");return;}   
          else{$scope.compromiso.model.tipoPeriodo= $scope.tipoPeriodo.current.value;}          
           
          $scope.compromiso.model.word= $scope.word.current.value;   
          $scope.compromiso.model.excel= $scope.excel.current.value;    
          $scope.compromiso.model.pdf= $scope.pdf.current.value;   
          $scope.compromiso.model.fisica= $scope.fisica.current.value;              
              
          $rootScope.loadingVisible = true;
          
        contratoHttp.newCompromiso({},$scope.compromiso.model, function (data) { 
            
                $rootScope.loadingVisible = false;   
                $scope.compromiso.close();

                }, function(faild) {
                 $rootScope.loadingVisible = false;
                 message.show("error", "No se creó el compromiso: " + faild.Message);
          })
        },
        edit: function(){
          
            
          if($scope.tipoPeriodo.current.value==''){message.show("error", "Período Requerido..");return;}   
          else{$scope.compromiso.model.tipoPeriodo= $scope.tipoPeriodo.current.value;}     
            
          $scope.compromiso.model.word= $scope.word.current.value;   
          $scope.compromiso.model.excel= $scope.excel.current.value;    
          $scope.compromiso.model.pdf= $scope.pdf.current.value;   
          $scope.compromiso.model.fisica= $scope.fisica.current.value;             
          $rootScope.loadingVisible = true;
            
          
        contratoHttp.updateCompromiso({},$scope.compromiso.model, function (data) { 
            
            $rootScope.loadingVisible = false;           
            $scope.compromiso.close();

          }, function(faild) {
            
            $rootScope.loadingVisible = false;
            message.show("error", "No se actualizo el compromiso: " + faild.Message);
            
          })
        },
        dismiss : function() {
            $modalInstance.dismiss('cancel');
        },
        close : function() {
            $modalInstance.close('close');
        }
    }
    
    $scope.excel = {
        current : {}, data:[],
        getData : function() {
            $scope.excel.data.push({value:0, descripcion: 'No'});
            $scope.excel.data.push({value:1, descripcion: 'Si'});   
        }        
    }
    
    $scope.word = {
        current : {}, data:[],
        getData : function() {
            $scope.word.data.push({value:0, descripcion: 'No'});
            $scope.word.data.push({value:1, descripcion: 'Si'});   
        }        
    }
    
    $scope.pdf = {
        current : {}, data:[],
        getData : function() {
            $scope.pdf.data.push({value:0, descripcion: 'No'});
            $scope.pdf.data.push({value:1, descripcion: 'Si'});
        }        
    }
    $scope.fisica = {
        current : {}, data:[],
        getData : function() {
            $scope.fisica.data.push({value:0, descripcion: 'No'});
            $scope.fisica.data.push({value:1, descripcion: 'Si'});
        }        
    }
     $scope.tipoPeriodo = {
        current : {}, data:[],
        getData : function() {
            $scope.tipoPeriodo.data.push({value:'', descripcion: 'Seleccione'});
            $scope.tipoPeriodo.data.push({value:'NA', descripcion: 'N/A'});  
            $scope.tipoPeriodo.data.push({value:'M', descripcion: 'Mensual'});
            $scope.tipoPeriodo.data.push({value:'T', descripcion: 'Trimestral'});
            $scope.tipoPeriodo.data.push({value:'S', descripcion: 'Semestral'});
            $scope.tipoPeriodo.data.push({value:'A', descripcion: 'Anual'});
        }        
    }
    
    $scope.excel.getData();
    $scope.word.getData();
    $scope.pdf.getData();
    $scope.fisica.getData();
    $scope.tipoPeriodo.getData();
    
    $scope.compromiso.model=datos;
      
    $scope.tipoPeriodo.current = $filter('filter')($scope.tipoPeriodo.data, {value : $scope.compromiso.model.tipoPeriodo})[0];
    $scope.excel.current = $filter('filter')($scope.excel.data, {value : $scope.compromiso.model.excel})[0];
    $scope.word.current = $filter('filter')($scope.word.data, {value : $scope.compromiso.model.word})[0];
    $scope.pdf.current = $filter('filter')($scope.pdf.data, {value : $scope.compromiso.model.pdf})[0];
    $scope.fisica.current = $filter('filter')($scope.fisica.data, {value : $scope.compromiso.model.fisica})[0];
          
  }
})();
/**=========================================================
 * Module: app.setcompromiso
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('contratoCompromisoParticipanteController', contratoCompromisoParticipanteController);

  contratoCompromisoParticipanteController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'parametersOfState', 'REGULAR_EXPRESION', 'sidebarHttp', 'UsuarioHttp'];


  function contratoCompromisoParticipanteController($scope, $filter, $state, LDataSource, $modal, parametersOfState, REGULAR_EXPRESION, sidebarHttp, UsuarioHttp){
    
    $scope.compromiso= {
        current : {},
        data:[],
        getData: function(){
            sidebarHttp.getcompromiso({},{},function(response) {
                $scope.compromiso.data = $filter('orderBy')(response, 'compromisoId');
                for(var i = 0; i < $scope.compromiso.data.length; i++){
                    $scope.compromiso.data[i].subcompromiso = $filter('orderBy')($scope.compromiso.data[i].subcompromiso, 'compromisoId');
                }
            }, function(faild) {
                $scope.compromiso.data = [];
                message.show("error", "No se cargaron los compromisos: " + faild.Message);
            })
        }, edit: function (item){
            var parameter = {id:0, nombreOperadora:$scope.operadora.model.nombre, empresaId:$scope.operadora.model.id};
            var modalInstance = $modal.open({
                templateUrl: 'app/views/contrato/compromisoRol_edit.html',
                controller: 'contratoCompromisoParticipanteController',
                size: 'lg',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {}, function (parameters) {$scope.compromiso.getData();});
        }
    }
    $scope.participantes= {
        current : {},
        data:[],
        getData: function(){
            UsuarioHttp.consultarPerfilesAll({}, {}, function(response) {
                $scope.participantes.data = response;
            }, function(faild) {
                message.show("error", "No se cargaron los perfiles : " + faild.Message);
            });
        }
    }
    
    $scope.compromiso.getData();
    $scope.roles.getData();
  }
})();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('contratoController', contratoController);

  contratoController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'contratoHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function contratoController($scope, $filter, $state, LDataSource, contratoHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {      
     
      var contrato = $state.params.contrato;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.contrato = {
		model : {
              id: 0,
              terceroId: 0,
              tipoContratoId: 0,            
              objetoId: 0,
              descripcion: '',
              fechaInicial: '',
              horaInicial: '',
              fechaFinal: '',
              estado: '',
              consecutivo: '',
              fechaAct: '',
              usuarioAct: '',
              fechaReg: '',
              usuarioReg: '',                        
              longitude: 0,
              latitude: 0
		}, 
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.contrato.fechaInicial.isOpen = true;
        }
      },  
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.contrato.fechaFinal.isOpen = true;
        }
      },
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.contrato.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.contrato.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.contrato', params : { filters : {procesoSeleccionado:contrato.tipo}, data : []} });
        $state.go('app.contrato');
          
      },
      save : function() {	
          
          if($scope.contrato.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
				else{$scope.contrato.model.fechaInicial=Date.parse(new Date($scope.contrato.fechaInicial.value));}	
          
             if($scope.contrato.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
				else{$scope.contrato.model.fechaFinal=Date.parse(new Date($scope.contrato.fechaFinal.value));}	
          	
          
                if(!$scope.tercerosCon.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.contrato.model.terceroId = $scope.tercerosCon.current.id;}
          
          		if($scope.contrato.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
				if($scope.contrato.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}				
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.contrato.model.estado= $scope.estados.current.value;}
				
				if(!$scope.tipoContrato.current){message.show("warning", "Tipo contrato requerido");return;}
				else{$scope.contrato.model.tipoContratoId= $scope.tipoContrato.current.codigo;}	
          
                if(!$scope.objetoContrato.current){message.show("warning", "Objeto contrato requerido");return;}
				else{$scope.contrato.model.objetoId= $scope.objetoContrato.current.codigo;}	
          
          
			//INSERTAR
            if($scope.contrato.model.id==0){
				
				$rootScope.loadingVisible = true;
				contratoHttp.save({}, $scope.contrato.model, function (data) { 
					
						contratoHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.contrato.model=data;
							contrato.id=$scope.contrato.model.id; 
							if($scope.contrato.model.fechaReg){$scope.contrato.fechaReg.value=new Date(parseFloat($scope.contrato.model.fechaReg));}    
							if($scope.contrato.model.fechaAct){$scope.contrato.fechaAct.value=new Date(parseFloat($scope.contrato.model.fechaAct));} 
                            
                            if($scope.contrato.model.fechaInicial){$scope.contrato.fechaInicial.value=new Date(parseFloat($scope.contrato.model.fechaInicial));}    
							if($scope.contrato.model.fechaFinal){$scope.contrato.fechaFinal.value=new Date(parseFloat($scope.contrato.model.fechaFinal));} 	
                            
								
							message.show("success", "contrato creada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.contrato.model.id){
					$state.go('app.contrato');
				}else{
					
					contratoHttp.update({}, $scope.contrato.model, function (data) {                           
					  $rootScope.loadingVisible = false;
					       $scope.contrato.model=data;
						if($scope.contrato.model.fechaReg){$scope.contrato.fechaReg.value=new Date(parseFloat($scope.contrato.model.fechaReg));}    
						if($scope.contrato.model.fechaAct){$scope.contrato.fechaAct.value=new Date(parseFloat($scope.contrato.model.fechaAct));}   		
                        
                        if($scope.contrato.model.fechaInicial){$scope.contrato.fechaInicial.value=new Date(parseFloat($scope.contrato.model.fechaInicial));}    
                        if($scope.contrato.model.fechaFinal){$scope.contrato.fechaFinal.value=new Date(parseFloat($scope.contrato.model.fechaFinal));} 	
                        
                            
					   message.show("success", "contrato actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.contrato.model.id,
                         referencia : 'CONTRATO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
          seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.contrato.model.id,
                         referencia : 'CONTRATO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.contrato.model.id,
                         referencia : 'CONTRATO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      } 
      }
    //COMPROMISOS
     $scope.compromiso= {
        current : {
          nombre:'',
          compromisoId:0
        },
        data:[],
        getData: function(){             
            $scope.nombre='';
            $scope.compromisoId=0;
            $scope.participantes.data=[];
            
            $rootScope.loadingVisible = true;   
            
           
            var parametros= {
                "tipo":$scope.tipoCompromiso.current.value,
                "contrato":$scope.contrato.model.id
            };    
            
            if($scope.tipoCompromiso.current.value!=""){
            contratoHttp.getListCompromiso({},parametros,function(response) {      
                
                $scope.nombreCompromiso="";                
                $scope.compromiso.data = $filter('orderBy')(response, 'id');               
                for(var i = 0; i < $scope.compromiso.data.length; i++){

                $scope.compromiso.data[i].subCompromiso = $filter('orderBy')($scope.compromiso.data[i].subCompromiso, 'compromisoId');
                $scope.compromiso.data[i].nombre= $scope.compromiso.data[i].nombre;
                $scope.compromiso.data[i].show=false;

                    if($scope.compromiso.data[i].subCompromiso!=null){
                        for(var j=0; j<$scope.compromiso.data[i].subCompromiso.length; j++){                            
                            $scope.compromiso.data[i].subCompromiso[j].show=false;
                            if($scope.compromiso.data[i].subCompromiso[j].subCompromiso!=null){
                                $scope.compromiso.data[i].subCompromiso[j].subCompromiso = $filter('orderBy')($scope.compromiso.data[i].subCompromiso[j].subCompromiso, 'compromisoId');

                                for(var k=0; k<$scope.compromiso.data[i].subCompromiso[j].subCompromiso.length; k++){
                                    $scope.compromiso.data[i].subCompromiso[j].subCompromiso[k].show=false;
                                }
                            }
                        }
                    }

                }
                $rootScope.loadingVisible = false;
                }, function(faild) {
                $scope.compromiso.data = [];
                $rootScope.loadingVisible = false;
                message.show("error", "No se cargaron los compromisos: " + faild.Message);
                })
            }       
            else{
                $scope.compromiso.data = [];
                $rootScope.loadingVisible = false;
                $scope.nombreCompromiso="";
            }
        }, edit: function (item){
            var parametros={
              detalle:{},
              edit:true,
              nombre:'Menú'
            };
            parametros.detalle=item;
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/contrato/compromiso_add.html',
                controller: 'contratoCompromisoAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {}, function (parameters) {});
        }, addsubCompromiso: function (item){           
            
            var parametros={
              detalle:{
                nombre: '',
                tipo: $scope.tipoCompromiso.current.value,
                tipoPeriodo: '',            
                orden: 0,
                padreId: item.id,                
                subCompromiso: null,
                contratoId:$scope.contrato.model.id,
                compromisoId:item.compromisoId,
                excel:0,
                word:0,
                pdf:0
              },
              edit:false,
              nombre:'Submenú'
            };
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/contrato/compromiso_add.html',
                controller: 'contratoCompromisoAddController',
                size: 'md',
                resolve: {
                 parameters: function () { return parameter; }                 
                }
            });
            modalInstance.result.then(function (parameters) {$scope.compromiso.getData();}, function (parameters) {$scope.compromiso.getData();});
            
        }, addCompromiso: function (){
            
            var parametros={
              detalle:{
                nombre: '',
                tipo: $scope.tipoCompromiso.current.value,
                tipoPeriodo: '',            
                orden: 0,
                padreId: 0,                
                subCompromiso: null,
                contratoId:$scope.contrato.model.id,
                compromisoId:0,
                excel:0,
                word:0,
                pdf:0
              },
              edit:false,
              nombre:'Menú',
            };
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/contrato/compromiso_add.html',
                controller: 'contratoCompromisoAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }                  
                }
            });
            modalInstance.result.then(function (parameters) {$scope.compromiso.getData();}, function (parameters) {$scope.compromiso.getData();});
            
        }, getParticipante: function(item){
            $scope.nombre=item.nombre;
            $scope.compromisoId=item.compromisoId;            
            $scope.id=item.id;
            $scope.participantes.getData();
            $scope.nombreCompromiso=item.nombre;
        }, remove: function(item){
            ngDialog.openConfirm({
                template: tpl.path,
                className: 'ngdialog-theme-default',
                scope: $scope
            }).then(function (value) {              
                
                var id = item.id;               
                contratoHttp.removeCompromiso({}, { id: id }, function(response) {
                    $scope.compromiso.getData();                    
                }, function(faild) {
                      $rootScope.loadingVisible = false;
                      message.show("error", "No se eliminó el Compromiso: " + faild.Message);
                });
            });
        },
         save:function (){
             
            ngDialog.openConfirm({
                template: tpl.ok,
                className: 'ngdialog-theme-default',
                scope: $scope
            }).then(function (value) {                    
                    contratoHttp.saveListCompromiso({},{tipo:$scope.tipoCompromiso.current.value,contrato:$scope.contrato.model.id}, function() {
                        message.show("success", "Compromisos actualizados exitosamente");
                        $scope.compromiso.getData();
                    
                }, function(faild) {
                    message.show("error", faild.Message);
                });
                
            });
         }
    }
    //COMPROMISOS PARTICIPANTES
       $scope.participantes= {
        current : {},
        data:[],
        compromisoId: 0,
        id: 0,
        getData: function(){
            
            if($scope.id!=0){
                $rootScope.loadingVisible = true;
                contratoHttp.getCompromisoParticipante({},{ contrato:$scope.contrato.model.id,compromiso : $scope.compromisoId,id : $scope.id}, function (response) {
                    $scope.participantes.data = response;
                    $scope.participantes.compromisoId = $scope.compromisoId;
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "No se cargaron los participantes : " + faild.Message);
                });
            }
            else{
                message.show("warning", "Debe guardar la lista de compromisos para continuar!!");
            }
            
        },
        checkParticipante: function(item){
            var _Http = new contratoHttp(item);
            $rootScope.loadingVisible = true;
            if(item.seleccionado==true){
              $rootScope.loadingVisible = true;
              _Http.$assignParticipante(function(response) {
                $rootScope.loadingVisible = false;
              }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se Asignó el participante: " + faild.Message);
              })
            }
            else{
              _Http.$unassignParticipante(function(response) {
                $rootScope.loadingVisible = false;
              }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se Desasignó el participante: " + faild.Message);
              })
            }
        }
    }
    //TIPO CONTRATO
    $scope.tipoContrato = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        contratoHttp.getTipoContrato({}, {}, function(response) {
            $scope.tipoContrato.data = $filter('orderBy')(response, 'codigo');
            if(!$scope.contrato.model){$scope.tipoContrato.current = $scope.tipoContrato.data[0];}
            else{$scope.tipoContrato.current = $filter('filter')($scope.tipoContrato.data, { codigo : $scope.contrato.model.tipoContratoId })[0];}
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setTipo : function(){
          $scope.contrato.model.tipoContratoId=$scope.tipoContrato.current.codigo;
    }}
    //OBJETO CONTRATO
    $scope.objetoContrato = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        contratoHttp.getObjetoContrato({}, {}, function(response) {
            $scope.objetoContrato.data = $filter('orderBy')(response, 'codigo');
            if(!$scope.contrato.model){$scope.objetoContrato.current = $scope.objetoContrato.data[0];}
            else{$scope.objetoContrato.current = $filter('filter')($scope.objetoContrato.data, { codigo : $scope.contrato.model.objetoId })[0];}
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setObjeto : function(){
          $scope.contrato.model.objetoId=$scope.objetoContrato.current.codigo;
    }}
    //PARTICIPANTES CONTRATO
    $scope.participanteContrato = {
    model : {  
            id: 0,
            funcionarioId: 0,
            contratoId: 0,
            cargoId: 0,
            funcionario: '',
            cargo: ''		
		},
      current : {},
      data:[],
      getData : function() {
          if(contrato){
              
            if($scope.contrato.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              contratoHttp.getParticipantesContrato({}, { contratoId: $scope.contrato.model.id }, function(response) {
                  $scope.participanteContrato.data = $filter('orderBy')(response, 'nombre');
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },
       add : function() {    
           
        var parameter = {
              participanteContrato : {              
                        id: 0,
                        funcionarioId: 0,
                        cargoId: 0,
                        contratoId: $scope.contrato.model.id,
                        funcionario: '',
                        cargo: ''
              }            
        };

        var modalInstance = $modal.open({
          templateUrl: 'app/views/contrato/participanteContrato_edit.html',
          controller: 'participanteContratoEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
        modalInstance.result.then(function (parameters) {
             $scope.participanteContrato.getData();
        });
           
      },
      delete : function(item){
          
          $rootScope.loadingVisible = true;          
          contratoHttp.deleteParticipanteContrato({}, {participanteContratoId: item.id}, function(response){
            $scope.participanteContrato.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
      },
      edit : function(item) {
          
        var parameter = {
          participanteContrato : item
        };
        var modalInstance = $modal.open({
        templateUrl: 'app/views/contrato/participanteContrato_edit.html',
          controller: 'participanteContratoEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
        modalInstance.result.then(function (parameters) {
            
             $scope.participanteContrato.getData();
        });
      }       
    }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'E', descripcion: 'Ejecución'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrado'}); 
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelado'});             
   
        }        
    } 
    //TERCEROS
    $scope.tercerosCon = {
      current : {}, data:[],
      getData : function() {
        $rootScope.loadingVisible = true;
            contratoHttp.getTerceros({}, {}, function(response) {  
            $scope.tercerosCon.data = response ;              
                
           if($scope.contrato.model.terceroId ){

                $scope.tercerosCon.current = $filter('filter')($scope.tercerosCon.data, { id : $scope.contrato.model.terceroId })[0];                   
           }
            else{
                $scope.tercerosCon.current=$scope.tercerosCon.data[0];                     
            }     
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }
    //CARGAMOS TIPO DE COMPROMISO
    $scope.tipoCompromiso = {
        current : {}, data:[],
        getData : function() {            
            $scope.tipoCompromiso.data.push({value:'', descripcion: 'Seleccione'});
            $scope.tipoCompromiso.data.push({value:'FI', descripcion: 'Financiera'});
            $scope.tipoCompromiso.data.push({value:'LE', descripcion: 'Legal'});
            $scope.tipoCompromiso.data.push({value:'AD', descripcion: 'Administrativa'});       
            $scope.tipoCompromiso.data.push({value:'SI', descripcion: 'Sistemas'});       
            $scope.tipoCompromiso.current =$scope.tipoCompromiso.data[0];
        }        
    }	 
	//CARGAMOS LOS LISTADOS
    
	$scope.tipoContrato.getData();
	$scope.objetoContrato.getData();    
    $scope.estados.getData();	
    $scope.tercerosCon.getData();  
    $scope.tipoCompromiso.getData();  
	
	//CARGAMOS LOS DATOS DEL contrato	
	
	if(contrato.id==0){
      $scope.estados.current=$scope.estados.data[0]; 
      $scope.tipoContrato.current = $scope.tipoContrato.data[0];
      $scope.objetoContrato.current = $scope.objetoContrato.data[0];
      //$scope.tipoProceso.current= $filter('filter')($scope.tipoProceso.data, {value : contrato.tipo})[0]; 
      //$scope.contrato.model.tipo=contrato.tipo; 
        
    }
    else{   
        $rootScope.loadingVisible = true;        
        $scope.divParticipanteContrato=true;
		
        contratoHttp.read({},$state.params.contrato, function (data) { 
        $scope.contrato.model = data;		

        if($scope.contrato.model.fechaReg){$scope.contrato.fechaReg.value=new Date(parseFloat($scope.contrato.model.fechaReg));}
        if($scope.contrato.model.fechaAct){$scope.contrato.fechaAct.value=new Date(parseFloat($scope.contrato.model.fechaAct));}  
        
        		
        if($scope.contrato.model.fechaInicial){$scope.contrato.fechaInicial.value=new Date(parseFloat($scope.contrato.model.fechaInicial));}    
        if($scope.contrato.model.fechaFinal){$scope.contrato.fechaFinal.value=new Date(parseFloat($scope.contrato.model.fechaFinal));}   
            
        $scope.tercerosCon.current = $filter('filter')($scope.tercerosCon.data, { id : $scope.contrato.model.terceroId })[0];  
     
        //$scope.estados.current=$scope.contrato.model.estado;	            
        $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.contrato.model.estado})[0];
        $scope.tipoContrato.current = $filter('filter')($scope.tipoContrato.data, {codigo : $scope.contrato.model.tipoContratoId})[0];
        $scope.objetoContrato.current = $filter('filter')($scope.objetoContrato.data, {codigo : $scope.contrato.model.objetoId})[0];            
        
        $scope.participanteContrato.getData();
        
       
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      
    
  }
})();
/**=========================================================
 * Module: app.contrato.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .service('contratoHttp', contratoHttp);

  contratoHttp.$inject = ['$resource', 'END_POINT'];


  function contratoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {  
      'getList' : {
        'method' : 'GET',
        'isArray' : true
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/contrato/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/contrato'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Cliente.svc/contrato'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/contrato/:id'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'getTipoContrato' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/TIPOCONTRATO'
      },
        'getObjetoContrato' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/OBJETOCONTRATO'
      },
        'getCargos' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/CARGO'
      },
       'getFuncionarios' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/funcionarioBusqueda'
      },
        'getParticipantesContrato': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            contratoId : '@contratoId'           
          },
          'url' : END_POINT + '/Cliente.svc/contrato/:contratoId/participante'
      },
      'addParticipanteContrato': {
        'method':'POST',
        'isArray' : true,
        'url' : END_POINT + '/Cliente.svc/contrato/participante'
      },
      'editParticipanteContrato': {
        'method':'PUT',
        'url' : END_POINT + '/Cliente.svc/contrato/participante'
      },
      'deleteParticipanteContrato': {
        'method':'DELETE',
        'params' : {
            participanteContratoId : '@participanteContratoId'           
          },
        'url' : END_POINT + '/Cliente.svc/contrato/participante/:participanteContratoId'
      },//COMPROMISOS CONTRATO
        'saveListCompromiso' : {
        'method' : 'POST',       
        'isArray' : true,
        'url' : END_POINT + '/Cliente.svc/compromiso/contrato'
      },
        'getListCompromiso' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : {
            tipo : '@tipo',
            contrato : '@contrato'
          },
        'url' : END_POINT + '/Cliente.svc/compromiso/:tipo/contrato/:contrato'
      },
     'getCompromisoParticipante' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : {
            contrato : '@contrato',
            compromiso : '@compromiso',
            id : '@id'
        },          
        'url' : END_POINT + '/Cliente.svc/compromiso/:contrato/participante/:compromiso/id/:id'
      },
      'getParticipante' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/participante'
      },
      'updateCompromiso' : {///
        'method' : 'PUT',
        'url' : END_POINT + '/Cliente.svc/compromiso'
      },
      'removeCompromiso':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/compromiso/:id'
      },
      'newCompromiso':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/compromiso/'
      },
      'deleteCompromiso': {
          'method':'DELETE',
          'isArray' : true,
          'params' : {
            compromisoId : '@compromisoId'
          },
          'url' : END_POINT + '/Cliente.svc/compromiso/:compromisoId'
      },
      'deleteParticipante': {
          'method':'DELETE',
          'params' : {
            participanteId : '@participanteId'
          },
          'url' : END_POINT + '/Cliente.svc/participante/:participanteId'
      },
      'assignParticipante':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/compromiso/participante'
      },
      'unassignParticipante':   {
        'method':'PUT',
        'url' : END_POINT + '/Cliente.svc/compromiso/participante'
      }        
     
    };
    return $resource( END_POINT + '/Cliente.svc/contrato', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.CONTRATO.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('contratoListController', contratoListController);

  contratoListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'contratoHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function contratoListController($scope, $filter, $state, ngDialog, tpl, contratoHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {   
	
		
	$scope.ESTADO_EJECUCION = 'E';
    $scope.ESTADO_ANULADO = 'A';
    $scope.ESTADO_CERRADO= 'C';	
    $scope.ESTADO_CANCELADO= 'CA';		

    $scope.contratos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrecontratos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.contratos.selectedAll = !$scope.contratos.selectedAll; 
        for (var key in $scope.contratos.selectedItems) {
          $scope.contratos.selectedItems[key].check = $scope.contratos.selectedAll;
        }
      },
      add : function() {  
              var cont = {
                      id: 0,
                      terceroId: 0,
                      tipoContratoId: 0,
                      objetoId: 0,
                      descripcion: '',
                      fechaInicial: '',
                      fechaFinal: '',
                      estado: '',
                      consecutivo: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.contrato_add', params : { contrato: cont } });
            $state.go('app.contrato_add');  
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.contrato_edit', params : { contrato: item } });
        $state.go('app.contrato_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            contratoHttp.remove({}, { id: id }, function(response) {
                $scope.contratos.getData();
                message.show("success", "Contrato eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.contratos.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    contratoHttp.remove({}, { id: id }, function(response) {
                        $scope.contratos.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.contratos.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.contratos.filterText,
          "precision": false
        }];
        $scope.contratos.selectedItems = $filter('arrayFilter')($scope.contratos.dataSource, paramFilter);
        $scope.contratos.paginations.totalItems = $scope.contratos.selectedItems.length;
        $scope.contratos.paginations.currentPage = 1;
        $scope.contratos.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.contratos.paginations.currentPage == 1 ) ? 0 : ($scope.contratos.paginations.currentPage * $scope.contratos.paginations.itemsPerPage) - $scope.contratos.paginations.itemsPerPage;
        $scope.contratos.data = $scope.contratos.selectedItems.slice(firstItem , $scope.contratos.paginations.currentPage * $scope.contratos.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.contratos.data = [];
        $scope.contratos.loading = true;
        $scope.contratos.noData = false;      
         contratoHttp.getList(function(response) {
             
          $scope.contratos.selectedItems = response;
          $scope.contratos.dataSource = response;
          for(var i=0; i<$scope.contratos.dataSource.length; i++){
            $scope.contratos.nombrecontratos.push({id: i, nombre: $scope.contratos.dataSource[i]});
          }
          $scope.contratos.paginations.totalItems = $scope.contratos.selectedItems.length;
          $scope.contratos.paginations.currentPage = 1;
          $scope.contratos.changePage();
          $scope.contratos.loading = false;
          ($scope.contratos.dataSource.length < 1) ? $scope.contratos.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }  
    
	//CARGAMOS DATA    
    $scope.contratos.getData();  
        
 
	}
  
  
  })();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('participanteContratoEditController', participanteContratoEditController);

  participanteContratoEditController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'contratoHttp',   'parameters', 'message', 'parametersOfState','$rootScope', 'REGULAR_EXPRESION'];


  function participanteContratoEditController($scope, $filter, $state, $modalInstance, LDataSource, contratoHttp, parameters, message, parametersOfState,$rootScope, REGULAR_EXPRESION) {
      
    
      
      $scope.participanteContrato = {     
          model:{
            id: 0,
            funcionarioId: 0,
            cargoId: 0,
            contratoId: 0,
            funcionario: '',
            cargo: ''
          },
		fechaReg : {
            isOpen : false,
            value:'',
            dateOptions : {
              formatYear: 'yy',
              startingDay: 1
            },
            open : function($event) {
              $scope.contrato.fechaReg.isOpen = false;
            }
          },
         save:  function(){       
             
             
                if(!$scope.funcionarios.current){message.show("warning", "Funcionario requerido");return;
                }else{$scope.participanteContrato.model.funcionarioId = $scope.funcionarios.current.id;}
          
                if(!$scope.cargo.current){message.show("warning", "Cargo requerido");return;}
                else{$scope.participanteContrato.model.cargoId= $scope.cargo.current.codigo;}	

             var _http = new contratoHttp($scope.participanteContrato.model);
             if(  $scope.participanteContrato.model.id==0){
                 
                  _http.$addParticipanteContrato(function(response){                  
                      message.show("success", "Participante creado satisfactoriamente!!");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editParticipanteContrato(function(response){                  
                      message.show("success", "Participante actualizado satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }             
              $modalInstance.close();
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }
      $scope.participanteContrato.model=parameters.participanteContrato;
      
     //TIPO CONTRATO
    $scope.cargo = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        contratoHttp.getCargos({}, {}, function(response) {
            $scope.cargo.data = $filter('orderBy')(response, 'codigo');
            
            if(!$scope.participanteContrato.model){
                $scope.cargo.current = $scope.cargo.data[0];
                }
            else{
                $scope.cargo.current = $filter('filter')($scope.cargo.data, { codigo : $scope.participanteContrato.model.cargoId })[0];
                }
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setCargo : function(){
          $scope.participanteContrato.cargoId=$scope.cargo.current.codigo;
    }}
     //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            contratoHttp.getFuncionarios({}, {}, function(response) {
            
            $scope.funcionarios.data = response;            
            
            if ($scope.participanteContrato.model.funcionarioId!=0)
            {
              $scope.funcionarios.current = $filter('filter')($scope.funcionarios.data, { id : $scope.participanteContrato.model.funcionarioId })[0];   
            }
            else{
                
                $scope.funcionarios.current=$scope.funcionarios.data[0];
            }

            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setFuncionario' : function() {
          $scope.participanteContrato.funcionarioId=$scope.funcionarios.current.id;
      }
    }   
	   //CARGAMOS LOS LISTADOS	
	   $scope.cargo.getData();
       $scope.funcionarios.getData();
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
    
       if($scope.participanteContrato.model.fechaReg){$scope.participanteContrato.fechaReg.value=new Date(parseFloat($scope.participanteContrato.model.fechaReg));}   
    
      
  }
})();
(function() {
    'use strict';

    angular
        .module('app.core')
        .config(coreConfig);

    coreConfig.$inject = ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$httpProvider'];
    function coreConfig($controllerProvider, $compileProvider, $filterProvider, $provide, $httpProvider){
      
      var core = angular.module('app.core');
      // registering components after bootstrap
      core.controller = $controllerProvider.register;
      core.directive  = $compileProvider.directive;
      core.filter     = $filterProvider.register;
      core.factory    = $provide.factory;
      core.service    = $provide.service;
      core.constant   = $provide.constant;
      core.value      = $provide.value;
      
    }

})();
/**=========================================================
 * Module: constants.js
 * Define constants to inject across the application
 https://crossorigin.me/https://ec2-54-213-134-94.us-west-2.compute.amazonaws.com/LyqService/Services
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.core')
        .constant('APP_MEDIAQUERY', {
          'desktopLG':             1200,
          'desktop':                992,
          'tablet':                 768,
          'mobile':                 480
        })
    .constant('END_POINT', 'https://service.lyqauditores.com/Services')    
    //.constant('END_POINT', 'http://localhost:59718/Services')       
    .constant('SERVER_URL', 'https://service.lyqauditores.com/')
    .constant('TO_STATE', '')
    .constant('REGULAR_EXPRESION', {
      POSITIVES_INTEGERS : /^\d{0,10}?$/g,
      POSTIVES_DECIMALS : /(^[0-9]+(\.[0-9]+)?$)|(^[0-9]+\.$)/g, 
      ONLY_UPPERCASE : /\b^[A-Z]+$\b/g 
    });
})();
(function() {
  'use strict';

  angular
    .module('app.core')
    .run(appRun);

  appRun.$inject = ['$rootScope', '$state', '$stateParams',  '$window', '$templateCache', 'Colors', 'parametersOfState', 'TO_STATE'];

  function appRun($rootScope, $state, $stateParams, $window, $templateCache, Colors, parametersOfState, TO_STATE) {


    // Set reference to access them from any scope
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$storage = $window.localStorage;

    // Uncomment this to disable template cache
    /*$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
          if (typeof(toState) !== 'undefined'){
            $templateCache.remove(toState.templateUrl);
          }
      });*/

    // Allows to use branding color with interpolation
    // {{ colorByName('primary') }}
    $rootScope.colorByName = Colors.byName;

    // cancel click event easily
    $rootScope.cancel = function($event) {                                                                                             $event.stopPropagation();
                                         };

    // Hooks Example
    // ----------------------------------- 

    // Hook not found
    $rootScope.$on('$stateNotFound',
                   function(event, unfoundState/*, fromState, fromParams*/) {
      console.log(unfoundState.to); // "lazy.state"
      console.log(unfoundState.toParams); // {a:1, b:2}
      console.log(unfoundState.options); // {inherit:false} + default options
    });
    // Hook error
    $rootScope.$on('$stateChangeError',
                   function(event, toState, toParams, fromState, fromParams, error){
    });
    // Hook success


    /*
    $rootScope.$on('$stateChangeSuccess',
                   function(event, toState, toParams, fromState, fromParams) {
      debugger;
    });
*/
    TO_STATE = '';
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
      if (parametersOfState.get(toState) != undefined) {
    //    debugger;
        if (TO_STATE != toState.name) {
          TO_STATE = toState.name;
          var params = parametersOfState.get(toState);
          $state.go(toState.name, params );
          event.preventDefault();  
        } else {
          TO_STATE = "";
        }
      }
    });

  }



})();


  angular
    .module('app.core')
    .factory('message', ["$timeout", function ($timeout) {

    var instance = {}
    instance.messages = [];

    instance.show = function(type, message) {
      if (message !== null) {

      toastr.options.closeButton = (true);
      toastr.options.positionClass = 'toast-bottom-right';
      toastr.options.progressBar = true;
      toastr.options.preventDuplicates = true;

      toastr[type](message, '');

/*    toastr.options.closeButton = false;
    toastr.options.progressBar = false;
    toastr.options.debug = false;
    toastr.options.positionClass = 'toast-bottom-left';
    toastr.options.showDuration = 333;
    toastr.options.hideDuration = 333;
    toastr.options.timeOut = 4000;
    toastr.options.extendedTimeOut = 4000;
    toastr.options.showEasing = 'swing';
    toastr.options.hideEasing = 'swing';
    toastr.options.showMethod = 'slideDown';
    toastr.options.hideMethod = 'slideUp';*/


      }
    }
    
    return instance;
  }]);

/**=========================================================
 * Module: app.core.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('parametersOfState', parametersOfState);

  parametersOfState.$inject = [];


  function parametersOfState() {
    
    var _parametersOfState = {};
      
    return {
      set: function (state) {
        _parametersOfState[state.name] = JSON.stringify(state.params);
      },
      get : function (state) {
        return _parametersOfState[state.name] != undefined ? JSON.parse(_parametersOfState[state.name]) : state.params;
      }
    }
  }

})();
/**=========================================================
 * Module: app.consecutivo.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.consecutivo')
    .controller('consecutivoController', consecutivoController);

  consecutivoController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'consecutivoHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function consecutivoController($scope, $filter, $state, LDataSource, consecutivoHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var consecutivo = $state.params.consecutivo;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.consecutivo = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,   
              funcionarioId: 0,   
              tipo: 0,
              codigo: '',			  
			  estado: '',
			  descripcion: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: ''	
          
		},   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.consecutivo.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.consecutivo.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.consecutivo', params : { filters : {procesoSeleccionado:consecutivo.tipo}, data : []} });
        $state.go('app.consecutivo');
          
      },
      save : function() {		  
       
		  
                
                if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.consecutivo.model.terceroId = $scope.tercerosOp.current.id;}
          
          		if($scope.consecutivo.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
				
                if(!$scope.funcionarios.current){message.show("warning", "Funcionario requerido");return;
                }else{$scope.consecutivo.model.funcionarioId = $scope.funcionarios.current.id;}
          
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.consecutivo.model.estado= $scope.estados.current.value;}
				
				if(!$scope.tipoConsecutivo.current){message.show("warning", "Tipo consecutivo");return;}
				else{$scope.consecutivo.model.tipo= $scope.tipoConsecutivo.current.id;}	
   	
          
			//INSERTAR
            if($scope.consecutivo.model.id==0){
				
				$rootScope.loadingVisible = true;
                
				consecutivoHttp.save({}, $scope.consecutivo.model, function (data) { 
					
						consecutivoHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.consecutivo.model=data;
							consecutivo.id=$scope.consecutivo.model.id; 
							if($scope.consecutivo.model.fechaReg){$scope.consecutivo.fechaReg.value=new Date(parseFloat($scope.consecutivo.model.fechaReg));}    
							if($scope.consecutivo.model.fechaAct){$scope.consecutivo.fechaAct.value=new Date(parseFloat($scope.consecutivo.model.fechaAct));} 		
								
							message.show("success", "consecutivo creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.consecutivo.model.id){
					$state.go('app.consecutivo');
				}else{
					
					consecutivoHttp.update({}, $scope.consecutivo.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.consecutivo.model=data;
						if($scope.consecutivo.model.fechaReg){$scope.consecutivo.fechaReg.value=new Date(parseFloat($scope.consecutivo.model.fechaReg));}    
						if($scope.consecutivo.model.fechaAct){$scope.consecutivo.fechaAct.value=new Date(parseFloat($scope.consecutivo.model.fechaAct));}  
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                     $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : data.contactoId })[0]; 
                    message.show("success", "consecutivo actualizado satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.consecutivo.model.id,
                         referencia : 'CONSECUTIVO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.consecutivo.model.id,
                         referencia : 'CONSECUTIVO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/consecutivo/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.consecutivo.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.consecutivo.model.id,
                         referencia : 'CONSECUTIVO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }        
          
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'AN', descripcion: 'Anulado'});            
            $scope.estados.data.push({value:'AP', descripcion: 'Aprobado'}); 
        }        
    }
	//TIPO PROCESO
      $scope.tipoConsecutivo = {
            current: {},
            data: [],
            getData: function() { 
               consecutivoHttp.getTipoConsecutivos({}, {}, function(response) {
                      
                     $scope.tipoConsecutivo.data = $filter('orderBy')(response, 'id');                     
                     $scope.tipoConsecutivo.current= $filter('filter')($scope.tipoConsecutivo.data, {id : consecutivo.tipo})[0];  
                     $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });
                    
            }
        }
      

    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            consecutivoHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.consecutivo.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.consecutivo.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.consecutivo.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.consecutivo.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }
    //CONTACTOS
    $scope.contactosOp = {
      current : {}, data:[],
      getData : function() {                  
          
               $rootScope.loadingVisible = true;
                consecutivoHttp.getContactosEmpresa({}, { terceroId: $scope.consecutivo.model.terceroId }, function(response) {   
                    
             $scope.contactosOp.data = response ;
                                                                                                                     
              if($scope.consecutivo.model ){ 
                    
                    $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.consecutivo.model.contactoId })[0];
               }
                else{
                    $scope.contactosOp.current=$scope.contactosOp.data[0];                     
                }
                                                                                                                   
                                                                                                                       
                    
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
            });
          
      },
      'setContacto' : function() {
        
          $scope.consecutivo.model.contactoId=$scope.contactosOp.current.id;
      }
    }
    
    //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            
            consecutivoHttp.getFuncionarios({}, {}, function(response) {
            
                $scope.funcionarios.data = response;  
                $scope.funcionarios.data.push({id:(-1), funcionario:"Seleccione"});
                $scope.funcionarios.data=$filter('orderBy')($scope.funcionarios.data, 'id');                
                $rootScope.loadingVisible = false;
                
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    } 
    

	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();
	$scope.tipoConsecutivo.getData();
    $scope.tercerosOp.getData();   
    $scope.funcionarios.getData();   
    
      
   
  
	
	//CARGAMOS LOS DATOS DEL consecutivo	
	
	if(consecutivo.id==0){        
        
        
      $scope.estados.current=$scope.estados.data[0]; 
      $scope.consecutivo.model.tipo=consecutivo.tipo;  
      $scope.funcionarios.current= $scope.funcionarios.data[0];
               
      
        
    }
    else{   
        $rootScope.loadingVisible = true;
		
            consecutivoHttp.read({},$state.params.consecutivo, function (data) { 
                
            $scope.consecutivo.model = data;	
            $scope.btnAdjunto=true;          
               
			if($scope.consecutivo.model.fechaReg){$scope.consecutivo.fechaReg.value=new Date(parseFloat($scope.consecutivo.model.fechaReg));}    
			if($scope.consecutivo.model.fechaAct){$scope.consecutivo.fechaAct.value=new Date(parseFloat($scope.consecutivo.model.fechaAct));}  
            
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.consecutivo.model.terceroId })[0];  
            $scope.contactosOp.getData();               
            $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.consecutivo.model.contactoId })[0]; 
                
            $scope.funcionarios.current = $filter('filter')($scope.funcionarios.data, { id : $scope.consecutivo.model.funcionarioId })[0]; 
                
			//$scope.estados.current=$scope.consecutivo.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.consecutivo.model.estado})[0];
            $scope.tipoConsecutivo.current = $filter('filter')($scope.tipoConsecutivo.data, {id : $scope.consecutivo.model.tipoProceso})[0];
        
                          
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
    
    
  }
})();
/**=========================================================
 * Module: app.consecutivo.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.consecutivo')
    .service('consecutivoHttp', consecutivoHttp);

  consecutivoHttp.$inject = ['$resource', 'END_POINT'];


  function consecutivoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            tipo : '@tipo',     
            estado : '@estado'     
          },
          'url' : END_POINT + '/Cliente.svc/consecutivo/tipo/:tipo/estado/:estado'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/consecutivo/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/consecutivo'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Cliente.svc/consecutivo'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/consecutivo/:id/tipo'
      },
        'getTipoConsecutivos' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Cliente.svc/tipoConsecutivo'
      },        
      'getContactosEmpresa' : {
       'params' : {
            terceroId : '@terceroId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroContratoBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroconsecutivo'
      },
        getFuncionarios : {
        method : 'GET',
        isArray : true,
        url : END_POINT + '/General.svc/funcionarioBusqueda'
      }
     
    };
    return $resource( END_POINT + '/Cliente.svc/consecutivo/tipo', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.consecutivo.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.consecutivo')
    .controller('consecutivoListController', consecutivoListController);

  consecutivoListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'consecutivoHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function consecutivoListController($scope, $filter, $state, ngDialog, tpl, consecutivoHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
        
    
    var procesoSeleccionado= $state.params.filters.procesoSeleccionado;
	
		
	$scope.ESTADO_PROCESO = 'P'; 
    $scope.ESTADO_APROBADO= 'AP';	
    $scope.ESTADO_ANULADO= 'AN';	
    		

    $scope.consecutivos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreconsecutivos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.consecutivos.selectedAll = !$scope.consecutivos.selectedAll; 
        for (var key in $scope.consecutivos.selectedItems) {
          $scope.consecutivos.selectedItems[key].check = $scope.consecutivos.selectedAll;
        }
      },
      add : function() {
        
        if($scope.tipoConsecutivo.current.id){        
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      funcionarioId: 0,
                      tipo: $scope.tipoConsecutivo.current.id,
                      codigo: '',                      
                      estado: '',
                      descripcion: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.consecutivo_add', params : { consecutivo: oport } });
            $state.go('app.consecutivo_add');
          
          }
          else{
              
              message.show("warning", "Debe seleccionar el tipo de proceso!!")
          }
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.consecutivo_edit', params : { consecutivo: item } });
        $state.go('app.consecutivo_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            consecutivoHttp.remove({}, { id: id }, function(response) {
                $scope.consecutivos.getData();
                message.show("success", "consecutivo eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.consecutivos.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    consecutivoHttp.remove({}, { id: id }, function(response) {
                        $scope.consecutivos.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.consecutivos.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.consecutivos.filterText,
          "precision": false
        }];
        $scope.consecutivos.selectedItems = $filter('arrayFilter')($scope.consecutivos.dataSource, paramFilter);
        $scope.consecutivos.paginations.totalItems = $scope.consecutivos.selectedItems.length;
        $scope.consecutivos.paginations.currentPage = 1;
        $scope.consecutivos.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.consecutivos.paginations.currentPage == 1 ) ? 0 : ($scope.consecutivos.paginations.currentPage * $scope.consecutivos.paginations.itemsPerPage) - $scope.consecutivos.paginations.itemsPerPage;
        $scope.consecutivos.data = $scope.consecutivos.selectedItems.slice(firstItem , $scope.consecutivos.paginations.currentPage * $scope.consecutivos.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.consecutivos.data = [];
        $scope.consecutivos.loading = true;
        $scope.consecutivos.noData = false;
        var parametros= {
            "tipo":$scope.tipoConsecutivo.current.id,   
            "estado":$scope.estado.current.id   
            
        };          
        consecutivoHttp.getList({}, parametros,function(response) {
              $scope.consecutivos.selectedItems = response;
              $scope.consecutivos.dataSource = response;
            
              for(var i=0; i<$scope.consecutivos.dataSource.length; i++){
                $scope.consecutivos.nombreconsecutivos.push({id: i, nombre: $scope.consecutivos.dataSource[i]});
              }
            
              $scope.consecutivos.paginations.totalItems = $scope.consecutivos.selectedItems.length;
              $scope.consecutivos.paginations.currentPage = 1;
              $scope.consecutivos.changePage();
              $scope.consecutivos.loading = false;
              ($scope.consecutivos.dataSource.length < 1) ? $scope.consecutivos.noData = true : null;
              $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
      //CARGAMOS PROCESOS
    $scope.tipoConsecutivo = {
        current : {}, data:[],
        getData : function() {
            
            $rootScope.loadingVisible = true;
                 consecutivoHttp.getTipoConsecutivos({}, {}, function(response) {
                     $scope.tipoConsecutivo.data = $filter('orderBy')(response, 'id');
                     
                     if(procesoSeleccionado){        
      
                            $scope.tipoConsecutivo.current = $filter('filter')($scope.tipoConsecutivo.data, {id : procesoSeleccionado})[0];  
                    }
                     else{
                         $scope.tipoConsecutivo.current =$scope.tipoConsecutivo.data[0];
                     }
                     
                     $scope.consecutivos.getData();  
                    $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });
            
        }        
    }	   
        
        //ESTADOS
    $scope.estado = {
        current : {}, data:[],
        getData : function() {
            $scope.estado.data.push({id:'P', nombre: 'Proceso'});
            $scope.estado.data.push({id:'AN', nombre: 'Anulado'});            
            $scope.estado.data.push({id:'AP', nombre: 'Aprobado'}); 
            $scope.estado.current =$scope.estado.data[0];
        }        
    }
    
	//CARGAMOS LOS TIPOS DE PROCESO
    $scope.tipoConsecutivo.getData();
    $scope.estado.getData();
    
        
   
        
	}
  
  
  })();
/**=========================================================
 * Module: app.factura.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.factura')
    .controller('facturaController', facturaController);

  facturaController.$inject = ['$scope', '$filter', '$state', 'LDataSource','base64', 'facturaHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION','SERVER_URL','$window'];


  function facturaController($scope, $filter, $state, LDataSource,base64, facturaHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION,SERVER_URL,$window) {
      
     
      var factura = $state.params.factura;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.factura = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,
              viaContactoId: 0,
              tipo: '',
              consecutivo: '',
			  asunto: '',
			  estado: '',
			  descripcion: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: '',
              fechaEmision: '',
              fechaVencimiento: ''
              
		},  
        fechaEmision : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.factura.fechaEmision.isOpen = true;
        }
      },  
        fechaVencimiento : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.factura.fechaVencimiento.isOpen = true;
        }
      },
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.factura.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.factura.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.factura', params : { filters : {procesoSeleccionado:factura.tipo}, data : []} });
        $state.go('app.factura');
          
      },
      save : function() {		  
       
		        if($scope.factura.fechaEmision.value==""){message.show("warning", "Fecha de emisión requerida");return;}
				else{$scope.factura.model.fechaEmision=Date.parse(new Date($scope.factura.fechaEmision.value));}	
          
                if($scope.factura.fechaVencimiento.value==""){message.show("warning", "Fecha de vencimiento requerida");return;}
				else{$scope.factura.model.fechaVencimiento=Date.parse(new Date($scope.factura.fechaVencimiento.value));}	
                
                if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.factura.model.terceroId = $scope.tercerosOp.current.id;}
          
          		if($scope.factura.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.factura.model.estado= $scope.estados.current.value;}
			
          
			//INSERTAR
            if($scope.factura.model.id==0){
				
				$rootScope.loadingVisible = true;
				facturaHttp.save({}, $scope.factura.model, function (data) { 
					
						facturaHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.factura.model=data;
							factura.id=$scope.factura.model.id; 
							if($scope.factura.model.fechaReg){$scope.factura.fechaReg.value=new Date(parseFloat($scope.factura.model.fechaReg));}    
							if($scope.factura.model.fechaAct){$scope.factura.fechaAct.value=new Date(parseFloat($scope.factura.model.fechaAct));} 		
                            
                                $scope.btnAdjunto=true;
                                $scope.divProductosfactura=true;
								
							message.show("success", "Información registrada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.factura.model.id){
					$state.go('app.factura');
				}else{
					
					facturaHttp.update({}, $scope.factura.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.factura.model=data;
						if($scope.factura.model.fechaReg){$scope.factura.fechaReg.value=new Date(parseFloat($scope.factura.model.fechaReg));}    
						if($scope.factura.model.fechaAct){$scope.factura.fechaAct.value=new Date(parseFloat($scope.factura.model.fechaAct));}  
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                     $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : data.contactoId })[0]; 
                    message.show("success", "Información actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.id,
                         referencia : 'factura' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/factura/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.factura.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.id,
                         referencia : 'factura' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      } ,imprimir : function() {            
        	facturaHttp.getFormato({},{ id : $scope.factura.model.id,formato:'FACTURA'}, function (data) {
							
                            var urlDownload = SERVER_URL + base64.decode(data.url);
                            $window.open(urlDownload);                           
											
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
          
          
          
      },
          verCliente : function() {            
            var modalInstance = $modal.open({
              templateUrl: 'app/views/tercero/consultaTercero_form.html',
              controller: 'consultaTerceroController',
              size: 'lg',
              resolve: {
                parameters: { id : $scope.factura.model.terceroId,
                              referencia : 'CLIENTE'}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          }, 
   
          
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'PA', descripcion: 'Pagada'});             
        }        
    }

    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            facturaHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.factura.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.factura.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.factura.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.factura.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }
    //CONTACTOS
    $scope.contactosOp = {
      current : {}, data:[],
      getData : function() {                  
          
               $rootScope.loadingVisible = true;
                facturaHttp.getContactosEmpresa({}, { terceroId: $scope.factura.model.terceroId }, function(response) {   
                    
             $scope.contactosOp.data = response ;
                                                                                                                     
              if($scope.factura.model ){ 
                    
                    $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.factura.model.contactoId })[0];
               }
                else{
                    $scope.contactosOp.current=$scope.contactosOp.data[0];                     
                }
                                                                                                                   
                                                                                                                       
                    
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
            });
          
      },
      'setContacto' : function() {
        
          $scope.factura.model.contactoId=$scope.contactosOp.current.id;
      }
    }
    
    
      //PRODUCTOS
    $scope.productosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            facturaHttp.getProductos({}, {}, function(response) {   
                
                $scope.productosOp.data = response ;                 
                $scope.productosOp.current=$scope.productosOp.data[0];
                $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }
    
    
    $scope.productofactura = {
    model : {  
            productoId: 0,
            oportunidadId :0,
            total :0,
            totalPendiente :0,
            valor :0,
            cantidad :0,
            valorIva :0,
            producto: '',
            comentario: ''
			
		},
      current : {},
      data:[],
      getData : function() {
          
          
          if(factura){
              
            if($scope.factura.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              facturaHttp.getProductosfactura({}, { facturaId: $scope.factura.model.id }, function(response) {
                  
                  $scope.productofactura.data = response;
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },        
        add: function(){           
            
              
            if($scope.factura.model.id!=0){
                if($scope.productosOp.current){
                    $rootScope.loadingVisible = true;                    
                    
                    var data ={
                         productoId: $scope.productosOp.current.id,
                         oportunidadId :$scope.factura.model.id,
                         total :  $scope.productosOp.current.valor,
                         totalPendiente :  $scope.productosOp.current.valor,
                         valor :$scope.productosOp.current.valor,
                         cantidad :1,
                         valorIva :0,
                         producto: $scope.productosOp.current.nombre,
                         comentario: ''
                        
                    } ;
                      facturaHttp.addProductofactura({}, data, function(response) {

                          $scope.productofactura.getData();
                          $rootScope.loadingVisible = false;
                          message.show("success", "Producto agregado satisfactoriamente");	
                      }, function(faild) {
                          $rootScope.loadingVisible = false;
                          message.show("error", faild.Message);
                      });    
                }
                else{message.show("warning", "Debe seleccionar un producto");}

            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
          
        }
        ,        
        update: function(data){
            
            
            if($scope.factura.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              facturaHttp.editProductofactura({}, data, function(response) {
                  
                  $scope.productofactura.getData();
                  $rootScope.loadingVisible = false;
                  message.show("success", "Producto actualizado satisfactoriamente");	
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
        }
        ,delete: function(data){            
            
            $rootScope.loadingVisible = true;    
            debugger;
            facturaHttp.removeProductofactura({}, {oportunidadId: $scope.factura.model.id,productoId: data.productoId }, function(response){
             $scope.productofactura.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
        }
    }
	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();	    
    $scope.tercerosOp.getData();   
    $scope.productosOp.getData();
	
	//CARGAMOS LOS DATOS DEL factura	
	
	if(factura.id==0){
        
      $scope.estados.current=$scope.estados.data[0];             
      $scope.factura.model.tipo=factura.tipo;             
        
    }
    else{   
        $rootScope.loadingVisible = true;
		
            facturaHttp.read({},$state.params.factura, function (data) { 
                
            $scope.factura.model = data;	
            $scope.btnAdjunto=true;
            $scope.divProductosfactura=true;
               
			if($scope.factura.model.fechaReg){$scope.factura.fechaReg.value=new Date(parseFloat($scope.factura.model.fechaReg));}    
			if($scope.factura.model.fechaAct){$scope.factura.fechaAct.value=new Date(parseFloat($scope.factura.model.fechaAct));}  
            
                
            if($scope.factura.model.fechaEmision){$scope.factura.fechaEmision.value=new Date(parseFloat($scope.factura.model.fechaEmision));}    
			if($scope.factura.model.fechaVencimiento){$scope.factura.fechaVencimiento.value=new Date(parseFloat($scope.factura.model.fechaVencimiento));}     
                
                
                
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.factura.model.terceroId })[0];  
            $scope.contactosOp.getData();
            $scope.productofactura.getData();                
            $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.factura.model.contactoId })[0];    
                
			//$scope.estados.current=$scope.factura.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.factura.model.estado})[0];
               
                    
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      

      
        
	
    
    
  }
})();
/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.factura')
    .service('facturaHttp', facturaHttp);

  facturaHttp.$inject = ['$resource', 'END_POINT'];


  function facturaHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            proceso : '@proceso',
            estado : '@estado'
          },
          'url' : END_POINT + '/Comercial.svc/oportunidad/proceso/:proceso/estado/:estado'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:id/proceso'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/oportunidad/proceso'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/oportunidad/proceso'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:id/proceso'
      },
        'getViaContacto' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/VIACONTACTO'
      },        
      'getContactosEmpresa' : {
       'params' : {
            terceroId : '@terceroId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroOportunidad'
      },
       'getProductos' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/productoBusqueda'
      },
       'getProductosfactura' : {
       'params' : {
            facturaId : '@facturaId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:facturaId/producto'
      },
        'addProductofactura': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/oportunidad/producto'
      },
        'editProductofactura': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/oportunidad/producto'
      },
      'removeProductofactura':  {
        'params' : {
            oportunidadId : '@oportunidadId',
            productoId : '@productoId'   
          },
        'method':'DELETE',        
        'url' : END_POINT + '/Comercial.svc/oportunidad/:oportunidadId/producto/:productoId'
      },'getFormato':  {
        'params' : {
            id : '@id',
            formato : '@formato'   
          },
        'method':'GET',        
        'url' : END_POINT + '/Comercial.svc/formato/:formato/oportunidad/:id'
      }
     
    };
    return $resource( END_POINT + '/Comercial.svc/oportunidad/proceso', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.factura.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.factura')
    .controller('facturaListController', facturaListController);

  facturaListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'facturaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function facturaListController($scope, $filter, $state, ngDialog, tpl, facturaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {	
		
	$scope.ESTADO_PROCESO = 'P';
    $scope.ESTADO_ANULADO = 'A';
    $scope.ESTADO_CERRADO= 'C';	
    $scope.ESTADO_CANCELADO= 'CA';		
        
    var estadoSeleccionado= $state.params.filters.estadoSeleccionado;

    $scope.facturas = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrefacturas:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.facturas.selectedAll = !$scope.facturas.selectedAll; 
        for (var key in $scope.facturas.selectedItems) {
          $scope.facturas.selectedItems[key].check = $scope.facturas.selectedAll;
        }
      },
      add : function() {
        
       
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      tipo: 'FAC',
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',                  
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.factura_add', params : { factura: oport } });
            $state.go('app.factura_add');
      
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.factura_edit', params : { factura: item } });
        $state.go('app.factura_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            facturaHttp.remove({}, { id: id }, function(response) {
                $scope.facturas.getData();
                message.show("success", "factura eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.facturas.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    facturaHttp.remove({}, { id: id }, function(response) {
                        $scope.facturas.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.facturas.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.facturas.filterText,
          "precision": false
        }];
        $scope.facturas.selectedItems = $filter('arrayFilter')($scope.facturas.dataSource, paramFilter);
        $scope.facturas.paginations.totalItems = $scope.facturas.selectedItems.length;
        $scope.facturas.paginations.currentPage = 1;
        $scope.facturas.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.facturas.paginations.currentPage == 1 ) ? 0 : ($scope.facturas.paginations.currentPage * $scope.facturas.paginations.itemsPerPage) - $scope.facturas.paginations.itemsPerPage;
        $scope.facturas.data = $scope.facturas.selectedItems.slice(firstItem , $scope.facturas.paginations.currentPage * $scope.facturas.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.facturas.data = [];
        $scope.facturas.loading = true;
        $scope.facturas.noData = false;
        var parametros= {
            "proceso":'FAC',
            "estado":$scope.estado.current.value
        };
       
        facturaHttp.getList({}, parametros,function(response) {
          $scope.facturas.selectedItems = response;
          $scope.facturas.dataSource = response;
          for(var i=0; i<$scope.facturas.dataSource.length; i++){
            $scope.facturas.nombrefacturas.push({id: i, nombre: $scope.facturas.dataSource[i]});
          }
          $scope.facturas.paginations.totalItems = $scope.facturas.selectedItems.length;
          $scope.facturas.paginations.currentPage = 1;
          $scope.facturas.changePage();
          $scope.facturas.loading = false;
          ($scope.facturas.dataSource.length < 1) ? $scope.facturas.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
    //ESTADOS
    $scope.estado = {
        current : {}, data:[],
        getData : function() {            
            $scope.estado.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estado.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estado.data.push({value:'C', descripcion: 'Cerrado'}); 
            $scope.estado.data.push({value:'CA', descripcion: 'Cancelado'}); 
            $scope.estado.current =$scope.estado.data[0];
        }        
    }

	//CARGAMOS LOS ESTADOS Y LAS FACTURAS
    $scope.estado.getData();
    $scope.facturas.getData();  
        
	}
  
  
  })();
/**=========================================================
 * Module: app.forma30.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.fileManager')
    .controller('fileManagerController', fileManagerController);

  fileManagerController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'fileManagerHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window'];


  function fileManagerController($scope, $rootScope, $state, $modalInstance, fileManagerHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window) {


    $scope.fileManager = {
      model : {
        file : {},
        referenciaId : '',
        tipoReferencia : '',
        comentario : ''
      },
      paginations : {
        maxSize : 3,
        itemsPerPage : 3,
        currentPage : 0,
        totalItems : 0
      },
      filterText : '',
      dataSource : [],
      selectedItems : [],
      data : [],
      noData : true,
      loading : false,
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.fileManager.filterText,
          "precision": false
        }];
        $scope.fileselectedItems = $filter('arrayFilter')($scope.fileManager.dataSource, paramFilter);
        $scope.fileManager.paginations.totalItems = $scope.fileManager.selectedItems.length;
        $scope.fileManager.paginations.currentPage = 1;
        $scope.fileManager.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.fileManager.paginations.currentPage == 1 ) ? 0 : ($scope.fileManager.paginations.currentPage * $scope.fileManager.paginations.itemsPerPage) - $scope.fileManager.paginations.itemsPerPage;
        $scope.fileManager.data = $scope.fileManager.selectedItems.slice(firstItem , $scope.fileManager.paginations.currentPage * $scope.fileManager.paginations.itemsPerPage);
      },
      refresh : function() {
        var params = {
          idReferencia : $scope.fileManager.model.referenciaId,
          tipoReferencia : $scope.fileManager.model.tipoReferencia
        };
        $scope.fileManager.getData(params);
      },
      setData : function(data) {
        $scope.fileManager.selectedItems = data;
        $scope.fileManager.dataSource = data;
        $scope.fileManager.paginations.totalItems = $scope.fileManager.selectedItems.length;
        $scope.fileManager.paginations.currentPage = 1;
        $scope.fileManager.changePage();
        $scope.fileManager.noData = false;
        $scope.fileManager.loading = false;
        ($scope.fileManager.dataSource.length < 1) ? $scope.fileManager.noData = true : null;
      },
      getData : function(params) {
        $scope.fileManager.data = [];
        $scope.fileManager.loading = true;
        $scope.fileManager.noData = false;

        fileManagerHttp.getDocumentos({}, params, function(response) {
          $scope.fileManager.setData(response);
        })
      },
      visualizar : function(id) {
        var params = {
          id : id 
        };

        fileManagerHttp.getDocumento({}, params, function(response) {
          if (response.url != '') {
            var urlDownload = SERVER_URL + base64.decode(response.url);
            $window.open(urlDownload);
          }
        })
      },
      eliminar : function(id) {
        var params = {
          id : id 
        };

        $rootScope.loadingVisible = true;

        fileManagerHttp.removeDocumento(params, function(response) {
          $scope.fileManager.refresh();
          message.show("info", "Adjunto eliminado correctamente");
          $rootScope.loadingVisible = false;
        }, function() {
          message.show("error", "Ocurrio un error al intentar eliminar el adjunto");
          $rootScope.loadingVisible = false;
        });
      },
      uploader : function(file) {

        $rootScope.loadingVisible = true;

        file.upload = Upload.upload({
          url: END_POINT + '/General.svc/uploadDocument',
          headers: {
            Authorization : tokenManager.get()
          },
          data: { file: file, 
                 referenciaId: base64.encode($scope.fileManager.model.referenciaId),
                 tipoReferencia : base64.encode($scope.fileManager.model.tipoReferencia),
                 comentario : base64.encode($scope.fileManager.model.comentario)
                }
        });

        file.upload.then(function (response) {
          message.show("success", "Archivo almacenado correctamente");
          $scope.fileManager.model.file = {};
          $scope.fileManager.model.comentario = '';
          $scope.fileManager.refresh();
          $rootScope.loadingVisible = false;
        }, function (faild) {
          $rootScope.loadingVisible = false;
          message.show("error", "Ocurrio un error al intentar guardar el archivo");
        });

      },
      close : function() {
        $modalInstance.dismiss('cancel');
      }
    }

    $scope.fileManager.model.referenciaId = parameters.id.toString();
    $scope.fileManager.model.tipoReferencia = parameters.referencia.toString();
    $scope.hideDelete = (parameters.hideDelete) ? parameters.hideDelete : false;

    $scope.fileManager.refresh();

  }

})();
/**=========================================================
 * Module: app.fileManager.js
 =========================================================*/
(function() {
    'use strict';
    angular.module('app.fileManager').controller('fileManagerDynamicController', fileManagerDynamicController);
    fileManagerDynamicController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'fileManagerHttp', 'parameters', 'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window'];

    function fileManagerDynamicController($scope, $rootScope, $state, $modalInstance, fileManagerHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window) {
        $scope.motrarTabla = false;
        $scope.fileManager = {
                model: {
                    file: {},
                    tipoReferencia: null,
                    anio: null,
                    mes: null,
                    liquidacion: null,
                    tipoPrecio: null,
                    curvaId: null,
                    tipoHdr: null
                },
                error: [],
                uploader: function(file) {
                    $rootScope.loadingVisible = true;
                    cargueArchivo(file);
                },
                close: function() {
                    $modalInstance.dismiss('cancel');
                }
            }
            /**
             * Objeto de la tabla
             */
        $scope.dataPage = {
            paginations: {
                maxSize: 3,
                itemsPerPage: 10,
                currentPage: 0,
                totalItems: 0
            },
            selectedAll: false,
            filterText: '',
            dataSource: [],
            selectedItems: [],
            data: [],
            noData: false,
            loading: false,
            selectAll: function() {
                $scope.dataPage.selectedAll = !$scope.dataPage.selectedAll;
                for (var key in $scope.dataPage.selectedItems) {
                    $scope.dataPage.selectedItems[key].check = $scope.dataPage.selectedAll;
                }
            },
            filter: function() {
                var paramFilter = [{
                    "key": "$",
                    "value": $scope.dataPage.filterText,
                    "precision": false
                }];
                $scope.dataPage.selectedItems = $filter('arrayFilter')($scope.dataPage.dataSource, paramFilter);
                $scope.dataPage.paginations.totalItems = $scope.dataPage.selectedItems.length;
                $scope.dataPage.paginations.currentPage = 1;
                $scope.dataPage.changePage();
            },
            changePage: function() {
                var firstItem = ($scope.dataPage.paginations.currentPage == 1) ? 0 : ($scope.dataPage.paginations.currentPage * $scope.dataPage.paginations.itemsPerPage) - $scope.dataPage.paginations.itemsPerPage;
                $scope.dataPage.data = $scope.dataPage.selectedItems.slice(firstItem, $scope.dataPage.paginations.currentPage * $scope.dataPage.paginations.itemsPerPage);
            },
            getData: function() {
                $scope.dataPage.data = [];
                $scope.dataPage.loading = true;
                $scope.dataPage.noData = false;
            }
        }
        $scope.fileManager.model.tipoReferencia = parameters.item.tipoReferencia;
        if ($scope.fileManager.model.tipoReferencia == "TRM") {
            $scope.fileManager.model.anio = parameters.item.anio;
            $scope.fileManager.model.mes = parameters.item.mes;
        }
        if ($scope.fileManager.model.tipoReferencia == "OIL") {
            $scope.fileManager.model.anio = parameters.item.anio;
            $scope.fileManager.model.mes = parameters.item.mes;
            $scope.fileManager.model.liquidacion = parameters.item.liquidacion;
            $scope.fileManager.model.tipoPrecio = parameters.item.tipoPrecio;
        }
        if ($scope.fileManager.model.tipoReferencia == "GAS") {
            $scope.fileManager.model.anio = parameters.item.anio;
            $scope.fileManager.model.mes = parameters.item.mes;
            $scope.fileManager.model.liquidacion = parameters.item.liquidacion;
        }
        if ($scope.fileManager.model.tipoReferencia == "CURVA") {
            $scope.motrarTabla = false;
            $scope.fileManager.model.curvaId = parameters.item.curvaId;
            $scope.fileManager.model.tipoHdr = parameters.item.tipoHdr;
        }
        /**
         * Metodo que carga un archivo dependiendo del tipo de cargue
         *
         * @method     cargueArchivo
         * @param      {<Object>}  file    { Contiene el archivo a cargar }
         */
        function cargueArchivo(file) {
            if ($scope.fileManager.model.tipoReferencia == "CURVA") {
                $rootScope.loadingVisible = true;
                console.log("Entro CURVA");
                file.upload = Upload.upload({
                    url: END_POINT + '/General.svc/uploadCurvaBase',
                    headers: {
                        Authorization: tokenManager.get()
                    },
                    data: {
                        file: file,
                        curvaId: base64.encode('' + $scope.fileManager.model.curvaId),
                        tipoHdr: base64.encode($scope.fileManager.model.tipoHdr)
                    }
                });
                file.upload.then(function(response) {
                    if (response.data.length > 0) {
                        $scope.motrarTabla = true;
                        $scope.dataPage.data = response.data;
                        message.show("success", "Se cargo archivo con errores, verifica en la tabla de errores. ");
                    } else {
                        $scope.dataPage.data = [];
                        $scope.motrarTabla = false;
                        message.show("success", "Archivo cargado correctamente. ");
                    }
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "Ocurrio un error al intentar guardar el archivo" + faild.Message);
                });
            }
            if ($scope.fileManager.model.tipoReferencia == "TRM") {
                $rootScope.loadingVisible = true;
                console.log("Entro TRM");
                file.upload = Upload.upload({
                    url: END_POINT + '/Liquidacion.svc/uploadTrm',
                    headers: {
                        Authorization: tokenManager.get()
                    },
                    data: {
                        file: file,
                        anio: base64.encode($scope.fileManager.model.anio),
                        mes: base64.encode($scope.fileManager.model.mes)
                    }
                });
                file.upload.then(function(response) {
                    if (response.data.length > 0) {
                        $scope.motrarTabla = true;
                        $scope.dataPage.data = response.data;
                        message.show("success", "Se cargo archivo con errores, verifica en la tabla de errores. ");
                    } else {
                        $scope.dataPage.data = [];
                        $scope.motrarTabla = false;
                        message.show("success", "Archivo cargado correctamente. ");
                    }
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "Ocurrio un error al intentar guardar el archivo" + faild.Message);
                });
            }
            if ($scope.fileManager.model.tipoReferencia == "OIL") {
                $rootScope.loadingVisible = true;
                console.log("Entro OIL");
                file.upload = Upload.upload({
                    url: END_POINT + '/Liquidacion.svc/uploadPrecioOil',
                    headers: {
                        Authorization: tokenManager.get()
                    },
                    data: {
                        file: file,
                        anio: base64.encode($scope.fileManager.model.anio),
                        mes: base64.encode($scope.fileManager.model.mes),
                        liquidacion: base64.encode($scope.fileManager.model.liquidacion),
                        tipoPrecio: base64.encode($scope.fileManager.model.tipoPrecio)
                    }
                });
                file.upload.then(function(response) {
                   if (response.data.length > 0) {
                        $scope.motrarTabla = true;
                        $scope.dataPage.data = response.data;
                        message.show("success", "Se cargo archivo con errores, verifica en la tabla de errores. ");
                    } else {
                        $scope.dataPage.data = [];
                        $scope.motrarTabla = false;
                        message.show("success", "Archivo cargado correctamente. ");
                    }
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "Ocurrio un error al intentar guardar el archivo" + faild.Message);
                });
            }
            if ($scope.fileManager.model.tipoReferencia == "GAS") {
                $rootScope.loadingVisible = true;
                console.log("Entro GAS");
                file.upload = Upload.upload({
                    url: END_POINT + '/Liquidacion.svc/uploadPrecioGas',
                    headers: {
                        Authorization: tokenManager.get()
                    },
                    data: {
                        file: file,
                        anio: base64.encode($scope.fileManager.model.anio),
                        mes: base64.encode($scope.fileManager.model.mes),
                        liquidacion: base64.encode($scope.fileManager.model.liquidacion)
                    }
                });
                file.upload.then(function(response) {
                    if (response.data.length > 0) {
                        $scope.motrarTabla = true;
                        $scope.dataPage.data = response.data;
                        message.show("success", "Se cargo archivo con errores, verifica en la tabla de errores. ");
                    } else {
                        $scope.dataPage.data = [];
                        $scope.motrarTabla = false;
                        message.show("success", "Archivo cargado correctamente. ");
                    }
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", "Ocurrio un error al intentar guardar el archivo" + faild.Message);
                });
            }
            $rootScope.loadingVisible = false;
        }
    }
})();
/**=========================================================
 * Module: app.forma30.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.fileManager')
    .service('fileManagerHttp', fileManagerHttp);

  fileManagerHttp.$inject = ['$resource', 'END_POINT'];


  function fileManagerHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      getDocumentos : {
        method : 'POST',
        isArray : true
      },
      getDocumento : {
        method : 'GET',
        url : END_POINT + '/General.svc/document/:id'
      },
      removeDocumento : {
        method : 'DELETE',
        isArray : true,
        url : END_POINT + '/General.svc/document/:id'
      }
    };

    return $resource(END_POINT + '/General.svc/document', paramDefault, actions);
  }

})();

/**=========================================================
 * Module: app.funcionario.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.funcionario')
    .controller('funcionarioController', funcionarioController);

  funcionarioController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'funcionarioHttp', 'ngDialog', 'tpl', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function funcionarioController($scope, $filter, $state, LDataSource, funcionarioHttp, ngDialog, tpl, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
      var funcionario = $state.params.funcionario;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      $scope.funcionario = {
		model : {
				  id: 0,
				  nombre: '',
				  empresaId: 0,
				  matricula: '',
				  email: '',
				  documento: '',
				  direccion: '',
				  estado: '',
				  fechaIngreso: '',
				  codigo: '',
				  telefono: '',
				  fechaAct: '',
				  usuarioAct: '',
				  fechaReg: '',
				  usuarioReg: '',
				  usuario: '',
				  contrasenia: '',
				  tipoAuth: '',
                  roles:[]
		},
        fechaIngreso : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.funcionario.fechaIngreso.isOpen = true;
        }
      },   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.funcionario.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.funcionario.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.funcionario', params : { filters : {}, data : []} });
        $state.go('app.funcionario');
      },
      save : function() {		  
       
		  
          		if($scope.funcionario.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
				if($scope.funcionario.model.documento ==""){message.show("warning", "Documento requerido");return;}				
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.funcionario.model.estado= $scope.estados.current.value;}
				
				if(!$scope.tipoAuth.current){message.show("warning", "Tipo autenticación requerido");return;}
				else{$scope.funcionario.model.tipoAuth= $scope.tipoAuth.current.value;}
				
				if($scope.funcionario.fechaIngreso.value==""){message.show("warning", "Fecha de ingreso requerido");return;}
				else{$scope.funcionario.model.fechaIngreso=Date.parse(new Date($scope.funcionario.fechaIngreso.value));}				
			
				$scope.funcionario.model.empresaId=1;

			
          
			//INSERTAR
            if($scope.funcionario.model.id==0){
				
				$rootScope.loadingVisible = true;
				funcionarioHttp.save({}, $scope.funcionario.model, function (data) { 
					
						funcionarioHttp.read({},{ id : data.id}, function (data) {
							$scope.funcionario.model=data;
							funcionario.id=$scope.funcionario.model.id; 
							if($scope.funcionario.model.fechaReg){$scope.funcionario.fechaReg.value=new Date(parseFloat($scope.funcionario.model.fechaReg));}    
							if($scope.funcionario.model.fechaAct){$scope.funcionario.fechaAct.value=new Date(parseFloat($scope.funcionario.model.fechaAct));} 		
								
							message.show("success", "Funcionario creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.funcionario.model.id){
					$state.go('app.funcionario');
				}else{
					
					funcionarioHttp.update({}, $scope.funcionario.model, function (data) {
					  $rootScope.loadingVisible = false;
					  $scope.funcionario.model=data;
						if($scope.funcionario.model.fechaReg){$scope.funcionario.fechaReg.value=new Date(parseFloat($scope.funcionario.model.fechaReg));}    
						if($scope.funcionario.model.fechaAct){$scope.funcionario.fechaAct.value=new Date(parseFloat($scope.funcionario.model.fechaAct));}   					  
					   message.show("success", "Funcionario actualizado satisfactoriamente");							
					  
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  
								
					
				   
				}
			}
		}
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'I', descripcion: 'Inactivo'});
            $scope.estados.data.push({value:'A', descripcion: 'Activo'});
            $scope.estados.data.push({value:'D', descripcion: 'Eliminado'});   
        }        
    }
	//TIPO AUTENTICACION
      $scope.tipoAuth = {
            current: {},
            data: [],
            getData: function() {
                $scope.tipoAuth.data.push({value: 'APP',descripcion: 'Aplicación'});
                $scope.tipoAuth.data.push({value: 'LDAP',descripcion: 'Directorio Activo'});
            }
        }
	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();
	$scope.tipoAuth.getData();
	
	
	//CARGAMOS LOS DATOS DEL FUNCIONARIO
	
	
	if(funcionario.id==0){
      $scope.estados.current=$scope.estados.data[0]; 
    }
    else{
        $rootScope.loadingVisible = true;
		
        funcionarioHttp.read({},$state.params.funcionario, function (data) { 
            $scope.funcionario.model = data;
			
            if($scope.funcionario.model.fechaIngreso){$scope.funcionario.fechaIngreso.value=new Date(parseFloat($scope.funcionario.model.fechaIngreso));}    
			if($scope.funcionario.model.fechaReg){$scope.funcionario.fechaReg.value=new Date(parseFloat($scope.funcionario.model.fechaReg));}    
			if($scope.funcionario.model.fechaAct){$scope.funcionario.fechaAct.value=new Date(parseFloat($scope.funcionario.model.fechaAct));}   
            
			//$scope.estados.current=$scope.funcionario.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.funcionario.model.estado})[0];
            $scope.tipoAuth.current = $filter('filter')($scope.tipoAuth.data, {value : $scope.funcionario.model.tipoAuth})[0];
			
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
	
    
    
  }
})();
/**=========================================================
 * Module: app.funcionario.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.funcionario')
    .service('funcionarioHttp', funcionarioHttp);

  funcionarioHttp.$inject = ['$resource', 'END_POINT'];


  function funcionarioHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/funcionario/:id'
      },
      'save':   {
        'method':'POST'
      },
      'update' : {
        'method' : 'PUT'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/funcionario/:id'
      }        
     
    };
    return $resource( END_POINT + '/General.svc/funcionario', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.funcionario.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.funcionario')
    .controller('funcionarioListController', funcionarioListController);

  funcionarioListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'funcionarioHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function funcionarioListController($scope, $filter, $state, ngDialog, tpl, funcionarioHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
		
	$scope.ESTADO_INACTIVO = 'I';
    $scope.ESTADO_ACTIVO = 'A';
    $scope.ESTADO_ELIMINADO = 'D';
	
	
	//$rootScope.loadingVisible = true;

    $scope.funcionarios = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrefuncionarios:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.funcionarios.selectedAll = !$scope.funcionarios.selectedAll; 
        for (var key in $scope.funcionarios.selectedItems) {
          $scope.funcionarios.selectedItems[key].check = $scope.funcionarios.selectedAll;
        }
      },
      add : function() {
          var func = {
              id: 0,
              nombre: '',
              empresaId: 0,
			  matricula: '',
			  email: '',
			  documento: '',
			  direccion: '',
			  estado: '',
			  fechaIngreso: '',
			  codigo: '',
			  telefono: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: '',
			  usuario: '',
			  contrasenia: '',
			  tipoAuth: ''
          };
        parametersOfState.set({ name : 'app.funcionario_add', params : { funcionario: func } });
        $state.go('app.funcionario_add');
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.funcionario_edit', params : { funcionario: item } });
        $state.go('app.funcionario_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            funcionarioHttp.remove({}, { id: id }, function(response) {
                $scope.funcionarios.getData();
                message.show("success", "Funcionario eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.funcionarios.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    funcionarioHttp.remove({}, { id: id }, function(response) {
                        $scope.funcionarios.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.funcionarios.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.funcionarios.filterText,
          "precision": false
        }];
        $scope.funcionarios.selectedItems = $filter('arrayFilter')($scope.funcionarios.dataSource, paramFilter);
        $scope.funcionarios.paginations.totalItems = $scope.funcionarios.selectedItems.length;
        $scope.funcionarios.paginations.currentPage = 1;
        $scope.funcionarios.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.funcionarios.paginations.currentPage == 1 ) ? 0 : ($scope.funcionarios.paginations.currentPage * $scope.funcionarios.paginations.itemsPerPage) - $scope.funcionarios.paginations.itemsPerPage;
        $scope.funcionarios.data = $scope.funcionarios.selectedItems.slice(firstItem , $scope.funcionarios.paginations.currentPage * $scope.funcionarios.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.funcionarios.data = [];
        $scope.funcionarios.loading = true;
        $scope.funcionarios.noData = false;
        funcionarioHttp.getList(function(response) {
          $scope.funcionarios.selectedItems = response;
          $scope.funcionarios.dataSource = response;
          for(var i=0; i<$scope.funcionarios.dataSource.length; i++){
            $scope.funcionarios.nombrefuncionarios.push({id: i, nombre: $scope.funcionarios.dataSource[i]});
          }
          $scope.funcionarios.paginations.totalItems = $scope.funcionarios.selectedItems.length;
          $scope.funcionarios.paginations.currentPage = 1;
          $scope.funcionarios.changePage();
          $scope.funcionarios.loading = false;
          ($scope.funcionarios.dataSource.length < 1) ? $scope.funcionarios.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
	//CARGAMOS LOS LISTADOS
    $scope.funcionarios.getData();
	}
  
  
  })();
(function() {
    'use strict';

    angular
        .module('app.loadingbar')
        .config(loadingbarConfig)
        ;
    loadingbarConfig.$inject = ['cfpLoadingBarProvider'];
    function loadingbarConfig(cfpLoadingBarProvider){
      cfpLoadingBarProvider.includeBar = true;
      cfpLoadingBarProvider.includeSpinner = false;
      cfpLoadingBarProvider.latencyThreshold = 500;
      cfpLoadingBarProvider.parentSelector = '.wrapper > section';
    }
})();
(function() {
    'use strict';

    angular
        .module('app.loadingbar')
        .run(loadingbarRun)
        ;
    loadingbarRun.$inject = ['$rootScope', '$timeout', 'cfpLoadingBar'];
    function loadingbarRun($rootScope, $timeout, cfpLoadingBar){

      // Loading bar transition
      // ----------------------------------- 
      var thBar;
      $rootScope.$on('$stateChangeStart', function() {
        if($('.wrapper > section').length) // check if bar container exists
            thBar = $timeout(function() {
              cfpLoadingBar.start();
            }, 0); // sets a latency Threshold
      });
      $rootScope.$on('$stateChangeSuccess', function(event) {
          event.targetScope.$watch('$viewContentLoaded', function () {
            $timeout.cancel(thBar);
            cfpLoadingBar.complete();
          });
      });

    }

})();
(function() {
    'use strict';

    angular
        .module('app.lazyload')
        .config(lazyloadConfig);

    lazyloadConfig.$inject = ['$ocLazyLoadProvider', 'APP_REQUIRES'];
    function lazyloadConfig($ocLazyLoadProvider, APP_REQUIRES){

      // Lazy Load modules configuration
      $ocLazyLoadProvider.config({
        debug: false,
        events: true,
        modules: APP_REQUIRES.modules
      });

    }
})();
(function() {
  'use strict';

  angular
    .module('app.lazyload')
    .constant('APP_REQUIRES', {
    // jQuery based and standalone scripts
    scripts: {
      'whirl':              ['vendor/whirl/dist/whirl.css'],
      'classyloader':       ['vendor/jquery-classyloader/js/jquery.classyloader.min.js'],
      'animo':              ['vendor/animo.js/animo.js'],
      'fastclick':          ['vendor/fastclick/lib/fastclick.js'],
      'modernizr':          ['vendor/modernizr/modernizr.js'],
      'animate':            ['vendor/animate.css/animate.min.css'],
      'skycons':            ['vendor/skycons/skycons.js'],
      'icons':              ['vendor/fontawesome/css/font-awesome.min.css',
                             'vendor/simple-line-icons/css/simple-line-icons.css'],
      'weather-icons':      ['vendor/weather-icons/css/weather-icons.min.css'],
      'sparklines':         ['app/vendor/sparklines/jquery.sparkline.min.js'],
      'wysiwyg':            ['vendor/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
                             'vendor/bootstrap-wysiwyg/external/jquery.hotkeys.js'],
      'slimscroll':         ['vendor/slimScroll/jquery.slimscroll.min.js'],
      'screenfull':         ['vendor/screenfull/dist/screenfull.js'],
      'vector-map':         ['vendor/ika.jvectormap/jquery-jvectormap-1.2.2.min.js',
                             'vendor/ika.jvectormap/jquery-jvectormap-1.2.2.css'],
      'vector-map-maps':    ['vendor/ika.jvectormap/jquery-jvectormap-world-mill-en.js',
                             'vendor/ika.jvectormap/jquery-jvectormap-us-mill-en.js'],
      'loadGoogleMapsJS':   ['app/vendor/gmap/load-google-maps.js'],
      'flot-chart':         ['vendor/Flot/jquery.flot.js'],
      'flot-chart-plugins': ['vendor/flot.tooltip/js/jquery.flot.tooltip.min.js',
                             'vendor/Flot/jquery.flot.resize.js',
                             'vendor/Flot/jquery.flot.pie.js',
                             'vendor/Flot/jquery.flot.time.js',
                             'vendor/Flot/jquery.flot.categories.js',
                             'vendor/flot-spline/js/jquery.flot.spline.min.js'],
      // jquery core and widgets
      'jquery-ui':          ['vendor/jquery-ui/ui/core.js',
                             'vendor/jquery-ui/ui/widget.js'],
      // loads only jquery required modules and touch support
      'jquery-ui-widgets':  ['vendor/jquery-ui/ui/core.js',
                             'vendor/jquery-ui/ui/widget.js',
                             'vendor/jquery-ui/ui/mouse.js',
                             'vendor/jquery-ui/ui/draggable.js',
                             'vendor/jquery-ui/ui/droppable.js',
                             'vendor/jquery-ui/ui/sortable.js',
                             'vendor/jqueryui-touch-punch/jquery.ui.touch-punch.min.js'],
      'moment' :            ['vendor/moment/min/moment-with-locales.min.js'],
      'inputmask':          ['vendor/jquery.inputmask/dist/jquery.inputmask.bundle.min.js'],
      'flatdoc':            ['vendor/flatdoc/flatdoc.js'],
      'codemirror':         ['vendor/codemirror/lib/codemirror.js',
                             'vendor/codemirror/lib/codemirror.css'],
      // modes for common web files
      'codemirror-modes-web': ['vendor/codemirror/mode/javascript/javascript.js',
                               'vendor/codemirror/mode/xml/xml.js',
                               'vendor/codemirror/mode/htmlmixed/htmlmixed.js',
                               'vendor/codemirror/mode/css/css.js'],
      'taginput' :          ['vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                             'vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js'],
      'filestyle':          ['vendor/bootstrap-filestyle/src/bootstrap-filestyle.js'],
      'parsley':            ['vendor/parsleyjs/dist/parsley.min.js'],
      'fullcalendar':       ['vendor/fullcalendar/dist/fullcalendar.min.js',
                             'vendor/fullcalendar/dist/fullcalendar.css'],
      'gcal':               ['vendor/fullcalendar/dist/gcal.js'],
      'chartjs':            ['vendor/Chart.js/Chart.js'],
      'morris':             ['vendor/raphael/raphael.js',
                             'vendor/morris.js/morris.js',
                             'vendor/morris.js/morris.css'],
      'loaders.css':          ['vendor/loaders.css/loaders.css'],
      'spinkit':              ['vendor/spinkit/css/spinkit.css'],
      'ngImgCrop':            ['vendor/ng-img-crop/compile/unminified/ng-img-crop.js',
                               'vendor/ng-img-crop/compile/unminified/ng-img-crop.css'],
      'toastr':               ['vendor/toastr/toastr.js',
                              'vendor/toastr/toastr.css']
    },
    // Angular based script (use the right module name)
    modules: [
      {name: 'localytics.directives',     files: ['vendor/chosen_v1.2.0/chosen.jquery.min.js',
                                                  'vendor/chosen_v1.2.0/chosen.min.css',
                                                  'vendor/angular-chosen-localytics/chosen.js']},
      {name: 'ngDialog',                  files: ['vendor/ngDialog/js/ngDialog.min.js',
                                                  'vendor/ngDialog/css/ngDialog.min.css',
                                                  'vendor/ngDialog/css/ngDialog-theme-default.min.css'] },
      {name: 'ngWig',                     files: ['vendor/ngWig/dist/ng-wig.min.js'] },
      {name: 'ngTable',                   files: ['vendor/ng-table/dist/ng-table.min.js',
                                                  'vendor/ng-table/dist/ng-table.min.css']},
      {name: 'ngTableExport',             files: ['vendor/ng-table-export/ng-table-export.js']},
      {name: 'angularBootstrapNavTree',   files: ['vendor/angular-bootstrap-nav-tree/dist/abn_tree_directive.js',
                                                  'vendor/angular-bootstrap-nav-tree/dist/abn_tree.css']},
      {name: 'htmlSortable',              files: ['vendor/html.sortable/dist/html.sortable.js',
                                                  'vendor/html.sortable/dist/html.sortable.angular.js']},
      {name: 'xeditable',                 files: ['vendor/angular-xeditable/dist/js/xeditable.js',
                                                  'vendor/angular-xeditable/dist/css/xeditable.css']},
      {name: 'angularFileUpload',         files: ['vendor/angular-file-upload/angular-file-upload.js']},
      {name: 'ngFileUpload',              files: ['vendor/ng-file-upload-master/dist/ng-file-upload.min.js']},
      {name: 'ngImgCrop',                 files: ['vendor/ng-img-crop/compile/unminified/ng-img-crop.js',
                                                  'vendor/ng-img-crop/compile/unminified/ng-img-crop.css']},
      {name: 'ui.select',                 files: ['vendor/angular-ui-select/dist/select.js',
                                                  'vendor/angular-ui-select/dist/select.css']},
      {name: 'ui.codemirror',             files: ['vendor/angular-ui-codemirror/ui-codemirror.js']},
      {name: 'angular-carousel',          files: ['vendor/angular-carousel/dist/angular-carousel.css',
                                                  'vendor/angular-carousel/dist/angular-carousel.js']},
      {name: 'ngGrid',                    files: ['vendor/ng-grid/build/ng-grid.min.js',
                                                  'vendor/ng-grid/ng-grid.css' ]},
      {name: 'infinite-scroll',           files: ['vendor/ngInfiniteScroll/build/ng-infinite-scroll.js']},
      {name: 'ui.bootstrap-slider',       files: ['vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js',
                                                  'vendor/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css',
                                                  'vendor/angular-bootstrap-slider/slider.js']},
      {name: 'ui.grid',                   files: ['vendor/angular-ui-grid/ui-grid.min.css',
                                                  'vendor/angular-ui-grid/ui-grid.min.js']},
      {name: 'textAngular',               files: ['vendor/textAngular/dist/textAngular.css',
                                                  'vendor/textAngular/dist/textAngular-rangy.min.js',
                                                  'vendor/textAngular/dist/textAngular-sanitize.js',
                                                  'vendor/textAngular/src/globals.js',
                                                  'vendor/textAngular/src/factories.js',
                                                  'vendor/textAngular/src/DOM.js',
                                                  'vendor/textAngular/src/validators.js',
                                                  'vendor/textAngular/src/taBind.js',
                                                  'vendor/textAngular/src/main.js',
                                                  'vendor/textAngular/dist/textAngularSetup.js'
                                                 ], serie: true},
      {name: 'angular-rickshaw',          files: ['vendor/d3/d3.min.js',
                                                  'vendor/rickshaw/rickshaw.js',
                                                  'vendor/rickshaw/rickshaw.min.css',
                                                  'vendor/angular-rickshaw/rickshaw.js'], serie: true},
      {name: 'angular-chartist',          files: ['vendor/chartist/dist/chartist.min.css',
                                                  'vendor/chartist/dist/chartist.js',
                                                  'vendor/angular-chartist.js/dist/angular-chartist.js'], serie: true},
      {name: 'ui.map',                    files: ['vendor/angular-ui-map/ui-map.js']},
      {name: 'datatables',                files: ['vendor/datatables/media/css/jquery.dataTables.css',
                                                  'vendor/datatables/media/js/jquery.dataTables.js',
                                                  'vendor/angular-datatables/dist/angular-datatables.js'], serie: true},
      {name: 'angular-jqcloud',           files: ['vendor/jqcloud2/dist/jqcloud.css',
                                                  'vendor/jqcloud2/dist/jqcloud.js',
                                                  'vendor/angular-jqcloud/angular-jqcloud.js']},
      {name: 'angularGrid',               files: ['vendor/ag-grid/dist/angular-grid.css',
                                                  'vendor/ag-grid/dist/angular-grid.js',
                                                  'vendor/ag-grid/dist/theme-dark.css',
                                                  'vendor/ag-grid/dist/theme-fresh.css']},
      {name: 'ng-nestable',               files: ['vendor/ng-nestable/src/angular-nestable.js',
                                                  'vendor/nestable/jquery.nestable.js']},
      {name: 'akoenig.deckgrid',          files: ['vendor/angular-deckgrid/angular-deckgrid.js']},
      {name: 'oitozero.ngSweetAlert',     files: ['vendor/sweetalert/dist/sweetalert.css',
                                                  'vendor/sweetalert/dist/sweetalert.min.js',
                                                  'vendor/angular-sweetalert/SweetAlert.js']},
      {name: 'bm.bsTour',                 files: ['vendor/bootstrap-tour/build/css/bootstrap-tour.css',
                                                  'vendor/bootstrap-tour/build/js/bootstrap-tour-standalone.js',
                                                  'vendor/angular-bootstrap-tour/dist/angular-bootstrap-tour.js'], serie: true},
      {name: 'al-ui', files : ['vendor/al-ui/al-ui.js']},
      {name: 'ab-base64', files : ['vendor/angular-utf8-base64/angular-utf8-base64.js']},
    { name: 'ngProgress', files : ['vendor/ngprogress/build/ngprogress.js',
                             'vendor/ngprogress/ngProgress.css']},
      {name: 'angular.bootstrap.datepicker',     files: ['vendor/angular-bootstrap-datepicker/ui-bootstrap-datepicker-0.14.3','vendor/angular-bootstrap-datepicker/ui-bootstrap-datepicker-tpls-0.14.3']},
      {name: 'al-ui', files : ['vendor/al-ui/al-ui.js']},
      {name: 'angular.morris-chart', files : ['vendor/angular-morris-chart/src/angular-morris-chart.min.js']},
      {name: 'ui.utils.masks', files : ['vendor/angular-input-masks/angular-input-masks-standalone.min.js']}
    ]
  })
  ;

})();

/**=========================================================
 * Module: demo-pagination.js
 * Provides a simple demo for pagination
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.mailbox')
        .controller('MailboxController', MailboxController);

    function MailboxController() {
        var vm = this;

        activate();

        ////////////////

        function activate() {
          vm.folders = [
            {name: 'Bandeja Entrada',   folder: 'inbox',   alert: 42, icon: 'fa-inbox' },
            {name: 'Notificaciones', folder: 'notificacion', alert: 10, icon: 'fa-bell-o' },
            {name: 'Enviados',    folder: 'enviados',    alert: 0,  icon: 'fa-paper-plane-o' },
            {name: 'Destacados',   folder: 'destacados',   alert: 5,  icon: 'fa-star-o' },
            {name: 'Eliminados',   folder: 'eliminados',   alert: 0,  icon: 'fa-trash-o'}
          ];

          /*
          vm.labels = [
            {name: 'Red',     color: 'danger'},
            {name: 'Pink',    color: 'pink'},
            {name: 'Blue',    color: 'info'},
            {name: 'Yellow',  color: 'warning'}
          ];
          */

          vm.mail = {
            cc: false,
            bcc: false
          };
          
          
          // Mailbox editr initial content
          vm.content = '<p>Type something..</p>';
          
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('app.mailbox')
        .controller('MailFolderController', MailFolderController);

    MailFolderController.$inject = ['mails', '$stateParams'];
    function MailFolderController(mails, $stateParams) {
        var vm = this;

        activate();

        ////////////////

        function activate() {
          debugger;
          vm.folder = {};
          // no filter for inbox
          vm.folder.folder = $stateParams.folder === 'inbox' ? '' : $stateParams.folder;

          mails.all().then(function(mails){
            vm.mails = mails;
          });
        }
    }
})();

// A RESTful factory for retrieving mails from json file

(function() {
    'use strict';

    angular
        .module('app.mailbox')
        .factory('mails', mails);

    mails.$inject = ['$http'];
    function mails($http) {
        var service = {
            all: all,
            get: get
        };
        return service;

        ////////////////
        
        function readMails() {
          var path = 'http://north-legacy.codio.io:8080/api/v1/anh/news';
          return $http.get(path).then(function (resp) {
            return resp.data.mails;
          });
        }

        function all() {
          return readMails();
        }

        function get(id) {
          return readMails().then(function(mails){
            for (var i = 0; i < mails.length; i++) {
              if (+mails[i].id === +id) return mails[i];
            }
            return null;
          });
        }
    }
})();


(function() {
  'use strict';

  angular
    .module('app.mailbox')
    .controller('MailViewController', MailViewController);

  MailViewController.$inject = ['mails', '$stateParams'];
  function MailViewController(mails, $stateParams) {
    debugger;
    var vm = this;

    activate();

    ////////////////

    function activate() {
      debugger;
      mails.get($stateParams.mid).then(function(mail){
        vm.mail = mail;
      });
    }
    
    
  }
})();

/**=========================================================
 * Module: app.mensajeria.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.mensajeria')
    .controller('mensajeriaController', mensajeriaController);

  mensajeriaController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'mensajeriaHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function mensajeriaController($scope, $filter, $state, LDataSource, mensajeriaHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var mensajeria = $state.params.mensajeria;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.mensajeria = {
		model : {
                id: 0,
                terceroId: 0,                      
                consecutivo: '',                     
                estado: '',
                descripcion: '',                  
                fechaRec: '',
                tipo: '1',
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: ''		
		},   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.mensajeria.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.mensajeria.fechaAct.isOpen = false;
        }
      },	
       fechaRec : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.mensajeria.fechaRec.isOpen = true;
        }
      },
      back : function() {
          parametersOfState.set({ name : 'app.mensajeria', params : { filters : {}, data : []} });
        $state.go('app.mensajeria');
          
      },
      save : function() {				  
               
            if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
            }else{$scope.mensajeria.model.terceroId = $scope.tercerosOp.current.id;}          

            if($scope.mensajeria.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			

            if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
            else{$scope.mensajeria.model.estado= $scope.estados.current.value;}		
          
            if(!$scope.jornadas.current){message.show("warning", "Jornada requerida");return;}
            else{$scope.mensajeria.model.jornada= $scope.jornadas.current.value;}	
          
            if(!$scope.tipos.current){message.show("warning", "Tipo requerido");return;}
            else{$scope.mensajeria.model.tipo= $scope.tipos.current.value;}		

            $scope.mensajeria.model.fechaRec=Date.parse(new Date($scope.mensajeria.fechaRec.value));
              
			//INSERTAR
            if($scope.mensajeria.model.id==0){
				
				$rootScope.loadingVisible = true;
				mensajeriaHttp.save({}, $scope.mensajeria.model, function (data) { 
					
						mensajeriaHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.mensajeria.model=data;
							mensajeria.id=$scope.mensajeria.model.id; 
                            
							if($scope.mensajeria.model.fechaReg){$scope.mensajeria.fechaReg.value=new Date(parseFloat($scope.mensajeria.model.fechaReg));}  
							if($scope.mensajeria.model.fechaAct){$scope.mensajeria.fechaAct.value=new Date(parseFloat($scope.mensajeria.model.fechaAct));} 	
                            if($scope.mensajeria.model.fechaRec){$scope.mensajeria.fechaRec.value=new Date(parseFloat($scope.mensajeria.model.fechaRec));} 	
                            
                           
                           
                            
                            $scope.btnAdjunto=true;
                                
								
							message.show("success", "Información registrada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.mensajeria.model.id){
					$state.go('app.mensajeria');
				}else{
					
					mensajeriaHttp.update({}, $scope.mensajeria.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.mensajeria.model=data;
						if($scope.mensajeria.model.fechaReg){$scope.mensajeria.fechaReg.value=new Date(parseFloat($scope.mensajeria.model.fechaReg));}    
						if($scope.mensajeria.model.fechaAct){$scope.mensajeria.fechaAct.value=new Date(parseFloat($scope.mensajeria.model.fechaAct));} 
                        if($scope.mensajeria.model.fechaRec){$scope.mensajeria.fechaRec.value=new Date(parseFloat($scope.mensajeria.model.fechaRec));}                       
                        
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                            message.show("success", "Información actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.mensajeria.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.mensajeria.model.id,
                         referencia : 'mensajeria' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/mensajeria/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.mensajeria.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.mensajeria.model.id,
                         referencia : 'mensajeria' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }        
          
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrado'});             
        }        
    }
    //TIPO
    $scope.tipos = {
        current : {}, data:[],
        getData : function() {
            $scope.tipos.data.push({value:'1', descripcion: 'Recibida'});
            $scope.tipos.data.push({value:'2', descripcion: 'Programada'});                    
        }        
    }
    //JORNADAS
    $scope.jornadas = {
        current : {}, data:[],
        getData : function() {
            $scope.jornadas.data.push({value:'M', descripcion: 'Mañana'});
            $scope.jornadas.data.push({value:'T', descripcion: 'Tarde'});                         
        }        
    }
    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            mensajeriaHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.mensajeria.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.mensajeria.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.mensajeria.model.terceroId=$scope.tercerosOp.current.id;
      }
    }

	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();
    $scope.jornadas.getData();
    $scope.tipos.getData();	  
    $scope.tercerosOp.getData();     
      
   
  
	
	//CARGAMOS LOS DATOS DEL mensajeria	
	
	if(mensajeria.id==0){
        
      $scope.estados.current=$scope.estados.data[0]; 
      $scope.jornadas.current=$scope.jornadas.data[0]; 
      $scope.tipos.current=$scope.tipos.data[0]; 
      var date = new Date(); 
      $scope.mensajeria.fechaRec.value=new Date(date.getFullYear(), date.getMonth(), date.getDate());
          
        
    }
    else{   
        $rootScope.loadingVisible = true;
		
            mensajeriaHttp.read({},$state.params.mensajeria, function (data) { 
                
            $scope.mensajeria.model = data;	
            $scope.btnAdjunto=true;            
               
			if($scope.mensajeria.model.fechaReg){$scope.mensajeria.fechaReg.value=new Date(parseFloat($scope.mensajeria.model.fechaReg));}    
			if($scope.mensajeria.model.fechaAct){$scope.mensajeria.fechaAct.value=new Date(parseFloat($scope.mensajeria.model.fechaAct));}  
            if($scope.mensajeria.model.fechaRec){$scope.mensajeria.fechaRec.value=new Date(parseFloat($scope.mensajeria.model.fechaRec));}  
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.mensajeria.model.terceroId })[0]; 
			//$scope.estados.current=$scope.mensajeria.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.mensajeria.model.estado})[0];
            $scope.jornadas.current = $filter('filter')($scope.jornadas.data, {value : $scope.mensajeria.model.jornada})[0];    
            $scope.tipos.current = $filter('filter')($scope.tipos.data, {value : $scope.mensajeria.model.tipo})[0];          
                    
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      

      
        
	
    
    
  }
})();
/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.mensajeria')
    .service('mensajeriaHttp', facturaHttp);

  facturaHttp.$inject = ['$resource', 'END_POINT'];


  function facturaHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,       
          'url' : END_POINT + '/Cliente.svc/mensajeria/:id'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/mensajeria/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/mensajeria'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Cliente.svc/mensajeria'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/mensajeria/:id'
      }, 
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroOportunidad'
      }
     
    };
    return $resource( END_POINT + '/Cliente.svc/mensajeria', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.mensajeria.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.mensajeria')
    .controller('mensajeriaListController', mensajeriaListController);

  mensajeriaListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'mensajeriaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function mensajeriaListController($scope, $filter, $state, ngDialog, tpl, mensajeriaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {	
		
	$scope.ESTADO_PROCESO = 'P';
    $scope.ESTADO_ANULADO = 'A';
    $scope.ESTADO_CERRADO= 'C';	
    $scope.JORNADA_MANANA = 'M';
    $scope.JORNADA_TARDE = 'T';	

    $scope.mensajerias = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombremensajerias:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.mensajerias.selectedAll = !$scope.mensajerias.selectedAll; 
        for (var key in $scope.mensajerias.selectedItems) {
          $scope.mensajerias.selectedItems[key].check = $scope.mensajerias.selectedAll;
        }
      },
      add : function() {
        
       
            
              var mens = {
                id: 0,
                terceroId: 0,                      
                consecutivo: '',                     
                estado: '',
                descripcion: '',                  
                fechaRec: '',    
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.mensajeria_add', params : { mensajeria: mens } });
            $state.go('app.mensajeria_add');
      
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.mensajeria_edit', params : { mensajeria: item } });
        $state.go('app.mensajeria_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            mensajeriaHttp.remove({}, { id: id }, function(response) {
                $scope.mensajerias.getData();
                message.show("success", "mensajeria eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.mensajerias.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    mensajeriaHttp.remove({}, { id: id }, function(response) {
                        $scope.mensajerias.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.mensajerias.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.mensajerias.filterText,
          "precision": false
        }];
        $scope.mensajerias.selectedItems = $filter('arrayFilter')($scope.mensajerias.dataSource, paramFilter);
        $scope.mensajerias.paginations.totalItems = $scope.mensajerias.selectedItems.length;
        $scope.mensajerias.paginations.currentPage = 1;
        $scope.mensajerias.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.mensajerias.paginations.currentPage == 1 ) ? 0 : ($scope.mensajerias.paginations.currentPage * $scope.mensajerias.paginations.itemsPerPage) - $scope.mensajerias.paginations.itemsPerPage;
        $scope.mensajerias.data = $scope.mensajerias.selectedItems.slice(firstItem , $scope.mensajerias.paginations.currentPage * $scope.mensajerias.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.mensajerias.data = [];
        $scope.mensajerias.loading = true;
        $scope.mensajerias.noData = false;
       
        mensajeriaHttp.getList({},{} ,function(response) {
          $scope.mensajerias.selectedItems = response;
          $scope.mensajerias.dataSource = response;
          for(var i=0; i<$scope.mensajerias.dataSource.length; i++){
            $scope.mensajerias.nombremensajerias.push({id: i, nombre: $scope.mensajerias.dataSource[i]});
          }
          $scope.mensajerias.paginations.totalItems = $scope.mensajerias.selectedItems.length;
          $scope.mensajerias.paginations.currentPage = 1;
          $scope.mensajerias.changePage();
          $scope.mensajerias.loading = false;
          ($scope.mensajerias.dataSource.length < 1) ? $scope.mensajerias.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }

	//CARGAMOS LOS TIPOS DE PROCESO
    
    $scope.mensajerias.getData();  

        
	}
  
  
  })();
/**=========================================================
 * Module: app.setMenu
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.setMenu')
    .controller('menuAddController', menuAddController);

  menuAddController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'parametersOfState', 'REGULAR_EXPRESION', 'menuRolHttp', 'message', '$rootScope', 'parameters', '$modalInstance'];


  function menuAddController($scope, $filter, $state, LDataSource, $modal, parametersOfState, REGULAR_EXPRESION, menuRolHttp, message, $rootScope, parameters, $modalInstance){
    $scope.datosParams= parameters;
    var datos=$scope.datosParams.detalle;
    
    $scope.menu= {
        model : {
            alert: '',
            icon: '',
            index: 0,
            label: '',
            orden: 0,
            padreId: 0,
            param: '',
            sref: '',
            submenu: null,
            text: '',
            urlParam: '',
            menuName:''
        },
        add: function(){
          if(!$scope.menu.model.label){message.show("error", "Label Requerido..");return;}
          if($scope.menu.model.label==""){message.show("error", "Label Requerido");return;}
          if(!$scope.menu.model.text){message.show("error", "Texto Requerido");return;}
          if($scope.menu.model.text==""){message.show("error", "Texto Requerido");return;}
          
          var paramatros = {
            alert: '',
            icon: $scope.menu.model.name,
            index: $scope.menu.model.index,
            label: $scope.menu.model.label,
            orden: $scope.menu.model.index,
            padreId: $scope.menu.model.padreId,
            param: $scope.menu.model.param,
            sref: $scope.menu.model.sref,
            submenu: null,
            text: $scope.menu.model.text,
            urlParam: $scope.menu.model.urlParam
          };
          
          $rootScope.loadingVisible = true;
          
          var _Http = new menuRolHttp($scope.menu.model);
           _Http.$newMenu (function(response) {
             var parametros = { menuId:response.menuId, rolId:1, seleccionado:true};
             var _HttpRol = new menuRolHttp(parametros);
             $rootScope.loadingVisible = true;  
             _HttpRol.$assignRol(function(response) {}, function(faild) {});
            $rootScope.loadingVisible = false;
            if(datos.padreId!=0){
              message.show("success", "Submenú Agregado");
            }else{
              message.show("success", "Menú Agregado");
            }
            $scope.menu.close();

          }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", "No se creó el Menu: " + faild.Message);
          })//*/
        },
        edit: function(){
          
          $rootScope.loadingVisible = true;
            
          var _Http = new menuRolHttp($scope.menu.model);
          
          _Http.$updateMenu(function(response) {
            $rootScope.loadingVisible = false;
            if(datos.padreId!=0){
              message.show("success", "Submenú Editado");
            }else{
              message.show("success", "Menú Editado");
            }
            $scope.menu.close();

          }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", "No se cargaron los Menus: " + faild.Message);
          })
        },
        dismiss : function() {
            $modalInstance.dismiss('cancel');
        },
        close : function() {
            $modalInstance.close('close');
        }
    }
    $scope.menu.model=datos;
  }
})();
/**=========================================================
 * Module: app.setMenu
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.setMenu')
    .controller('menuRolController', menuRolController);

  menuRolController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'parametersOfState', 'REGULAR_EXPRESION', 'sidebarHttp', 'UsuarioHttp'];


  function menuRolController($scope, $filter, $state, LDataSource, $modal, parametersOfState, REGULAR_EXPRESION, sidebarHttp, UsuarioHttp){
    
    $scope.menu= {
        current : {},
        data:[],
        getData: function(){
            sidebarHttp.getMenu({},{},function(response) {
                $scope.menu.data = $filter('orderBy')(response, 'menuId');
                for(var i = 0; i < $scope.menu.data.length; i++){
                    $scope.menu.data[i].submenu = $filter('orderBy')($scope.menu.data[i].submenu, 'menuId');
                }
            }, function(faild) {
                $scope.menu.data = [];
                message.show("error", "No se cargaron los Menus: " + faild.Message);
            })
        }, edit: function (item){
            var parameter = {id:0, nombreOperadora:$scope.operadora.model.nombre, empresaId:$scope.operadora.model.id};
            var modalInstance = $modal.open({
                templateUrl: 'app/views/menu/menuRol_edit.html',
                controller: 'menuRolController',
                size: 'lg',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {}, function (parameters) {$scope.menu.getData();});
        }
    }
    $scope.roles= {
        current : {},
        data:[],
        getData: function(){
            UsuarioHttp.consultarPerfilesAll({}, {}, function(response) {
                $scope.roles.data = response;
            }, function(faild) {
                message.show("error", "No se cargaron los perfiles : " + faild.Message);
            });
        }
    }
    
    $scope.menu.getData();
    $scope.roles.getData();
  }
})();
/**=========================================================
 * Module: app.setMenu.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.setMenu')
    .service('menuRolHttp', menuRolHttp);

  menuRolHttp.$inject = ['$resource', 'END_POINT'];


  function menuRolHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getListMenu' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/menu'
      },
      'getMenuRol' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/menu/:id/rol'
      },
      'getRol' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/rol'
      },
      'updateMenu' : {///
        'method' : 'PUT',
        'url' : END_POINT + '/General.svc/menu'
      },
      'removeMenu':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/menu/:id'
      },
      'newMenu':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/menu/'
      },
      'deleteMenu': {
          'method':'DELETE',
          'isArray' : true,
          'params' : {
            menuId : '@menuId'
          },
          'url' : END_POINT + '/General.svc/menu/:menuId'
      },
      'deleteRol': {
          'method':'DELETE',
          'params' : {
            rolId : '@rolId'
          },
          'url' : END_POINT + '/General.svc/rol/:rolId'
      },
      'assignRol':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/menu/rol'
      },
      'unassignRol':   {
        'method':'PUT',
        'url' : END_POINT + '/General.svc/menu/rol'
      }
    };
    return $resource( END_POINT + '/General.svc/menu', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.setMenu
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.setMenu')
    .controller('setMenuController', setMenuController);

  setMenuController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'tpl', 'ngDialog', 'parametersOfState', 'REGULAR_EXPRESION', 'menuRolHttp', 'message', '$rootScope'];


  function setMenuController($scope, $filter, $state, LDataSource, $modal, tpl, ngDialog, parametersOfState, REGULAR_EXPRESION, menuRolHttp, message, $rootScope){
    $scope.menuName='';
    $scope.menuId=0;
    $scope.nameMenu='';
    $scope.menu= {
        current : {
          menuName:'',
          menuId:0
        },
        data:[],
        getData: function(){
            $scope.menuName='';
            $scope.menuId=0;
            $scope.roles.data=[];
            $rootScope.loadingVisible = true;
            menuRolHttp.getListMenu({},{},function(response) {
                $scope.menu.data = $filter('orderBy')(response, 'menuId');
                for(var i = 0; i < $scope.menu.data.length; i++){
                    $scope.menu.data[i].submenu = $filter('orderBy')($scope.menu.data[i].submenu, 'menuId');
                    $scope.menu.data[i].menuName= $scope.menu.data[i].label;
                    $scope.menu.data[i].show=false;
                    if($scope.menu.data[i].submenu!=null){
                        for(var j=0; j<$scope.menu.data[i].submenu.length; j++){
                            $scope.menu.data[i].submenu[j].menuName=$scope.menu.data[i].menuName + "/" + $scope.menu.data[i].submenu[j].label;
                            $scope.menu.data[i].submenu[j].show=false;
                            if($scope.menu.data[i].submenu[j].submenu!=null){
                                $scope.menu.data[i].submenu[j].submenu = $filter('orderBy')($scope.menu.data[i].submenu[j].submenu, 'menuId');
                                for(var k=0; k<$scope.menu.data[i].submenu[j].submenu.length; k++){
                                    $scope.menu.data[i].submenu[j].submenu[k].show=false;
                                    $scope.menu.data[i].submenu[j].submenu[k].menuName=$scope.menu.data[i].label + "/" + $scope.menu.data[i].submenu[j].label + "/" + $scope.menu.data[i].submenu[j].submenu[k].label;
                                }
                            }
                        }
                    }
                }
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $scope.menu.data = [];
                $rootScope.loadingVisible = false;
                message.show("error", "No se cargaron los Menus: " + faild.Message);
            })
        }, edit: function (item){
            var parametros={
              detalle:{},
              edit:true,
              menuName:'Menú'
            };
            parametros.detalle=item;
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/menu/menu_add.html',
                controller: 'menuAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {$scope.menu.getData();}, function (parameters) {$scope.menu.getData();});
        }, addSubmenu: function (item){
            var parametros={
              detalle:{
                alert: '',
                icon: '',
                index: 0,
                label: '',
                orden: 0,
                padreId: item.menuId,
                param: '',
                sref: '',
                submenu: null,
                text: '',
                urlParam: ''
              },
              edit:false,
              menuName:'Submenú'
            };
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/menu/menu_add.html',
                controller: 'menuAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {$scope.menu.getData();}, function (parameters) {$scope.menu.getData();});
            
        }, addMenu: function (){
            var parametros={
              detalle:{
                alert: '',
                icon: '',
                index: 0,
                label: '',
                orden: 0,
                padreId: 0,
                param: '',
                sref: '',
                submenu: null,
                text: '',
                urlParam: ''
              },
              edit:false,
              menuName:'Menú',
            };
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/menu/menu_add.html',
                controller: 'menuAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {$scope.menu.getData();}, function (parameters) {$scope.menu.getData();});
            
        }, getRol: function(item){
            $scope.menuName=item.menuName;
            $scope.menuId=item.menuId;
            $scope.roles.getData();
            $scope.nameMenu=item.label;
        }, remove: function(item){
            ngDialog.openConfirm({
                template: tpl.path,
                className: 'ngdialog-theme-default',
                scope: $scope
            }).then(function (value) {
                var parametros={menuId: $scope.roles.menuId, rol: 'ADMINISTRADOR', rolId: 1, seleccionado: false};
                var _Http = new menuRolHttp(parametros);
                _Http.$unassignRol(function(response) {
                  $rootScope.loadingVisible = false;
                }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", "No se Desasignó el Rol: " + faild.Message);
                })
                var id = item.menuId;
                console.log(id);
                $rootScope.loadingVisible = true;
                menuRolHttp.removeMenu({}, { id: id }, function(response) {
                    $scope.menu.getData();
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", "No se Eliminó el Menú: " + faild.Message);
                });
            });
        }
    }
    $scope.roles= {
        current : {},
        data:[],
        menuId: 0,
        getData: function(){
            $rootScope.loadingVisible = true;
            menuRolHttp.getMenuRol({},{ id : $scope.menuId}, function (response) {
                $scope.roles.data = response;
                $scope.roles.menuId = $scope.menuId;
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se cargaron los perfiles : " + faild.Message);
            });
        },
        checkRol: function(item){
            var _Http = new menuRolHttp(item);
            $rootScope.loadingVisible = true;
            if(item.seleccionado==true){
              $rootScope.loadingVisible = true;
              _Http.$assignRol(function(response) {
                $rootScope.loadingVisible = false;
              }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se Asignó el Rol: " + faild.Message);
              })
            }
            else{
              _Http.$unassignRol(function(response) {
                $rootScope.loadingVisible = false;
              }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se Desasignó el Rol: " + faild.Message);
              })
            }
        }
    }
    
    $scope.menu.getData();
  }
})();
/**=========================================================
 * Module: navbar-search.js
 * Navbar search toggler * Auto dismiss on ESC key
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.navsearch')
        .directive('searchOpen', searchOpen)
        .directive('searchDismiss', searchDismiss);

    //
    // directives definition
    // 
    
    function searchOpen () {
        var directive = {
            controller: searchOpenController,
            restrict: 'A'
        };
        return directive;

    }

    function searchDismiss () {
        var directive = {
            controller: searchDismissController,
            restrict: 'A'
        };
        return directive;
        
    }

    //
    // Contrller definition
    // 
    
    searchOpenController.$inject = ['$scope', '$element', 'NavSearch'];
    function searchOpenController ($scope, $element, NavSearch) {
      $element
        .on('click', function (e) { e.stopPropagation(); })
        .on('click', NavSearch.toggle);
    }

    searchDismissController.$inject = ['$scope', '$element', 'NavSearch'];
    function searchDismissController ($scope, $element, NavSearch) {
      
      var inputSelector = '.navbar-form input[type="text"]';

      $(inputSelector)
        .on('click', function (e) { e.stopPropagation(); })
        .on('keyup', function(e) {
          if (e.keyCode === 27) // ESC
            NavSearch.dismiss();
        });
        
      // click anywhere closes the search
      $(document).on('click', NavSearch.dismiss);
      // dismissable options
      $element
        .on('click', function (e) { e.stopPropagation(); })
        .on('click', NavSearch.dismiss);
    }

})();


/**=========================================================
 * Module: nav-search.js
 * Services to share navbar search functions
 =========================================================*/
 
(function() {
    'use strict';

    angular
        .module('app.navsearch')
        .service('NavSearch', NavSearch);

    function NavSearch() {
        this.toggle = toggle;
        this.dismiss = dismiss;

        ////////////////

        var navbarFormSelector = 'form.navbar-form';

        function toggle() {
          var navbarForm = $(navbarFormSelector);

          navbarForm.toggleClass('open');
          
          var isOpen = navbarForm.hasClass('open');
          
          navbarForm.find('input')[isOpen ? 'focus' : 'blur']();
        }

        function dismiss() {
          $(navbarFormSelector)
            .removeClass('open') // Close control
            .find('input[type="text"]').blur() // remove focus
            .val('') // Empty input
            ;
        }        
    }
})();

/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .controller('crearClienteController', crearClienteController);

  crearClienteController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'oportunidadHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];


  function crearClienteController($scope, $filter, $state, $modalInstance, LDataSource, oportunidadHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {
      
      
      
      $scope.cliente = {     
          model:{
              identificacion: '',
              nombre: '',
              telefono: '', 
              direccion: '', 
              email: '',              
              nombreContacto: '',
              telefonoContacto: '',
              cargoContacto: ''
          },         
          
         save:  function(){ 
                
                if($scope.cliente.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
                
                   
             var _http = new oportunidadHttp($scope.cliente.model);
                  _http.$addTercero(function(response){                  
                      
                      
                      message.show("success", "Cliente creado satisfactoriamente!!");
                      $modalInstance.close(response);
                      
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
                 
              
              
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }      
       	      
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
      //$scope.cliente.model=parameters.cliente;
  }
})();
/**=========================================================
 * Module: app.oportunidad.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .controller('oportunidadController', oportunidadController);

  oportunidadController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'oportunidadHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function oportunidadController($scope, $filter, $state, LDataSource, oportunidadHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var oportunidad = $state.params.oportunidad;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.oportunidad = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,
              viaContactoId: 0,
              padreId: 0,
              tipo: '',
              consecutivo: '',
			  asunto: '',
			  estado: '',
			  descripcion: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: '',
              fechaEmision: '',
              fechaVencimiento: ''
            
		},   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.oportunidad.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.oportunidad.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.oportunidad', params : { filters : {procesoSeleccionado:oportunidad.tipo}, data : []} });
        $state.go('app.oportunidad');
          
      },
      save : function() {		  
       
		  
                
                if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.oportunidad.model.terceroId = $scope.tercerosOp.current.id;}
          
          		if($scope.oportunidad.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
				if($scope.oportunidad.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.oportunidad.model.estado= $scope.estados.current.value;}
				
				if(!$scope.tipoProceso.current){message.show("warning", "Tipo proceso requerido");return;}
				else{$scope.oportunidad.model.tipo= $scope.tipoProceso.current.value;}	
          
                if(!$scope.viaContacto.current){message.show("warning", "Vía de contacto requerido");return;}
				else{$scope.oportunidad.model.viaContactoId= $scope.viaContacto.current.codigo;}	
          
			//INSERTAR
            if($scope.oportunidad.model.id==0){
				
				$rootScope.loadingVisible = true;
				oportunidadHttp.save({}, $scope.oportunidad.model, function (data) { 
					
						oportunidadHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.oportunidad.model=data;
							oportunidad.id=$scope.oportunidad.model.id; 
							if($scope.oportunidad.model.fechaReg){$scope.oportunidad.fechaReg.value=new Date(parseFloat($scope.oportunidad.model.fechaReg));}    
							if($scope.oportunidad.model.fechaAct){$scope.oportunidad.fechaAct.value=new Date(parseFloat($scope.oportunidad.model.fechaAct));} 		
								
							message.show("success", "Información registrada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.oportunidad.model.id){
					$state.go('app.oportunidad');
				}else{
					
					oportunidadHttp.update({}, $scope.oportunidad.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.oportunidad.model=data;
						if($scope.oportunidad.model.fechaReg){$scope.oportunidad.fechaReg.value=new Date(parseFloat($scope.oportunidad.model.fechaReg));}    
						if($scope.oportunidad.model.fechaAct){$scope.oportunidad.fechaAct.value=new Date(parseFloat($scope.oportunidad.model.fechaAct));}  
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                     $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : data.contactoId })[0]; 
                    message.show("success", "Información actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.oportunidad.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.oportunidad.model.id,
                         referencia : 'OPORTUNIDAD' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/oportunidad/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.oportunidad.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.oportunidad.model.id,
                         referencia : 'OPORTUNIDAD' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },verCliente : function() {            
            var modalInstance = $modal.open({
              templateUrl: 'app/views/tercero/consultaTercero_form.html',
              controller: 'consultaTerceroController',
              size: 'lg',
              resolve: {
                parameters: { id : $scope.oportunidad.model.terceroId,
                              referencia : 'CLIENTE'}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.oportunidad.model.padreId,
                         referencia : 'OPORTUNIDAD' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      }
    }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrado'}); 
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelado'}); 
        }        
    }
	//TIPO PROCESO
      $scope.tipoProceso = {
            current: {},
            data: [],
            getData: function() {
                $scope.tipoProceso.data.push({value:'O', descripcion: 'Oportunidad'});
                $scope.tipoProceso.data.push({value:'POR', descripcion: 'Portafolio'});
                $scope.tipoProceso.data.push({value:'PRO', descripcion: 'Propuesta'});  
                    
            }
        }
      
    //VIA CONTACTO
    $scope.viaContacto = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        oportunidadHttp.getViaContacto({}, {}, function(response) {
            $scope.viaContacto.data = $filter('orderBy')(response, 'codigo');
            
            if(!$scope.oportunidad.model){$scope.viaContacto.current = $scope.viaContacto.data[0];}
            else{$scope.viaContacto.current = $filter('filter')($scope.viaContacto.data, { codigo : $scope.oportunidad.model.viaContactoId })[0];}
            
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setViaContacto : function(){
          $scope.oportunidad.model.viaContactoId=$scope.viaContacto.current.codigo;
    }}
    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            oportunidadHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.oportunidad.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.oportunidad.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.oportunidad.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.oportunidad.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }
    //CONTACTOS
    $scope.contactosOp = {
      current : {}, data:[],
      getData : function() {                  
          
               $rootScope.loadingVisible = true;
                oportunidadHttp.getContactosEmpresa({}, { terceroId: $scope.oportunidad.model.terceroId }, function(response) {   
                    
             $scope.contactosOp.data = response ;
                                                                                                                     
              if($scope.oportunidad.model ){ 
                    
                    $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.oportunidad.model.contactoId })[0];
               }
                else{
                    $scope.contactosOp.current=$scope.contactosOp.data[0];                     
                }
                                                                                                                   
                                                                                                                       
                    
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
            });
          
      },
      'setContacto' : function() {
        
          $scope.oportunidad.model.contactoId=$scope.contactosOp.current.id;
      }
    }
    
    
      //PRODUCTOS
    $scope.productosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            oportunidadHttp.getProductos({}, {}, function(response) {   
                
                $scope.productosOp.data = response ;                 
                $scope.productosOp.current=$scope.productosOp.data[0];
                $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }
    
    
    $scope.productoOportunidad = {
    model : {  
            productoId: 0,
            oportunidadId :0,
            total :0,
            valor :0,
            cantidad :0,
            valorIva :0,
            producto: '',
            comentario: ''
			
		},
      current : {},
      data:[],
      getData : function() {
          
          
          if(oportunidad){
              
            if($scope.oportunidad.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              oportunidadHttp.getProductosOportunidad({}, { oportunidadId: $scope.oportunidad.model.id }, function(response) {
                  
                  $scope.productoOportunidad.data = response;
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },        
        add: function(){           
            
              
            if($scope.oportunidad.model.id!=0){
                if($scope.productosOp.current){
                    $rootScope.loadingVisible = true;                    
                    
                    var data ={
                         productoId: $scope.productosOp.current.id,
                         oportunidadId :$scope.oportunidad.model.id,
                         total :  $scope.productosOp.current.valor,
                         valor :$scope.productosOp.current.valor,
                         cantidad :1,
                         valorIva :0,
                         producto: $scope.productosOp.current.nombre,
                         comentario: ''
                        
                    } ;
                      oportunidadHttp.addProductoOportunidad({}, data, function(response) {

                          $scope.productoOportunidad.getData();
                          $rootScope.loadingVisible = false;
                          message.show("success", "Producto agregado satisfactoriamente");	
                      }, function(faild) {
                          $rootScope.loadingVisible = false;
                          message.show("error", faild.Message);
                      });    
                }
                else{message.show("warning", "Debe seleccionar un producto");}

            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
          
        }
        ,        
        update: function(data){
            
            
            if($scope.oportunidad.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              oportunidadHttp.editProductoOportunidad({}, data, function(response) {
                  
                  $scope.productoOportunidad.getData();
                  $rootScope.loadingVisible = false;
                  message.show("success", "Producto actualizado satisfactoriamente");	
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
        }
        ,delete: function(data){            
            
            $rootScope.loadingVisible = true;          
            oportunidadHttp.removeProductoOportunidad({}, {oportunidadId: data.oportunidadId,productoId: data.productoId }, function(response){
             $scope.productoOportunidad.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
        }
    }
	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();
	$scope.tipoProceso.getData();	
    $scope.viaContacto.getData();
    $scope.tercerosOp.getData();   
    $scope.productosOp.getData();
      
   
  
	
	//CARGAMOS LOS DATOS DEL OPORTUNIDAD	
	
	if(oportunidad.id==0){
        
      $scope.estados.current=$scope.estados.data[0]; 
      $scope.tipoProceso.current= $filter('filter')($scope.tipoProceso.data, {value : oportunidad.tipo})[0]; 
      $scope.viaContacto.current=  $scope.viaContacto[0];   
      $scope.oportunidad.model.tipo=oportunidad.tipo;             
        
    }
    else{   
        $rootScope.loadingVisible = true;
		
            oportunidadHttp.read({},$state.params.oportunidad, function (data) { 
                
            $scope.oportunidad.model = data;	
            $scope.btnAdjunto=true;
            $scope.divProductosOportunidad=true;
               
			if($scope.oportunidad.model.fechaReg){$scope.oportunidad.fechaReg.value=new Date(parseFloat($scope.oportunidad.model.fechaReg));}    
			if($scope.oportunidad.model.fechaAct){$scope.oportunidad.fechaAct.value=new Date(parseFloat($scope.oportunidad.model.fechaAct));}  
            
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.oportunidad.model.terceroId })[0];  
            $scope.contactosOp.getData();
            $scope.productoOportunidad.getData();                
            $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.oportunidad.model.contactoId })[0];    
                
			//$scope.estados.current=$scope.oportunidad.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.oportunidad.model.estado})[0];
            $scope.tipoProceso.current = $filter('filter')($scope.tipoProceso.data, {value : $scope.oportunidad.model.tipo})[0];
            $scope.viaContacto.current = $filter('filter')($scope.viaContacto.data, {codigo : $scope.oportunidad.model.viaContactoId})[0];
                
             if  ($scope.oportunidad.model.padreId!=0){
                 
                 $scope.btnHistorico=true;
             }     
                
                    
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      

      
        
	
    
    
  }
})();
/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .service('oportunidadHttp', oportunidadHttp);

  oportunidadHttp.$inject = ['$resource', 'END_POINT'];


  function oportunidadHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',           
        'isArray' : true,
          'params' : {
            proceso : '@proceso',
            estado : '@estado'              
          },
          'url' : END_POINT + '/Comercial.svc/oportunidad/proceso/:proceso/estado/:estado'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:id/proceso'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/oportunidad/proceso'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/oportunidad/proceso'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:id/proceso'
      },
        'getViaContacto' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/VIACONTACTO'
      },        
      'getContactosEmpresa' : {
       'params' : {
            terceroId : '@terceroId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroOportunidad'
      },
       'getProductos' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/productoBusqueda'
      },
       'getProductosOportunidad' : {
       'params' : {
            oportunidadId : '@oportunidadId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:oportunidadId/producto'
      },
        'addProductoOportunidad': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/oportunidad/producto'
      },
        'editProductoOportunidad': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/oportunidad/producto'
      },
      'removeProductoOportunidad':  {
        'params' : {
            oportunidadId : '@oportunidadId',
            productoId : '@productoId'   
          },
        'method':'DELETE',        
        'url' : END_POINT + '/Comercial.svc/oportunidad/:oportunidadId/producto/:productoId'
      }
     
    };
    return $resource( END_POINT + '/Comercial.svc/oportunidad/proceso', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.oportunidad.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .controller('oportunidadListController', oportunidadListController);

  oportunidadListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'oportunidadHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function oportunidadListController($scope, $filter, $state, ngDialog, tpl, oportunidadHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {        

    var procesoSeleccionado= $state.params.filters.procesoSeleccionado;	
    var estadoSeleccionado= $state.params.filters.estadoSeleccionado;	
		
	$scope.ESTADO_PROCESO = 'P';
    $scope.ESTADO_ANULADO = 'A';
    $scope.ESTADO_CERRADO= 'C';	
    $scope.ESTADO_CANCELADO= 'CA';		
        
    $scope.oportunidades = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreoportunidades:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.oportunidades.selectedAll = !$scope.oportunidades.selectedAll; 
        for (var key in $scope.oportunidades.selectedItems) {
          $scope.oportunidades.selectedItems[key].check = $scope.oportunidades.selectedAll;
        }
      },
      add : function() {
        
        if($scope.tipoProceso.current.value){        
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      tipo: $scope.tipoProceso.current.value,
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.oportunidad_add', params : { oportunidad: oport } });
            $state.go('app.oportunidad_add');
          
          }
          else{
              
              message.show("warning", "Debe seleccionar el tipo de proceso!!")
          }
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.oportunidad_edit', params : { oportunidad: item } });
        $state.go('app.oportunidad_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            oportunidadHttp.remove({}, { id: id }, function(response) {
                $scope.oportunidades.getData();
                message.show("success", "Oportunidad eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.oportunidades.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    oportunidadHttp.remove({}, { id: id }, function(response) {
                        $scope.oportunidades.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.oportunidades.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.oportunidades.filterText,
          "precision": false
        }];
        $scope.oportunidades.selectedItems = $filter('arrayFilter')($scope.oportunidades.dataSource, paramFilter);
        $scope.oportunidades.paginations.totalItems = $scope.oportunidades.selectedItems.length;
        $scope.oportunidades.paginations.currentPage = 1;
        $scope.oportunidades.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.oportunidades.paginations.currentPage == 1 ) ? 0 : ($scope.oportunidades.paginations.currentPage * $scope.oportunidades.paginations.itemsPerPage) - $scope.oportunidades.paginations.itemsPerPage;
        $scope.oportunidades.data = $scope.oportunidades.selectedItems.slice(firstItem , $scope.oportunidades.paginations.currentPage * $scope.oportunidades.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.oportunidades.data = [];
        $scope.oportunidades.loading = true;
        $scope.oportunidades.noData = false;
        var parametros= {
            "proceso":$scope.tipoProceso.current.value,
            "estado":$scope.estado.current.value
        };     
        oportunidadHttp.getList({}, parametros,function(response) {
          $scope.oportunidades.selectedItems = response;
          $scope.oportunidades.dataSource = response;
          for(var i=0; i<$scope.oportunidades.dataSource.length; i++){
            $scope.oportunidades.nombreoportunidades.push({id: i, nombre: $scope.oportunidades.dataSource[i]});
          }
          $scope.oportunidades.paginations.totalItems = $scope.oportunidades.selectedItems.length;
          $scope.oportunidades.paginations.currentPage = 1;
          $scope.oportunidades.changePage();
          $scope.oportunidades.loading = false;
          ($scope.oportunidades.dataSource.length < 1) ? $scope.oportunidades.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
      //CARGAMOS PROCESOS
    $scope.tipoProceso = {
        current : {}, data:[],
        getData : function() {
            $scope.tipoProceso.data.push({value:'O', descripcion: 'Oportunidad'});
            $scope.tipoProceso.data.push({value:'POR', descripcion: 'Portafolio'});
            $scope.tipoProceso.data.push({value:'PRO', descripcion: 'Propuesta'});  
            
            
            $scope.tipoProceso.current =$scope.tipoProceso.data[0];
        }        
    }	   
          //ESTADOS
    $scope.estado = {
        current : {}, data:[],
        getData : function() {            
            $scope.estado.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estado.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estado.data.push({value:'C', descripcion: 'Cerrado'}); 
            $scope.estado.data.push({value:'CA', descripcion: 'Cancelado'}); 
            $scope.estado.current =$scope.estado.data[0];
        }        
    }
        
	//CARGAMOS LOS TIPOS DE PROCESO
    $scope.estado.getData();
    $scope.tipoProceso.getData();
    $scope.oportunidades.getData();  
    
        
        if(procesoSeleccionado){

            $scope.tipoProceso.current = $filter('filter')($scope.tipoProceso.data, {value : procesoSeleccionado})[0];        
            $scope.estado.current = $filter('filter')($scope.estado.data, {value : estadoSeleccionado})[0];        
            $scope.oportunidades.getData();        
        }
        
	}
  
  
  })();
/**=========================================================
 * Module: access-login.js
 * Demo for login api
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.pages')
        .controller('LoginFormController', LoginFormController);

    LoginFormController.$inject = ['$http', '$state'];
    function LoginFormController($http, $state) {
        var vm = this;

        activate();

        ////////////////

        function activate() {
          // bind here all data from the form
          vm.account = {};
          // place the message if something goes wrong
          vm.authMsg = '';

          vm.login = function() {
            vm.authMsg = '';

            if(vm.loginForm.$valid) {

              $http
                .post('api/account/login', {email: vm.account.email, password: vm.account.password})
                .then(function(response) {
                  // assumes if ok, response is an object with some data, if not, a string with error
                  // customize according to your api
                  if ( !response.account ) {
                    vm.authMsg = 'Incorrect credentials.';
                  }else{
                    $state.go('app.dashboard');
                  }
                }, function() {
                  vm.authMsg = 'Server Request Error';
                });
            }
            else {
              // set as dirty if the user click directly to login so we show the validation messages
              /*jshint -W106*/
              vm.loginForm.account_email.$dirty = true;
              vm.loginForm.account_password.$dirty = true;
            }
          };
        }
    }
})();

/**=========================================================
 * Module: access-register.js
 * Demo for register account api
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.pages')
        .controller('RegisterFormController', RegisterFormController);

    RegisterFormController.$inject = ['$http', '$state'];
    function RegisterFormController($http, $state) {
        var vm = this;

        activate();

        ////////////////

        function activate() {
          // bind here all data from the form
          vm.account = {};
          // place the message if something goes wrong
          vm.authMsg = '';
            
          vm.register = function() {
            vm.authMsg = '';

            if(vm.registerForm.$valid) {

              $http
                .post('api/account/register', {email: vm.account.email, password: vm.account.password})
                .then(function(response) {
                  // assumes if ok, response is an object with some data, if not, a string with error
                  // customize according to your api
                  if ( !response.account ) {
                    vm.authMsg = response;
                  }else{
                    $state.go('app.dashboard');
                  }
                }, function() {
                  vm.authMsg = 'Server Request Error';
                });
            }
            else {
              // set as dirty if the user click directly to login so we show the validation messages
              /*jshint -W106*/
              vm.registerForm.account_email.$dirty = true;
              vm.registerForm.account_password.$dirty = true;
              vm.registerForm.account_agreed.$dirty = true;
              
            }
          };
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('app.preloader')
        .directive('preloader', preloader);

    preloader.$inject = ['$animate', '$timeout', '$q'];
    function preloader ($animate, $timeout, $q) {

        var directive = {
            restrict: 'EAC',
            template: 
              '<div class="preloader-progress">' +
                  '<div class="preloader-progress-bar" ' +
                       'ng-style="{width: loadCounter + \'%\'}"></div>' +
              '</div>'
            ,
            link: link
        };
        return directive;

        ///////

        function link(scope, el) {

          scope.loadCounter = 0;

          var counter  = 0,
              timeout;

          // disables scrollbar
          angular.element('body').css('overflow', 'hidden');
          // ensure class is present for styling
          el.addClass('preloader');

          appReady().then(endCounter);

          timeout = $timeout(startCounter);

          ///////

          function startCounter() {

            var remaining = 100 - counter;
            counter = counter + (0.015 * Math.pow(1 - Math.sqrt(remaining), 2));

            scope.loadCounter = parseInt(counter, 10);

            timeout = $timeout(startCounter, 20);
          }

          function endCounter() {

            $timeout.cancel(timeout);

            scope.loadCounter = 100;

            $timeout(function(){
              // animate preloader hiding
              $animate.addClass(el, 'preloader-hidden');
              // retore scrollbar
              angular.element('body').css('overflow', '');
            }, 300);
          }

          function appReady() {
            var deferred = $q.defer();
            var viewsLoaded = 0;
            // if this doesn't sync with the real app ready
            // a custom event must be used instead
            var off = scope.$on('$viewContentLoaded', function () {
              viewsLoaded ++;
              // we know there are at least two views to be loaded 
              // before the app is ready (1-index.html 2-app*.html)
              if ( viewsLoaded === 2) {
                // with resolve this fires only once
                $timeout(function(){
                  deferred.resolve();
                }, 3000);

                off();
              }

            });

            return deferred.promise;
          }

        } //link
    }

})();
/**=========================================================
 * Module: app.producto.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.producto')
    .controller('productoController', productoController);

  productoController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'productoHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function productoController($scope, $filter, $state, LDataSource, productoHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var producto = $state.params.producto;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.producto = {
		model : {
              id: 0,                            
			  nombre: '',
              descripcion: '',
              valor:0,			
              estado: '',
              fechaAct: '',
              usuarioAct: '',
              fechaReg: '',
              usuarioReg: ''			
		}, 
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.producto.fechaInicial.isOpen = true;
        }
      },  
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.producto.fechaFinal.isOpen = true;
        }
      },
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.producto.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.producto.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.producto', params : { filters : {procesoSeleccionado:producto.tipo}, data : []} });
        $state.go('app.producto');
          
      },
      save : function() {		  
       
		  
        
          		if($scope.producto.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
				if($scope.producto.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}				
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.producto.model.estado= $scope.estados.current.value;}
				
			 
			//INSERTAR
            if($scope.producto.model.id==0){
				
				$rootScope.loadingVisible = true;
				productoHttp.save({}, $scope.producto.model, function (data) { 
					
						productoHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.producto.model=data;
							producto.id=$scope.producto.model.id; 
							if($scope.producto.model.fechaReg){$scope.producto.fechaReg.value=new Date(parseFloat($scope.producto.model.fechaReg));}    
							if($scope.producto.model.fechaAct){$scope.producto.fechaAct.value=new Date(parseFloat($scope.producto.model.fechaAct));} 
                            
                                 
								
							message.show("success", "producto creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.producto.model.id){
					$state.go('app.producto');
				}else{
					
					productoHttp.update({}, $scope.producto.model, function (data) {                           
					  $rootScope.loadingVisible = false;
					       $scope.producto.model=data;
						if($scope.producto.model.fechaReg){$scope.producto.fechaReg.value=new Date(parseFloat($scope.producto.model.fechaReg));}    
						if($scope.producto.model.fechaAct){$scope.producto.fechaAct.value=new Date(parseFloat($scope.producto.model.fechaAct));}   		
                                     
					   message.show("success", "producto actualizado satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		}
      }
        
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'A', descripcion: 'Activo'});
            $scope.estados.data.push({value:'I', descripcion: 'Inactivo'});
            $scope.estados.data.push({value:'E', descripcion: 'Eliminado'}); 
        }       
    }      
    //Estados
    $scope.estados.getData();	
	//CARGAMOS LOS DATOS DEL producto	
	
	if(producto.id==0){
      $scope.estados.current=$scope.estados.data[0]; 
       
        
    }
    else{   
        $rootScope.loadingVisible = true;       
        
		
        productoHttp.read({},$state.params.producto, function (data) { 
        $scope.producto.model = data;		

        if($scope.producto.model.fechaReg){$scope.producto.fechaReg.value=new Date(parseFloat($scope.producto.model.fechaReg));}    
        if($scope.producto.model.fechaAct){$scope.producto.fechaAct.value=new Date(parseFloat($scope.producto.model.fechaAct));}              
       
        //$scope.estados.current=$scope.producto.model.estado;	            
        $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.producto.model.estado})[0];
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
    
      

	
    
    
  }
})();
/**=========================================================
 * Module: app.contrato.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.producto')
    .service('productoHttp', contratoHttp);

  contratoHttp.$inject = ['$resource', 'END_POINT'];


  function contratoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {  
      'getList' : {
        'method' : 'GET',
        'isArray' : true
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/producto/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/producto'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/producto'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/producto/:id'
      }
        
     
    };
    return $resource( END_POINT + '/Comercial.svc/producto', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.producto.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.producto')
    .controller('productoListController', productoListController);

  productoListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'productoHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function productoListController($scope, $filter, $state, ngDialog, tpl, productoHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {   
	
		
	$scope.ESTADO_ACTIVO = 'A';
    $scope.ESTADO_INACTIVO = 'I';
    $scope.ESTADO_ELIMINADO= 'E';	
    	

    $scope.productos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreproductos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.productos.selectedAll = !$scope.productos.selectedAll; 
        for (var key in $scope.productos.selectedItems) {
          $scope.productos.selectedItems[key].check = $scope.productos.selectedAll;
        }
      },
      add : function() {  
              var cont = {
                      id: 0,                    
                      nombre: '',
                      descripcion: '',
                      valor: 0,
                      estado: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.producto_add', params : { producto: cont } });
            $state.go('app.producto_add');  
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.producto_edit', params : { producto: item } });
        $state.go('app.producto_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            productoHttp.remove({}, { id: id }, function(response) {
                $scope.productos.getData();
                message.show("success", "producto eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.productos.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    productoHttp.remove({}, { id: id }, function(response) {
                        $scope.productos.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.productos.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.productos.filterText,
          "precision": false
        }];
        $scope.productos.selectedItems = $filter('arrayFilter')($scope.productos.dataSource, paramFilter);
        $scope.productos.paginations.totalItems = $scope.productos.selectedItems.length;
        $scope.productos.paginations.currentPage = 1;
        $scope.productos.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.productos.paginations.currentPage == 1 ) ? 0 : ($scope.productos.paginations.currentPage * $scope.productos.paginations.itemsPerPage) - $scope.productos.paginations.itemsPerPage;
        $scope.productos.data = $scope.productos.selectedItems.slice(firstItem , $scope.productos.paginations.currentPage * $scope.productos.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.productos.data = [];
        $scope.productos.loading = true;
        $scope.productos.noData = false;      
         productoHttp.getList(function(response) {
          $scope.productos.selectedItems = response;
          $scope.productos.dataSource = response;
          for(var i=0; i<$scope.productos.dataSource.length; i++){
            $scope.productos.nombreproductos.push({id: i, nombre: $scope.productos.dataSource[i]});
          }
          $scope.productos.paginations.totalItems = $scope.productos.selectedItems.length;
          $scope.productos.paginations.currentPage = 1;
          $scope.productos.changePage();
          $scope.productos.loading = false;
          ($scope.productos.dataSource.length < 1) ? $scope.productos.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }  
    
	//CARGAMOS DATA    
    $scope.productos.getData();  
        
 
	}
  
  
  })();
/**=========================================================
 * Module: app.reporte.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.reporte')
    .service('reporteHttp', reporteHttp);

  reporteHttp.$inject = ['$resource', 'END_POINT'];


  function reporteHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            modulo : '@modulo'           
          },
          'url' : END_POINT + '/General.svc/reporte/:modulo'
          
      }  
    };
    return $resource( END_POINT + '/General.svc/reporte', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.reporte.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.reporte')
    .controller('reporteListController', reporteListController);

  reporteListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'reporteHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function reporteListController($scope, $filter, $state, ngDialog, tpl, reporteHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
        

    var moduloSeleccionado= $state.params.filters.moduloSeleccionado;
	
		
	$scope.ESTADO_PROCESO = 'P';
    $scope.ESTADO_ANULADO = 'A';
    $scope.ESTADO_CERRADO= 'C';	
    $scope.ESTADO_CANCELADO= 'CA';		

    $scope.reportes = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrereportes:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.reportes.selectedAll = !$scope.reportes.selectedAll; 
        for (var key in $scope.reportes.selectedItems) {
          $scope.reportes.selectedItems[key].check = $scope.reportes.selectedAll;
        }
      },
      add : function() {
        
        if($scope.modulo.current.value){        
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      tipo: $scope.modulo.current.value,
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.reporte_add', params : { reporte: oport } });
            $state.go('app.reporte_add');
          
          }
          else{
              
              message.show("warning", "Debe seleccionar el tipo de proceso!!")
          }
          
      },
      edit : function(item) {
          
          //ngDialog.open(item.url);  
          //alert("hola");
          
          window.open(item.url);
        //parametersOfState.set({ name : 'app.reporte_edit', params : { reporte: item } });
        //$state.go('app.reporte_edit');
          
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            reporteHttp.remove({}, { id: id }, function(response) {
                $scope.reportes.getData();
                message.show("success", "reporte eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.reportes.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    reporteHttp.remove({}, { id: id }, function(response) {
                        $scope.reportes.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.reportes.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.reportes.filterText,
          "precision": false
        }];
        $scope.reportes.selectedItems = $filter('arrayFilter')($scope.reportes.dataSource, paramFilter);
        $scope.reportes.paginations.totalItems = $scope.reportes.selectedItems.length;
        $scope.reportes.paginations.currentPage = 1;
        $scope.reportes.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.reportes.paginations.currentPage == 1 ) ? 0 : ($scope.reportes.paginations.currentPage * $scope.reportes.paginations.itemsPerPage) - $scope.reportes.paginations.itemsPerPage;
        $scope.reportes.data = $scope.reportes.selectedItems.slice(firstItem , $scope.reportes.paginations.currentPage * $scope.reportes.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.reportes.data = [];
        $scope.reportes.loading = true;
        $scope.reportes.noData = false;
        var parametros= {
            "modulo":$scope.modulo.current.value                
        };
          
        reporteHttp.getList({}, parametros,function(response) {
          $scope.reportes.selectedItems = response;
          $scope.reportes.dataSource = response;
          for(var i=0; i<$scope.reportes.dataSource.length; i++){
            $scope.reportes.nombrereportes.push({id: i, nombre: $scope.reportes.dataSource[i]});
          }
          $scope.reportes.paginations.totalItems = $scope.reportes.selectedItems.length;
          $scope.reportes.paginations.currentPage = 1;
          $scope.reportes.changePage();
          $scope.reportes.loading = false;
          ($scope.reportes.dataSource.length < 1) ? $scope.reportes.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
      //CARGAMOS PROCESOS
    $scope.modulo = {
        current : {}, data:[],
        getData : function() {
            $scope.modulo.data.push({value:'CL', descripcion: 'Cliente'});                                
            $scope.modulo.current =$scope.modulo.data[0];
        }        
    }	   
    
	//CARGAMOS LOS TIPOS DE PROCESO
    $scope.modulo.getData();
    $scope.reportes.getData();  
        
        if(moduloSeleccionado){

            $scope.modulo.current = $filter('filter')($scope.modulo.data, {value : moduloSeleccionado})[0];        
            $scope.reportes.getData();        
        }
        
	}
  
  
  })();
/**=========================================================
 * Module: app.report.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.rol')
    .controller('rolController', rolController);

  rolController.$inject = ['$scope', '$rootScope', '$state', '$q', '$filter', 'roltHttp', 'base64', '$stateParams', 'parametersOfState', 'message', '$modal', 'ngDialog', 'tpl'];


  function rolController($scope, $rootScope, $state, $q, $filter, roltHttp, base64, $stateParams, parametersOfState, message, $modal, ngDialog, tpl) {
      
      
      $scope.gridOptions = {
        rowHeight: 34,
        data: [],
        subsections: [],
        setData: function(){
          $rootScope.loadingVisible = true;
          $scope.gridOptions.data = []; 
          roltHttp.getList({}, {}, function(response) {
              $scope.gridOptions.data = $filter('orderBy')(response, 'nombre');
              $rootScope.loadingVisible = false;
          }, function(faild) {
              $rootScope.loadingVisible = false;
              message.show("error", "No se Pudo Obtener el Listado de Roles: " + faild.Message);
          });
        },
        edit: function (item){
          var parameter = {datos: item, option: 0};
          var modalInstance = $modal.open({
              templateUrl: 'app/views/roles/roles_edit.html',
              controller: 'rolEditController',
              size: 'sm',
              resolve: {
                parameters: function () { return parameter; }
              }
          });
          modalInstance.result.then(function (parameters) {
            $scope.gridOptions.setData();
          }, function (parameters) {
          });
        },
        add: function (){
          var model = {id: 0, nombre: '', estado: 0};
          var parameter = {datos: model, option: 1};
          var modalInstance = $modal.open({
              templateUrl: 'app/views/roles/roles_edit.html',
              controller: 'rolEditController',
              size: 'sm',
              resolve: {
                parameters: function () { return parameter; }
              }
          });
          modalInstance.result.then(function (parameters) {
            $scope.gridOptions.setData();
          }, function (parameters) {
          });
        },
        remove: function(item){
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
              $rootScope.loadingVisible = true;
              roltHttp.removeRol({}, { id: item.id }, function(response) {
                  $scope.gridOptions.setData();
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", "No se Pudo Eliminar el Registro: " + faild.Message);
              });
          });
        }
      };
      $scope.gridOptions.setData();
  }

})();
/**=========================================================
 * Module: app.report.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.rol')
    .controller('rolEditController', rolEditController);

  rolEditController.$inject = ['$scope', '$rootScope', '$state', '$q', '$filter', 'roltHttp', 'base64', '$stateParams', 'parametersOfState', 'message', 'parameters', '$modalInstance'];


  function rolEditController($scope, $rootScope, $state, $q, $filter, roltHttp, base64, $stateParams, parametersOfState, message, parameters, $modalInstance) {
    
      $scope.estados= {
          current:{},
          data: [],
          setData: function() {
              $scope.estados.data.push({id: 0, value: 'Inactivo'});
              $scope.estados.data.push({id: 1, value: 'Activo'});
              $scope.estados.current = $scope.estados.data[0];
          }
      };
      $scope.estados.setData();
      
      $scope.rol={
          model:{
              id: 0,
              nombre: '',
              estado: 0
          },
          option: 0,
          secciones: [],
          save: function() {
            
            if(!$scope.rol.model.nombre){message.show("warning", "Nombre del Rol Requerido");return;}
            if($scope.rol.model.nombre == ""){message.show("warning", "Nombre del Rol Requerido");return;}
            if(!$scope.estados.current){message.show("warning", "Estado del Rol Requerido");return;}
            $scope.rol.model.estado = $scope.estados.current.id;
            
            if($scope.rol.option == 0){
              $rootScope.loadingVisible = true;
              var parametros = $scope.rol.model;
              roltHttp.editRol({}, parametros,  function(response) {
                  $scope.rol.close();
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", "No se pudo guardar el registro: " + faild.Message);
              });
            }
            if($scope.rol.option == 1){
              
              var parametros = {
                  nombre: $scope.rol.model.nombre,
                  estado: $scope.rol.model.estado
              };
              
              var _Http = new roltHttp(parametros);
              _Http.$newrol( function(response) {
                  $scope.rol.close();
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });
            }
          },
          dismiss : function() {
              $modalInstance.dismiss('cancel');
          },
          close : function() {
              $modalInstance.close('close');
          }
      };
      
      $scope.rol.model = parameters.datos;
      $scope.rol.option = parameters.option;
      
      if($scope.rol.option==0){
        $scope.estados.current= $filter('filter')($scope.estados.data, {id: $scope.rol.model.estado})[0];
      }
  }

})();
/**=========================================================
 * Module: app.report.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.rol')
    .service('roltHttp', roltHttp);

  roltHttp.$inject = ['$resource', 'END_POINT'];


  function roltHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/rol'
      },
      'editRol' : {
        'method' : 'PUT',
        'url' : END_POINT + '/General.svc/rol'
      },
      'newrol' : {
        'method' : 'POST',
        'url' : END_POINT + '/General.svc/rol'
      },
      'removeRol': {
        'method':'DELETE',
        'params' : {
           id : '@id'
        },
        'url' : END_POINT + '/General.svc/rol/:id'
      }
    };

    return $resource(END_POINT + '/General.svc/rol', paramDefault, actions);
  }

})();
/**=========================================================
 * Module: helpers.js
 * Provides helper functions for routes definition
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.routes')
        .provider('RouteHelpers', RouteHelpersProvider)
        ;

    RouteHelpersProvider.$inject = ['APP_REQUIRES'];
    function RouteHelpersProvider(APP_REQUIRES) {

      /* jshint validthis:true */
      return {
        // provider access level
        basepath: basepath,
        resolveFor: resolveFor,
        // controller access level
        $get: function() {
          return {
            basepath: basepath,
            resolveFor: resolveFor
          };
        }
      };

      // Set here the base of the relative path
      // for all app views
      function basepath(uri) {
        return 'app/views/' + uri;
      }

      // Generates a resolve object by passing script names
      // previously configured in constant.APP_REQUIRES
      function resolveFor() {
        var _args = arguments;
        return {
          deps: ['$ocLazyLoad','$q', function ($ocLL, $q) {
            // Creates a promise chain for each argument
            var promise = $q.when(1); // empty promise
            for(var i=0, len=_args.length; i < len; i ++){
              promise = andThen(_args[i]);
            }
            return promise;

            // creates promise to chain dynamically
            function andThen(_arg) {
              // also support a function that returns a promise
              if(typeof _arg === 'function')
                  return promise.then(_arg);
              else
                  return promise.then(function() {
                    // if is a module, pass the name. If not, pass the array
                    var whatToLoad = getRequired(_arg);
                    // simple error check
                    if(!whatToLoad) return $.error('Route resolve: Bad resource name [' + _arg + ']');
                    // finally, return a promise
                    return $ocLL.load( whatToLoad );
                  });
            }
            // check and returns required data
            // analyze module items with the form [name: '', files: []]
            // and also simple array of script files (for not angular js)
            function getRequired(name) {
              if (APP_REQUIRES.modules)
                  for(var m in APP_REQUIRES.modules)
                      if(APP_REQUIRES.modules[m].name && APP_REQUIRES.modules[m].name === name)
                          return APP_REQUIRES.modules[m];
              return APP_REQUIRES.scripts && APP_REQUIRES.scripts[name];
            }

          }]};
      } // resolveFor

    }


})();


/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.routes')
    .config(routesConfig);

  routesConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider'];
  function routesConfig($stateProvider, $locationProvider, $urlRouterProvider, helper){

    // Set the following to true to enable the HTML5 Mode
    // You may have to set <base> tag in index and a routing configuration in your server
    $locationProvider.html5Mode(false);

    // defaults to dashboard
    $urlRouterProvider.otherwise('/page/login');
    //'toastr',

    $stateProvider
      .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: helper.basepath('app.html'),
      resolve: helper.resolveFor('fastclick', 'modernizr', 'icons', 'screenfull', 'animo', 'sparklines', 'slimscroll', 'classyloader', 'whirl', 'al-ui', 'ngProgress', 'ab-base64','angularFileUpload', 'ngFileUpload', 'flot-chart','flot-chart-plugins', 'ui.select', 'ngDialog', 'ui.utils.masks')
    })
      .state('app.home', {
      url: '/home',
      title: 'home',
      templateUrl: helper.basepath('home.html'),
      resolve: helper.resolveFor( 'weather-icons')
    })
      .state('app.template', {
      url: '/template',
      title: 'Blank Template',
      templateUrl: helper.basepath('template.html')
    })
    
    //FUNCIONARIO
	   .state('app.funcionario', {
      url: '/funcionario',
      controller : 'funcionarioListController',
      templateUrl: 'app/views/funcionario/funcionario_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
	
	 .state('app.funcionario_add', {
      url: '/funcionario_add',
      controller : 'funcionarioController',
      templateUrl: 'app/views/funcionario/funcionario_form.html',
      params : { funcionario: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
      .state('app.funcionario_edit', {
      url: '/funcionario_edit',
      controller : 'funcionarioController',
      templateUrl: 'app/views/funcionario/funcionario_form.html',
      params : { funcionario: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
    //TERCERO
       .state('app.tercero', {
      url: '/tercero',
      controller : 'terceroListController',
      templateUrl: 'app/views/tercero/tercero_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
     .state('app.tercero_add', {
      url: '/tercero_add',
      controller : 'terceroController',
      templateUrl: 'app/views/tercero/tercero_form.html',
      params : { tercero: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
      .state('app.tercero_edit', {
      url: '/tercero_edit',
      controller : 'terceroController',
      templateUrl: 'app/views/tercero/tercero_form.html',
      params : { tercero: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
    
    //REPORTES
    
     .state('app.reporte', {
      url: '/reporte',
      controller : 'reporteListController',
      templateUrl: 'app/views/reporte/reporte_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
    
    //OPORTUNIDAD
    
     .state('app.oportunidad', {
      url: '/oportunidad',
      controller : 'oportunidadListController',
      templateUrl: 'app/views/oportunidad/oportunidad_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.oportunidad_add', {
      url: '/oportunidad_add',
      controller : 'oportunidadController',
      templateUrl: 'app/views/oportunidad/oportunidad_form.html',
      params : { oportunidad: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.oportunidad_edit', {
      url: '/oportunidad_edit',
      controller : 'oportunidadController',
      templateUrl: 'app/views/oportunidad/oportunidad_form.html',
      params : { oportunidad: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })   
    
    
    //CONTRATO
    
     .state('app.contrato', {
      url: '/contrato',
      controller : 'contratoListController',
      templateUrl: 'app/views/contrato/contrato_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.contrato_add', {
      url: '/contrato_add',
      controller : 'contratoController',
      templateUrl: 'app/views/contrato/contrato_form.html',
      params : { contrato: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html'),ok: helper.basepath('templates/ok_confirmation.html') }; }
      })
    })    
      .state('app.contrato_edit', {
      url: '/contrato_edit',
      controller : 'contratoController',
      templateUrl: 'app/views/contrato/contrato_form.html',
      params : { contrato: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html'),ok: helper.basepath('templates/ok_confirmation.html') }; }
      })
    })    
    
    //PRODUCTO
    
     .state('app.producto', {
      url: '/producto',
      controller : 'productoListController',
      templateUrl: 'app/views/producto/producto_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.producto_add', {
      url: '/contrato_add',
      controller : 'productoController',
      templateUrl: 'app/views/producto/producto_form.html',
      params : { producto: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.producto_edit', {
      url: '/producto_edit',
      controller : 'productoController',
      templateUrl: 'app/views/producto/producto_form.html',
      params : { producto: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
    // CALENDARIO
    .state('app.calendar', {
          url: '/calendar',
          title: 'Calendar',
          templateUrl: helper.basepath('/calendar/calendar.html'),
          resolve: helper.resolveFor('jquery-ui', 'jquery-ui-widgets', 'moment', 'fullcalendar')
      })
    
    
    //AGENDAS    
     .state('app.agenda', {
      url: '/agenda',
      controller : 'agendaListController',
      templateUrl: 'app/views/agenda/agenda_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.agenda_add', {
      url: '/agenda_add',
      controller : 'agendaFormController',
      templateUrl: 'app/views/agenda/agenda_form.html',
      params : { agenda: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.agenda_edit', {
      url: '/agenda_edit',
      controller : 'agendaFormController',
      templateUrl: 'app/views/agenda/agenda_form.html',
      params : { agenda: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    

    //CONSECUTIVO
    
     .state('app.consecutivo', {
      url: '/consecutivo',
      controller : 'consecutivoListController',
      templateUrl: 'app/views/consecutivo/consecutivo_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.consecutivo_add', {
      url: '/consecutivo_add',
      controller : 'consecutivoController',
      templateUrl: 'app/views/consecutivo/consecutivo_form.html',
      params : { consecutivo: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.consecutivo_edit', {
      url: '/consecutivo_edit',
      controller : 'consecutivoController',
      templateUrl: 'app/views/consecutivo/consecutivo_form.html',
      params : { consecutivo: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
     .state('app.setMenu', {
      url: '/Menu',
      controller : 'setMenuController',
      templateUrl: 'app/views/menu/setMenu.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
     .state('app.compromiso', {
      url: '/Compromiso',
      controller : 'compromisoController',
      templateUrl: 'app/views/compromiso/setCompromiso.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
      .state('app.rol', {
      url: '/Roles',
      controller : 'rolController',
      templateUrl: 'app/views/roles/roles.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
        
    //FACTURA
    
     .state('app.factura', {
      url: '/factura',
      controller : 'facturaListController',
      templateUrl: 'app/views/factura/factura_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.factura_add', {
      url: '/factura_add',
      controller : 'facturaController',
      templateUrl: 'app/views/factura/factura_form.html',
      params : { factura: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.factura_edit', {
      url: '/factura_edit',
      controller : 'facturaController',
      templateUrl: 'app/views/factura/factura_form.html',
      params : { factura: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    }) 
    
    //MENSAJERIA
    
     .state('app.mensajeria', {
      url: '/mensajeria',
      controller : 'mensajeriaListController',
      templateUrl: 'app/views/mensajeria/mensajeria_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.mensajeria_add', {
      url: '/mensajeria_add',
      controller : 'mensajeriaController',
      templateUrl: 'app/views/mensajeria/mensajeria_form.html',
      params : { mensajeria: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.mensajeria_edit', {
      url: '/mensajeria_edit',
      controller : 'mensajeriaController',
      templateUrl: 'app/views/mensajeria/mensajeria_form.html',
      params : { mensajeria: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
     
      .state('app.registroUbicacion', {
      url: '/registroUbicacion',
      controller : 'registroUbicacionListController',
      templateUrl: 'app/views/usuario/registroUbicacion_list.html',
      params : { filters : {}, data : []},
        resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    }) 
    
    // 
    // Single Page Routes
    // -----------------------------------
      .state('page', {
      url: '/page',
      templateUrl: 'app/pages/page.html',
      resolve: helper.resolveFor('modernizr', 'icons'),
      controller: ['$rootScope', function($rootScope) {
        $rootScope.app.layout.isBoxed = false;
      }]
    })
      .state('page.login', {
      url: '/login',
      controller : 'LoginController',
      title: 'Login',
      templateUrl: 'app/views/security/login.html'
    })
      .state('page.register', {
      url: '/register',
      title: 'Register',
      templateUrl: 'app/views/security/register.html'
    })
      .state('page.recover', {
      url: '/recover',
      controller : 'RecoverController',
      title: 'Recover',
      templateUrl: 'app/views/security/recover.html'
    })
      .state('page.resetpassword', {
      url: '/resetpassword/:token',
      controller : 'ResetPasswordController',
      templateUrl: 'app/views/security/resetPassword.html'
    })
      .state('page.lock', {
      url: '/lock',
      title: 'Lock',
      templateUrl: 'app/views/security/lock.html'
    })
      .state('page.404', {
      url: '/404',
      title: 'Not Found',
      templateUrl: 'app/pages/404.html'
    });

  } // routesConfig

})();


(function() {
  'use strict';
  angular
    .module('app.security')
    .service('authRequestInterceptor', authRequestInterceptor);

  authRequestInterceptor.$inject = ['tokenManager'];


  function authRequestInterceptor(tokenManager) {
    return {
      request: function (config) {

        if (config.headers.Authorization == undefined) {
          config.headers = config.headers || {};
          if ( tokenManager.get() !== undefined || tokenManager.get() !== null ) {
            config.headers.Authorization = tokenManager.get();        
          }
        }

        return config;
      }
    }
  }

})();
(function() {
  'use strict';
  angular
    .module('app.security')
    .service('authResponseInterceptor', authResponseInterceptor);

  authResponseInterceptor.$inject = ['$q', '$timeout', '$location',  '$rootScope', '$window', 'tokenManager', 'message'];


  function authResponseInterceptor($q, $timeout, $location,  $rootScope, $window, tokenManager, message) {
    return {
      response: function(response) {
        return response;
      },
      responseError: function (response) {
        var INVALID_TOKEN =  1;
        if(response.status == 0) {
          if ($rootScope.updatingToken == false) {
            message.show("warning", "No fue posible conectarse al servidor");
          }
          return $q.reject(response.status);
        } else if (response.data.ErrorCode !== undefined) {
          if (response.data.ErrorCode === INVALID_TOKEN && !$rootScope.updatingToken){  
            tokenManager.resetToken();
            if ($rootScope.updatingToken == false) {
              message.show("error", "No fue posible conectarse al servidor");
            }
            $location.path('page/login');
            return $q.reject(null);
          } else {
            return $q.reject(response.data);
          }
        } else if (response.data !== undefined) {
          return $q.resolve(response.data);
        } else {
          return $q.resolve(response);
        }
      }
    }
  }
  
})();
/**=========================================================
 * Module: app.cuadro4.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.security')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$scope', '$rootScope', '$state', 'securityHttp', 'tokenManager', 'message'];


  function LoginController($scope, $rootScope, $state, securityHttp, tokenManager, message) {

    (tokenManager.getRefreshToken()) ? $state.go('app.home') : null;
      
      

      
    $scope.login = {
      account : {
        user : '',
        password : ''
      },
      login : function() {
        var params = {
          user : $scope.login.account.user,
          pass : $scope.login.account.password
        };

        $rootScope.loadingVisible = true;

        securityHttp.login({}, params, function(response) {
          $rootScope.perfil = response.perfil;
          tokenManager.set(response);
          $state.go('app.home');
          $rootScope.loadingVisible = false;
        }, function(faild) {
          message.show('error', 'Usuario o Clave incorrecta');
          $rootScope.loadingVisible = false;
        })


        $rootScope.$on('refresh_token', function(event, param) {
          if ($rootScope.updatingToken == false) {
            $rootScope.updatingToken = true;
            securityHttp.refreshToken({}, param, function(token) {
              tokenManager.set(token);
              $rootScope.updatingToken = false;
            }, function(faild) {
              message.show('error', "No fue posible ingresar, con credenciales actuales");
              $rootScope.updatingToken = false;
            });
          }
        })

      }
    }

  }

})();
/**=========================================================
 * Module: app.cuadro4.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.security')
    .controller('RecoverController', RecoverController);

  RecoverController.$inject = ['$scope', '$rootScope', '$state', 'securityHttp','message'];


  function RecoverController($scope, $rootScope, $state, securityHttp, message) {

    $scope.recover = {
      model : {
        email : ''
      },
      resetPassword : function() {
        $rootScope.loadingVisible = true;
        securityHttp.resetPassword({},{ email : $scope.recover.model.email}, function() {
          message.show("info","Hemos enviado un correo electronico para continuar con el proceso");
          $state.go('page.login');
          $rootScope.loadingVisible = false;
        }, function(faild) {
          $state.go('page.login');
          message.show("info","Ocurrio un error al intentar enviar el mensaje de confirmación.");
          $rootScope.loadingVisible = false;
        });
      }
    }


  }

})();
/**=========================================================
 * Module: app.cuadro4.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.security')
    .controller('ResetPasswordController', ResetPasswordController);

  ResetPasswordController.$inject = ['$scope', '$rootScope', '$state', '$location', 'securityHttp', 'tokenManager', 'message'];


  function ResetPasswordController($scope, $rootScope, $state, $location, securityHttp, tokenManager, message) {


    $scope.reset = {
        model : {
            token : '',
            password : '',
            repeatPassword: ''
        },
      resetPassword : function() {
        if ($scope.reset.model.password == $scope.reset.model.repeatPassword) {
        debugger;
        $rootScope.loadingVisible = true;
        tokenManager.set({ token : $scope.reset.model.token });
        securityHttp.confirmPassword({}, { password : $scope.reset.model.password }, function(response) {
          $state.go('page.login');
          $rootScope.loadingVisible = false;
          tokenManager.resetToken();
          message.show('info', 'Su contraseña ha sido actualizada exitosamente.');
        }, function(faild) {
          tokenManager.resetToken();
          $state.go('page.recover');
          message.show('error', faild.Message);
          $rootScope.loadingVisible = false;
        })
          
        } else {
          message.show("info", "Las claves no corresponden");
          $scope.reset.model.password = '';
          $scope.reset.model.repeatPassword = '';          
        }
      }
    }
    
    $scope.reset.model.token = $location.url().split("/")[3];

       
      
  }

})();
(function() {
    'use strict';

    angular
        .module('app.security')
        .config(securityConfig);

    securityConfig.$inject = ['$httpProvider'];
    function securityConfig($httpProvider) {
      
      $httpProvider.interceptors.push('authRequestInterceptor');
      $httpProvider.interceptors.push('authResponseInterceptor');
      
    }

})();
(function() {
  'use strict';

  angular
    .module('app.security')
    .run(securityRun);

  securityRun.$inject = ['$rootScope', '$state', '$stateParams',  '$window', '$q', 'tokenManager', 'securityHttp', 'message'];

  function securityRun($rootScope, $state, $stateParams, $window, $q, tokenManager, securityHttp, message) {

    // Hook not found
    $rootScope.$on('$stateNotFound',
                   function(event, unfoundState/*, fromState, fromParams*/) {
      console.log(unfoundState.to); // "lazy.state"
      console.log(unfoundState.toParams); // {a:1, b:2}
      console.log(unfoundState.options); // {inherit:false} + default options
    });
    // Hook error
    $rootScope.$on('$stateChangeError',
                   function(event, toState, toParams, fromState, fromParams, error){
      console.log(error);
    });
    // Hook success

    var LAST_STATE = '';
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        
      if ((toState.name == 'page.recover' && fromState.name == 'page.login') || toState.name == 'page.resetpassword') {
      } else {
        if ((tokenManager.get() == undefined && toState.name != 'page.login') || (tokenManager.get() == '' && toState.name != 'page.login')) {
          if (LAST_STATE != toState.name) {
            event.preventDefault();  
            refreshToken().then(function() {
              LAST_STATE = 'app.home';
              $state.go('app.home');
            }, function() {
              $state.go('page.login');
            });
          }
        };        
      }
    });



    function refreshToken() {
      var defer = $q.defer();
      if (!tokenManager.get() && tokenManager.getRefreshToken()) {
        $rootScope.updatingToken = true;
        var param = { refreshToken : tokenManager.getRefreshToken() };
        tokenManager.resetToken();
        securityHttp.refreshToken({}, param, function(token) {
          tokenManager.set(token);
          $rootScope.updatingToken = false;
          defer.resolve(true);
        }, function(faild) {
          message.show("error", "No fue posible ingresar, con credenciales actuales");
          $rootScope.updatingToken = false;
          defer.reject();
        }); 
      } else {
        defer.reject();
      }
      return defer.promise;
    }
  }

})();
/**=========================================================
 * Module: app.report.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.security')
    .service('securityHttp', securityHttp);

  securityHttp.$inject = ['$resource', 'END_POINT'];


  function securityHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      'login' : {
        'method' : 'POST',
        'url' : END_POINT + '/Administracion.svc/authUser'
      },
      'refreshToken' : {
        'method' : 'POST',
        'url' : END_POINT + '/Administracion.svc/refreshToken'        
      },
      'resetPassword' : {
        'method' : 'POST',
        'url' : END_POINT + '/Administracion.svc/resetPassword'        
      },
      'confirmPassword' : {
        'method' : 'POST',
        'url' : END_POINT + '/Administracion.svc/confirmPassword'        
      }
    };

    return $resource(END_POINT + '/Administracion.svc/authUser', paramDefault, actions);
  }

})();

(function() {
  'use strict';

  angular
    .module('app.security')
    .service('tokenManager', tokenManager);

  tokenManager.$inject = ['$window', '$rootScope'];

  function tokenManager($window, $rootScope) {
    return ({
      set: setToken,
      get: getToken,
      getRefreshToken : getRefreshToken, 
      resetToken : resetToken
    });

    var token = '';

    function expirationTime(time) {
      console.log("expirationTime" + time);
      //var _time = (time * 60) - 60;
      var _time = time * 3;
      setTimeout(function() {
        $rootScope.$broadcast('refresh_token', { refreshToken : $window.localStorage.token } );
      }, _time * 1000);
    }

    function setToken(_token) {
      $rootScope.perfil = _token.perfil;
      token = _token.token;
      $window.localStorage.token = _token.refreshToken;
      expirationTime(_token.timeOut);
    }

    function getToken() {
      return token;
    }

    function getRefreshToken() {
      return $window.localStorage.token;
    }

    function resetToken() {
      token = '';
      $window.localStorage.removeItem("token");
    }


  }

})();




/**=========================================================
 * Module: app.forma30.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.seguimiento')
    .controller('seguimientoController', seguimientoController);

  seguimientoController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'seguimientoHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window'];


  function seguimientoController($scope, $rootScope, $state, $modalInstance, seguimientoHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window) {


    $scope.seguimiento = {
      model : {     
        id:0,  
        idReferencia : '',
        tipoReferencia : '',
        descripcion : '',
        usuarioReg : '',
        fechaReg : ''
          
      },
      paginations : {
        maxSize : 3,
        itemsPerPage : 3,
        currentPage : 0,
        totalItems : 0
      },
      filterText : '',
      dataSource : [],
      selectedItems : [],
      data : [],
      noData : true,
      loading : false,
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.seguimiento.filterText,
          "precision": false
        }];
        $scope.fileselectedItems = $filter('arrayFilter')($scope.seguimiento.dataSource, paramFilter);
        $scope.seguimiento.paginations.totalItems = $scope.seguimiento.selectedItems.length;
        $scope.seguimiento.paginations.currentPage = 1;
        $scope.seguimiento.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.seguimiento.paginations.currentPage == 1 ) ? 0 : ($scope.seguimiento.paginations.currentPage * $scope.seguimiento.paginations.itemsPerPage) - $scope.seguimiento.paginations.itemsPerPage;
        $scope.seguimiento.data = $scope.seguimiento.selectedItems.slice(firstItem , $scope.seguimiento.paginations.currentPage * $scope.seguimiento.paginations.itemsPerPage);
      },
      refresh : function() {
        var params = {
          idReferencia : $scope.seguimiento.model.idReferencia,
          tipoReferencia : $scope.seguimiento.model.tipoReferencia
        };
        $scope.seguimiento.getData(params);
      },
      setData : function(data) {
        $scope.seguimiento.selectedItems = data;
        $scope.seguimiento.dataSource = data;
        $scope.seguimiento.paginations.totalItems = $scope.seguimiento.selectedItems.length;
        $scope.seguimiento.paginations.currentPage = 1;
        $scope.seguimiento.changePage();
        $scope.seguimiento.noData = false;
        $scope.seguimiento.loading = false;
        ($scope.seguimiento.dataSource.length < 1) ? $scope.seguimiento.noData = true : null;
      },
      getData : function(params) {
        $scope.seguimiento.data = [];
        $scope.seguimiento.loading = true;
        $scope.seguimiento.noData = false;

        seguimientoHttp.getSeguimientos({}, params, function(response) {
          $scope.seguimiento.setData(response);
        })
      },
      visualizar : function(id) {
        var params = {
          id : id 
        };

        seguimientoHttp.getSeguimiento({}, params, function(response) {
          if (response) {
              $scope.seguimiento.model.descripcion=response.descripcion;
              
          }
        })
      },
      eliminar : function(id) {
        var params = {
          id : id 
        };

        $rootScope.loadingVisible = true;
        seguimientoHttp.removeSeguimiento(params, function(response) {
          $scope.seguimiento.refresh();
          message.show("info", "Seguimiento eliminado correctamente");
          $rootScope.loadingVisible = false;
        }, function() {
          message.show("error", "Ocurrio un error al intentar eliminar el adjunto");
          $rootScope.loadingVisible = false;
        });
      },     
      close : function() {
        $modalInstance.dismiss('cancel');
      },        
      save : function() {	
          
        $rootScope.loadingVisible = true;
        seguimientoHttp.addSeguimiento({}, $scope.seguimiento.model, function (data) { 

             message.show("success", "Seguimiento creaado correctamente");
             $rootScope.loadingVisible = false;
             $scope.seguimiento.refresh();

        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      }
    }
    
    $scope.hideDelete=true;
    
    $scope.seguimiento.model.idReferencia = parameters.id.toString();
    $scope.seguimiento.model.tipoReferencia = parameters.referencia.toString();
    //$scope.hideDelete = (parameters.hideDelete) ? parameters.hideDelete : false;
    $scope.seguimiento.refresh();

  }

})();
/**=========================================================
 * Module: app.forma30.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.seguimiento')
    .service('seguimientoHttp', seguimientoHttp);

  seguimientoHttp.$inject = ['$resource', 'END_POINT'];


  function seguimientoHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
        
        'addSeguimiento': {
        'method':'POST',
        'url' : END_POINT + '/General.svc/addSeguimiento'
        },
      getSeguimientos : {
        method : 'POST',
        isArray : true
      },
      getSeguimiento : {
        method : 'GET',
        url : END_POINT + '/General.svc/seguimiento/:id'
      },
      removeSeguimiento : {
        method : 'DELETE',
        isArray : true,
        url : END_POINT + '/General.svc/seguimiento/:id'
      }
    };

    return $resource(END_POINT + '/General.svc/seguimiento', paramDefault, actions);
  }

})();

(function() {
    'use strict';

    angular
        .module('app.settings')
        .run(settingsRun);

    settingsRun.$inject = ['$rootScope', '$localStorage'];

    function settingsRun($rootScope, $localStorage){
      
      // Global Settings
      // ----------------------------------- 
      $rootScope.app = {
        name: 'CRM - L&Q',
        description: 'Sistema Información',
        year: ((new Date()).getFullYear()),
        layout: {
          isFixed: true,
          isCollapsed: false,
          isBoxed: false,
          isRTL: false,
          horizontal: false,
          isFloat: false,
          asideHover: false,
          theme: 'app/css/anh.css'
        },
        useFullLayout: false,
        hiddenFooter: false,
        offsidebarOpen: false,
        asideToggled: false,
        viewAnimation: 'ng-fadeInUp'
      };

      // Setup the layout mode
      $rootScope.app.layout.horizontal = ( $rootScope.$stateParams.layout === 'app-h') ;

      // Restore layout settings
      if( angular.isDefined($localStorage.layout) )
        $rootScope.app.layout = $localStorage.layout;
      else
        $localStorage.layout = $rootScope.app.layout;

      $rootScope.$watch('app.layout', function () {
        $localStorage.layout = $rootScope.app.layout;
      }, true);

      // Close submenu when sidebar change from collapsed to normal
      $rootScope.$watch('app.layout.isCollapsed', function(newValue) {
        if( newValue === false )
          $rootScope.$broadcast('closeSidebarMenu');
      });

    }

})();

/**=========================================================
 * Module: sidebar-menu.js
 * Handle sidebar collapsible elements
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.sidebar')
    .controller('SidebarController', SidebarController);

  SidebarController.$inject = ['$rootScope', '$scope', '$state', 'Utils', 'sidebarHttp', 'parametersOfState', 'TO_STATE'];
  function SidebarController($rootScope, $scope, $state, Utils, sidebarHttp, parametersOfState, TO_STATE) {

    activate();

    function activate() {
      var collapseList = [];

      // demo: when switch from collapse to hover, close all items
      $rootScope.$watch('app.layout.asideHover', function(oldVal, newVal) {
        if ( newVal  === false && oldVal === true ) {
          closeAllBut(-1);
        }
      });

      $scope.changeOptions = function(item) {
        TO_STATE = '';
        if (item.sref != '') {
          var param = (item.param != '') ? JSON.parse(item.param) : null;
          parametersOfState.set({ name : item.sref, params : param });
          $state.go(item.sref);
          if ($state.current.name == 'app.reporte') {
            $rootScope.$broadcast('changeOptions', null);
          }
        }
      }


      // Load menu from json file
      // ----------------------------------- 

      sidebarHttp.getMenu({},{},function(response) {
        $scope.menuItems = response; 
      }, function(faild) {
        $scope.menuItems = [];
      })

      // Handle sidebar and collapse items
      // ----------------------------------

      $scope.getMenuItemPropClasses = function(item) {
        return (item.heading ? 'nav-heading' : '') +
          (isActive(item) ? ' active' : '') ;
      };

      $scope.addCollapse = function($index, item) {
        collapseList[$index] = $rootScope.app.layout.asideHover ? true : !isActive(item);
      };

      $scope.isCollapse = function($index) {
        return (collapseList[$index]);
      };

      $scope.toggleCollapse = function($index, isParentItem, param) {
        // collapsed sidebar doesn't toggle drodopwn
        if( Utils.isSidebarCollapsed() || $rootScope.app.layout.asideHover ) return true;

        // make sure the item index exists
        if( angular.isDefined( collapseList[$index] ) ) {
          if ( ! $scope.lastEventFromChild ) {
            collapseList[$index] = !collapseList[$index];
            closeAllBut($index);
          }
        }
        else if ( isParentItem ) {
          closeAllBut(-1);
        }

        $scope.lastEventFromChild = isChild($index);

        return true;

      };

      // Controller helpers
      // ----------------------------------- 

      // Check item and children active state
      function isActive(item) {

        if(!item) return;

        if( !item.sref || item.sref === '#') {
          var foundActive = false;
          angular.forEach(item.submenu, function(value) {
            if(isActive(value)) foundActive = true;
          });
          return foundActive;
        }
        else
          return $state.is(item.sref) || $state.includes(item.sref);
      }

      function closeAllBut(index) {
        index += '';
        for(var i in collapseList) {
          if(index < 0 || index.indexOf(i) < 0)
            collapseList[i] = true;
        }
      }

      function isChild($index) {
        /*jshint -W018*/
        return (typeof $index === 'string') && !($index.indexOf('-') < 0);
      }


    } // activate
  }

})();

/**=========================================================
 * Module: sidebar.js
 * Wraps the sidebar and handles collapsed state
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .directive('sidebar', sidebar);

    sidebar.$inject = ['$rootScope', '$timeout', '$window', 'Utils'];
    function sidebar ($rootScope, $timeout, $window, Utils) {
        var $win = angular.element($window);
        var directive = {
            // bindToController: true,
            // controller: Controller,
            // controllerAs: 'vm',
            link: link,
            restrict: 'EA',
            template: '<nav class="sidebar" ng-transclude></nav>',
            transclude: true,
            replace: true
            // scope: {}
        };
        return directive;

        function link(scope, element, attrs) {

          var currentState = $rootScope.$state.current.name;
          var $sidebar = element;

          var eventName = Utils.isTouch() ? 'click' : 'mouseenter' ;
          var subNav = $();

          $sidebar.on( eventName, '.nav > li', function() {

            if( Utils.isSidebarCollapsed() || $rootScope.app.layout.asideHover ) {

              subNav.trigger('mouseleave');
              subNav = toggleMenuItem( $(this), $sidebar);

              // Used to detect click and touch events outside the sidebar          
              sidebarAddBackdrop();

            }

          });

          scope.$on('closeSidebarMenu', function() {
            removeFloatingNav();
          });

          // Normalize state when resize to mobile
          $win.on('resize', function() {
            if( ! Utils.isMobile() )
          	asideToggleOff();
          });

          // Adjustment on route changes
          $rootScope.$on('$stateChangeStart', function(event, toState) {
            currentState = toState.name;
            // Hide sidebar automatically on mobile
            asideToggleOff();

            $rootScope.$broadcast('closeSidebarMenu');
          });

      	  // Autoclose when click outside the sidebar
          if ( angular.isDefined(attrs.sidebarAnyclickClose) ) {
            
            var wrapper = $('.wrapper');
            var sbclickEvent = 'click.sidebar';
            
            $rootScope.$watch('app.asideToggled', watchExternalClicks);

          }

          //////

          function watchExternalClicks(newVal) {
            // if sidebar becomes visible
            if ( newVal === true ) {
              $timeout(function(){ // render after current digest cycle
                wrapper.on(sbclickEvent, function(e){
                  // if not child of sidebar
                  if( ! $(e.target).parents('.aside').length ) {
                    asideToggleOff();
                  }
                });
              });
            }
            else {
              // dettach event
              wrapper.off(sbclickEvent);
            }
          }

          function asideToggleOff() {
            $rootScope.app.asideToggled = false;
            if(!scope.$$phase) scope.$apply(); // anti-pattern but sometimes necessary
      	  }
        }
        
        ///////

        function sidebarAddBackdrop() {
          var $backdrop = $('<div/>', { 'class': 'dropdown-backdrop'} );
          $backdrop.insertAfter('.aside-inner').on('click mouseenter', function () {
            removeFloatingNav();
          });
        }

        // Open the collapse sidebar submenu items when on touch devices 
        // - desktop only opens on hover
        function toggleTouchItem($element){
          $element
            .siblings('li')
            .removeClass('open')
            .end()
            .toggleClass('open');
        }

        // Handles hover to open items under collapsed menu
        // ----------------------------------- 
        function toggleMenuItem($listItem, $sidebar) {

          removeFloatingNav();

          var ul = $listItem.children('ul');
          
          if( !ul.length ) return $();
          if( $listItem.hasClass('open') ) {
            toggleTouchItem($listItem);
            return $();
          }

          var $aside = $('.aside');
          var $asideInner = $('.aside-inner'); // for top offset calculation
          // float aside uses extra padding on aside
          var mar = parseInt( $asideInner.css('padding-top'), 0) + parseInt( $aside.css('padding-top'), 0);
          var subNav = ul.clone().appendTo( $aside );
          
          toggleTouchItem($listItem);

          var itemTop = ($listItem.position().top + mar) - $sidebar.scrollTop();
          var vwHeight = $win.height();

          subNav
            .addClass('nav-floating')
            .css({
              position: $rootScope.app.layout.isFixed ? 'fixed' : 'absolute',
              top:      itemTop,
              bottom:   (subNav.outerHeight(true) + itemTop > vwHeight) ? 0 : 'auto'
            });

          subNav.on('mouseleave', function() {
            toggleTouchItem($listItem);
            subNav.remove();
          });

          return subNav;
        }

        function removeFloatingNav() {
          $('.dropdown-backdrop').remove();
          $('.sidebar-subnav.nav-floating').remove();
          $('.sidebar li.open').removeClass('open');
        }
    }


})();


(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .controller('UserBlockController', UserBlockController);

    UserBlockController.$inject = ['$scope','$rootScope', '$modal'];
    function UserBlockController($scope,$rootScope,$modal) {

        activate();

        ////////////////

        function activate() {
          $rootScope.user = {
            name:     'John',
            job:      'ng-developer',
            picture:  'app/img/user/02.jpg'
          };

          // Hides/show user avatar on sidebar
          $rootScope.toggleUserBlock = function(){
            $rootScope.$broadcast('toggleUserBlock');
          };

          $rootScope.userBlockVisible = true;
          
          $rootScope.$on('toggleUserBlock', function(/*event, args*/) {

            $rootScope.userBlockVisible = ! $rootScope.userBlockVisible;
            
          });
        }
      $rootScope.changeImg = function(){
        var modalInstance = $modal.open({
          templateUrl: 'app/views/partials/user-photo.html',
          controller: 'userPhotoController',
          size: 'lg',
          resolve: {
            parameters: function () { //return parameter; 
            }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }
      
      
      $rootScope.myImage='';
    $rootScope.myCroppedImage='';
      
    $rootScope.setEditing=function(varEditing) {
      $scope.editing =varEditing;
    };
      
     $rootScope.handleFileSelect=function() {
    
      var file=event.target.files[0];
      var reader = new FileReader();
      reader.onload = function (event) {
        $rootScope.$apply(function($scope){
          debugger
          $scope.myImage=event.target.result;
        
          
        });
      };
         $scope.editing = true;
      reader.readAsDataURL(file);
    };
      
    }
})();

(function() {
  'use strict';

  angular
    .module('app.sidebar')
    .controller('userPhotoController', userPhotoController);

  userPhotoController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'parameters'];


  function userPhotoController($scope, $filter, $state, $modalInstance, parameters) {
    $scope.myImage='';
    $scope.myCroppedImage='';

     $scope.handleFileSelect=function() {
      debugger;
       alert(event.target.files[0].name);
      var file=event.target.files[0];
      var reader = new FileReader();
      reader.onload = function (event) {
        $scope.$apply(function($scope){
          debugger;
          $scope.myImage=event.target.result;
        });
      };
      reader.readAsDataURL(file);
    };
    
//    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
  

    
  }
})();
/**=========================================================
 * Module: app.report.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.sidebar')
    .service('sidebarHttp', sidebarHttp);

  sidebarHttp.$inject = ['$resource', 'END_POINT'];


  function sidebarHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      'getMenu' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Administracion.svc/menu'
      }
    };

    return $resource(END_POINT + '/Administracion.svc/autmenuhUser', paramDefault, actions);
  }

})();
(function() {
  'use strict';

  angular
    .module('app.sidebar')
    .controller('TopnavbarController', TopnavbarController);

  TopnavbarController.$inject = ['$scope', '$state', 'tokenManager', 'ngDialog'];
  function TopnavbarController($scope, $state, tokenManager, ngDialog) {

    $scope.logout = function() {

      ngDialog.openConfirm({
        template: 'app/views/templates/logout_confirmation.html',
        className: 'ngdialog-theme-default',
        scope: $scope
      }).then(function (value) {
        tokenManager.resetToken();
        $state.go('page.login');
      });        
    }

  }

})();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('consultaTerceroController', consultaTerceroController);

  consultaTerceroController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'terceroHttp',   'parameters', 'message', 'parametersOfState','$rootScope', 'REGULAR_EXPRESION'];


  function consultaTerceroController($scope, $filter, $state, $modalInstance, LDataSource, terceroHttp, parameters, message, parametersOfState,$rootScope, REGULAR_EXPRESION) {      
    
      
      $scope.consultaTercero = {     
          model:{
                  id: 0,
                  identificacion: '', 
                  nombre: '', 
                  telefono: '',
                  direccion: '',
                  correo: '',
                  tipoId: 0,
                  estado: '',			  
                  fechaAct: '',
                  usuarioAct: '',
                  fechaReg: '',
                  usuarioReg: ''
          },	
          fechaReg : {
            isOpen : false,
            value:'',
            dateOptions : {
              formatYear: 'yy',
              startingDay: 1
            },
            open : function($event) {
              $scope.tercero.fechaReg.isOpen = false;
            }
          },   
            fechaAct : {
            isOpen : false,
            value:'',
            dateOptions : {
              formatYear: 'yy',
              startingDay: 1
            },
            open : function($event) {
              $scope.tercero.fechaAct.isOpen = false;
            }
          },
         save:  function(){                    
             
                if(!$scope.funcionarios.current){message.show("warning", "Funcionario requerido");return;
                }else{$scope.participanteContrato.model.funcionarioId = $scope.funcionarios.current.id;}
          
                if(!$scope.cargo.current){message.show("warning", "Cargo requerido");return;}
                else{$scope.participanteContrato.model.cargoId= $scope.cargo.current.codigo;}	

             var _http = new terceroHttp($scope.participanteContrato.model);
             if(  $scope.participanteContrato.model.id==0){
                 
                  _http.$addParticipanteContrato(function(response){                  
                      message.show("success", "Participante creado satisfactoriamente!!");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editParticipanteContrato(function(response){                  
                      message.show("success", "Participante actualizado satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }             
              $modalInstance.close();
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }
      
      
    
      //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'I', descripcion: 'Inactivo'});
            $scope.estados.data.push({value:'A', descripcion: 'Activo'});
            $scope.estados.data.push({value:'D', descripcion: 'Eliminado'});   
        }        
    }
    //CONTACTO
    $scope.contactoTercero = {
    model : {  
            id: 0,
            nombre: '',
            telefono: '',
			ext: '',
			email: ''
		},
      current : {},
      data:[],
      getData : function() {        
              
            
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              terceroHttp.getContactosTercero({}, { terceroId: parameters.id }, function(response) {
                  $scope.contactoTercero.data = $filter('orderBy')(response, 'nombre');
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
          
      },
       add : function() {          
           
           
        var parameter = {
              contactoTercero : {              
                      id: 0,
                      terceroId:$scope.tercero.model.id,
                      nombre: '',
                      telefono: '',
                      ext: '',
                      email: ''
              }            
        };

        var modalInstance = $modal.open({
          templateUrl: 'app/views/tercero/contactoTercero_edit.html',
          controller: 'contactoTerceroEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
        modalInstance.result.then(function (parameters) {            
            $scope.contactoTercero.getData();
        });
           
      },
      delete : function(item){
          
          $rootScope.loadingVisible = true;          
          terceroHttp.deleteContactoTercero({}, {contactoTerceroId: item.id}, function(response){
            $scope.contactoTercero.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
      },
      edit : function(item) {
        var parameter = {
          contactoTercero : item
        };
        var modalInstance = $modal.open({
        templateUrl: 'app/views/tercero/contactoTercero_edit.html',
          controller: 'contactoTerceroEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
          
        modalInstance.result.then(function (parameters) {   
            $scope.contactoTercero.getData();
        });
      }
    }
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();	
    $scope.modoEdicion=true;
    $scope.modoActualizacion=false;
      
    //DEFINIMOS TITULO 
    if( parameters.referencia.toString()=="CLIENTE")
    {
        $scope.destino="Cliente";            
    }
      
      terceroHttp.read({},{ id : parameters.id}, function (data) { 
            $scope.consultaTercero.model = data;			
             
			if($scope.consultaTercero.model.fechaReg){$scope.consultaTercero.fechaReg.value=new Date(parseFloat($scope.consultaTercero.model.fechaReg));}    
			if($scope.consultaTercero.model.fechaAct){$scope.consultaTercero.fechaAct.value=new Date(parseFloat($scope.consultaTercero.model.fechaAct));}   
            
			       
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.consultaTercero.model.estado})[0];            
			$scope.contactoTercero.getData();
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });  
      
    
      
  }
})();
/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .controller('contactoTerceroEditController', contactoTerceroEditController);

  contactoTerceroEditController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'terceroHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];


  function contactoTerceroEditController($scope, $filter, $state, $modalInstance, LDataSource, terceroHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {
      
      
      
      $scope.contactoTercero = {     
          model:{
              id: 0,
              terceroId: 0,
              nombre: '',
              telefono: '',
              ext: '',
              email: '',
              envioInf: '',
              usuario: '',
              clave: '',
              claveTmp: ''
          },         
          
         save:  function(){
             
                if($scope.contactoTercero.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
                if(!$scope.envioInf.current){message.show("warning", "Envio información requerido");return;}
                else{$scope.contactoTercero.model.envioInf= $scope.envioInf.current.value;}	             
                
                 if($scope.contactoTercero.model.usuario!=""){
                     if($scope.contactoTercero.model.clave==""){
                            message.show("warning", "Debe especificar al menos una clave");return;
                        }
                     }
                else{
                    $scope.contactoTercero.model.clave='';
                    $scope.contactoTercero.model.claveTmp='';                    
                }                       
             
             var _http = new terceroHttp($scope.contactoTercero.model);
             if(  $scope.contactoTercero.model.id==0){
                 
                  _http.$addContactoTercero(function(response){                  
                      message.show("success", "Contacto creado satisfactoriamente!!");                       
                      $scope.contactoTercero.close();
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });             
             }
             else{         
                 
                  _http.$editContactoTercero(function(response){                  
                      message.show("success", "Contacto actualizado satisfactoriamente");
                      $scope.contactoTercero.close();                
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }             
             
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }
      
       //ESTADOS
        $scope.envioInf = {
            current : {}, data:[],
            getData : function() {
                $scope.envioInf.data.push({value:'NA', descripcion: 'No Autorizado'}); 
                $scope.envioInf.data.push({value:'A', descripcion: 'Autorizado'});
            }        
        }	
	   //CARGAMOS LOS LISTADOS	
	   $scope.envioInf.getData();	      
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
      $scope.contactoTercero.model=parameters.contactoTercero;
      $scope.envioInf.current = $filter('filter')($scope.envioInf.data, {value :  $scope.contactoTercero.envioInf})[0];
  }
})();
/**=========================================================
 * Module: app.tercero.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .controller('terceroController', terceroController);

  terceroController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'terceroHttp', 'ngDialog', 'tpl', '$modal','message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function terceroController($scope, $filter, $state, LDataSource, terceroHttp, ngDialog, tpl, $modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
      var tercero = $state.params.tercero;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?.¿ ]+$/;
      $scope.tercero = {
		model : {
				  id: 0,
                  identificacion: '', 
                  nombre: '',                    
                  telefono: '',
                  direccion: '',
                  correo: '',
                  tipoId: 0,
                  estado: '',			  
                  fechaAct: '',
                  usuarioAct: '',
                  fechaReg: '',
                  usuarioReg: ''
		},   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.tercero.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.tercero.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.tercero', params : { filters : {estadoSeleccionado:tercero.estado}, data : []} });
        $state.go('app.tercero');
      },
      save : function() {		  
       
		          
          		if($scope.tercero.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
				if($scope.tercero.model.identificacion ==""){message.show("warning", "Identificación requerido");return;}	
                //if($scope.tercero.model.telefono ==""){message.show("warning", "Teléfono requerido");return;}	
                //if($scope.tercero.model.direccion ==""){message.show("warning", "Dirección requerido");return;}	
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.tercero.model.estado= $scope.estados.current.value;}			
				
			
          
			//INSERTAR
            if($scope.tercero.model.id==0){
				
				$rootScope.loadingVisible = true;
				terceroHttp.save({}, $scope.tercero.model, function (data) { 
					
						terceroHttp.read({},{ id : data.id}, function (data) {
							$scope.tercero.model=data;
							tercero.id=$scope.tercero.model.id; 
							if($scope.tercero.model.fechaReg){$scope.tercero.fechaReg.value=new Date(parseFloat($scope.tercero.model.fechaReg));}    
							if($scope.tercero.model.fechaAct){$scope.tercero.fechaAct.value=new Date(parseFloat($scope.tercero.model.fechaAct));} 		
								
							message.show("success", "Tercero creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.tercero.model.id){
					$state.go('app.tercero');
				}else{					 
					terceroHttp.update({}, $scope.tercero.model, function (data) {
					  $rootScope.loadingVisible = false;
					  $scope.tercero.model=data;
						if($scope.tercero.model.fechaReg){$scope.tercero.fechaReg.value=new Date(parseFloat($scope.tercero.model.fechaReg));}    
						if($scope.tercero.model.fechaAct){$scope.tercero.fechaAct.value=new Date(parseFloat($scope.tercero.model.fechaAct));}   					  
					   message.show("success", "Tercero actualizado satisfactoriamente");							
					  
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.tercero.model.id,
                         referencia : 'TERCERO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.tercero.model.id,
                         referencia : 'TERCERO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }
    }
      
    $scope.contactoTercero = {
    model : {  
            id: 0,
            nombre: '',
            telefono: '',
			ext: '',
			email: ''
		},
      current : {},
      data:[],
      getData : function() {
          if(tercero){
              
            if($scope.tercero.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              terceroHttp.getContactosTercero({}, { terceroId: $scope.tercero.model.id }, function(response) {
                  $scope.contactoTercero.data = $filter('orderBy')(response, 'nombre');
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },
       add : function() {          
           
           
        var parameter = {
              contactoTercero : {              
                      id: 0,
                      terceroId:$scope.tercero.model.id,
                      nombre: '',
                      telefono: '',
                      ext: '',
                      email: ''
              }            
        };

        var modalInstance = $modal.open({
          templateUrl: 'app/views/tercero/contactoTercero_edit.html',
          controller: 'contactoTerceroEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
        modalInstance.result.then(function (parameters) {}, function (parameters) { 
            
            $scope.contactoTercero.getData();
            
        });
           
      },
      delete : function(item){
          
          $rootScope.loadingVisible = true;          
          terceroHttp.deleteContactoTercero({}, {contactoTerceroId: item.id}, function(response){
            $scope.contactoTercero.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
      },
      edit : function(item) {
        var parameter = {
          contactoTercero : item
        };
        
        var modalInstance = $modal.open({
        templateUrl: 'app/views/tercero/contactoTercero_edit.html',
          controller: 'contactoTerceroEditController',
          size: 'lg',
          resolve: {
             
            parameters: function () { return parameter; }
          }
        });        
        modalInstance.result.then(function (parameters) {}, function (parameters) {$scope.contactoTercero.getData();});
      }
    }
    //TAB
     $scope.showTabs= function(){
        if ($scope.tercero.id==0)
          return false;
        else
          return true;
    }
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'I', descripcion: 'Inactivo'});
            $scope.estados.data.push({value:'A', descripcion: 'Activo'});
            $scope.estados.data.push({value:'D', descripcion: 'Eliminado'});   
        }        
    }	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();	
	
	//CARGAMOS LOS DATOS DEL TERCERO
	
	if(tercero.id==0){
      $scope.estados.current=$scope.estados.data[0]; 
    }
    else{        
        $rootScope.loadingVisible = true;
		$scope.divContactoTercero=true;
        terceroHttp.read({},$state.params.tercero, function (data) { 
            $scope.tercero.model = data;			
             
			if($scope.tercero.model.fechaReg){$scope.tercero.fechaReg.value=new Date(parseFloat($scope.tercero.model.fechaReg));}    
			if($scope.tercero.model.fechaAct){$scope.tercero.fechaAct.value=new Date(parseFloat($scope.tercero.model.fechaAct));}   
            
			       
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.tercero.model.estado})[0];            
			$scope.contactoTercero.getData();
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
	
    
    
  }
})();
/**=========================================================
 * Module: app.tercero.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .service('terceroHttp', terceroHttp);

  terceroHttp.$inject = ['$resource', 'END_POINT'];


  function terceroHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {     
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/tercero/:id'
      },
      'save':   {
        'method':'POST'
      },
      'update' : {
        'method' : 'PUT'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/tercero/:id'
      },
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            estado : '@estado'           
          },
          'url' : END_POINT + '/Comercial.svc/tercero/estado/:estado'          
      },
      'getContactosTercero': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            terceroId : '@terceroId'           
          },
          'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },
      'addContactoTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/tercero/contacto'
      },
      'editContactoTercero': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/tercero/contacto'
      },
      'deleteContactoTercero': {
        'method':'DELETE',
        'params' : {
            contactoTerceroId : '@contactoTerceroId'           
          },
        'url' : END_POINT + '/Comercial.svc/tercero/contacto/:contactoTerceroId'
      }
     
    };
    return $resource( END_POINT + '/Comercial.svc/tercero', {}, actions, {}); 
  }

})();
/**=========================================================
 * Module: app.tercero.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .controller('terceroListController', terceroListController);

  terceroListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'terceroHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function terceroListController($scope, $filter, $state, ngDialog, tpl, terceroHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
		
	$scope.ESTADO_INACTIVO = 'I';
    $scope.ESTADO_ACTIVO = 'A';
    $scope.ESTADO_ELIMINADO = 'D';
	//$rootScope.loadingVisible = true;

    var estadoSeleccionado= $state.params.filters.estadoSeleccionado;
    $scope.terceros = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreterceros:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.terceros.selectedAll = !$scope.terceros.selectedAll; 
        for (var key in $scope.terceros.selectedItems) {
          $scope.terceros.selectedItems[key].check = $scope.terceros.selectedAll;
        }
      },
      add : function() {
          var terc = {
              id: 0,
              identificacion: '', 
              nombre: '', 
			  telefono: '',
			  direccion: '',
			  correo: '',
              tipoId: 0,
			  estado: '',			  
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: ''			 
          };
        parametersOfState.set({ name : 'app.tercero_add', params : { tercero: terc } });
        $state.go('app.tercero_add');
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.tercero_edit', params : { tercero: item } });
        $state.go('app.tercero_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            terceroHttp.remove({}, { id: id }, function(response) {
                $scope.terceros.getData();
                message.show("success", "Tercero eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.terceros.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    terceroHttp.remove({}, { id: id }, function(response) {
                        $scope.terceros.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.terceros.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.terceros.filterText,
          "precision": false
        }];
        $scope.terceros.selectedItems = $filter('arrayFilter')($scope.terceros.dataSource, paramFilter);
        $scope.terceros.paginations.totalItems = $scope.terceros.selectedItems.length;
        $scope.terceros.paginations.currentPage = 1;
        $scope.terceros.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.terceros.paginations.currentPage == 1 ) ? 0 : ($scope.terceros.paginations.currentPage * $scope.terceros.paginations.itemsPerPage) - $scope.terceros.paginations.itemsPerPage;
        $scope.terceros.data = $scope.terceros.selectedItems.slice(firstItem , $scope.terceros.paginations.currentPage * $scope.terceros.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.terceros.data = [];
        $scope.terceros.loading = true;
        $scope.terceros.noData = false;
          
        var parametros= {
            "estado":$scope.estadoTercero.current.value                
        };
          
        terceroHttp.getList({}, parametros,function(response) {
          $scope.terceros.selectedItems = response;
          $scope.terceros.dataSource = response;
          for(var i=0; i<$scope.terceros.dataSource.length; i++){
            $scope.terceros.nombreterceros.push({id: i, nombre: $scope.terceros.dataSource[i]});
          }
          $scope.terceros.paginations.totalItems = $scope.terceros.selectedItems.length;
          $scope.terceros.paginations.currentPage = 1;
          $scope.terceros.changePage();
          $scope.terceros.loading = false;
          ($scope.terceros.dataSource.length < 1) ? $scope.terceros.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
        //CARGAMOS PROCESOS
    $scope.estadoTercero = {
        current : {}, data:[],
        getData : function() {
            $scope.estadoTercero.data.push({value:'A', descripcion: 'Activos'});
            $scope.estadoTercero.data.push({value:'I', descripcion: 'Inactivos'});
            $scope.estadoTercero.data.push({value:'D', descripcion: 'Eliminados'});
            $scope.estadoTercero.current =$scope.estadoTercero.data[0];
        }        
    }
	//CARGAMOS LOS LISTADOS
    
    $scope.estadoTercero.getData();
    $scope.terceros.getData();
        
         if(estadoSeleccionado){

            $scope.estadoTercero.current = $filter('filter')($scope.estadoTercero.data, {value : estadoSeleccionado})[0];        
            $scope.terceros.getData();        
        }
        
        
	}
  
  
  })();
(function() {
    'use strict';

    angular
        .module('app.translate')
        .config(translateConfig)
        ;
    translateConfig.$inject = ['$translateProvider'];
    function translateConfig($translateProvider){
  
      $translateProvider.useStaticFilesLoader({
          prefix : 'app/i18n/',
          suffix : '.json'
      });
      $translateProvider.preferredLanguage('es');
      $translateProvider.useLocalStorage();
      $translateProvider.usePostCompiling(true);

    }
})();
(function() {
    'use strict';

    angular
        .module('app.translate')
        .run(translateRun)
        ;
    translateRun.$inject = ['$rootScope', '$translate'];
    
    function translateRun($rootScope, $translate){

      // Internationalization
      // ----------------------

      $rootScope.language = {
        // Handles language dropdown
        listIsOpen: false,
        // list of available languages
        available: {
          'en':       'English',
          'es':    'Español'
        },
        // display always the current ui language
        init: function () {
          var proposedLanguage = $translate.proposedLanguage() || $translate.use();
          var preferredLanguage = $translate.preferredLanguage(); // we know we have set a preferred one in app.config
          $rootScope.language.selected = $rootScope.language.available[ (proposedLanguage || preferredLanguage) ];
        },
        set: function (localeId) {
          // Set the new idiom
          $translate.use(localeId);
          // save a reference for the current language
          $rootScope.language.selected = $rootScope.language.available[localeId];
          // finally toggle dropdown
          $rootScope.language.listIsOpen = ! $rootScope.language.listIsOpen;
        }
      };

      $rootScope.language.init();

    }
})();
/**=========================================================
 * Module: modals.js
 * Provides a simple way to implement bootstrap modals from templates
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.usuario')
        .controller('ModalGmapController', ModalGmapController);

    ModalGmapController.$inject =     
    ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'usuarioHttp',   'parameters', 'message', 'parametersOfState','$rootScope', 'REGULAR_EXPRESION','$timeout'];
      
    function ModalGmapController($scope, $filter, $state, $modalInstance, LDataSource, usuarioHttp, parameters, message, parametersOfState,$rootScope, REGULAR_EXPRESION,$timeout) { 
        
        var  objUbi = parameters.objUbicacion;
        var registroUbicacion = parameters.registroUbicacion;   
        var vm = this;       
        
        $scope.registroUbicacionModal={               
                     
            model : {
                id:0,
                contratoId:0,
                funcionarioId:0,
                fecha: '',
                fechaInicio: '',
                tipo: '',
                comentario: '',
                longitude : 0,
                latitude : 0,
                accuracy : 0
		},             
            
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          save : function() {
                             
                if($scope.tipos.current.value==''){message.show("warning", "Tipo registro requerido");return;}
                else{$scope.registroUbicacionModal.model.tipo= $scope.tipos.current.value;}   
                $scope.registroUbicacionModal.model.fechaInicio= Date.parse(new Date($scope.registroUbicacionModal.model.fechaInicio));
               
                    
                $rootScope.loadingVisible = true;
				usuarioHttp.addRegistroUbicacion({}, $scope.registroUbicacionModal.model, function (data) { 
					
						usuarioHttp.getRegistroUbicacion({},{ id : data.id}, function (data) {                           
							$scope.registroUbicacionModal.model=data;                         
								
							message.show("success", "Ubicación Registrada satisfactoriamente!!");
                             $modalInstance.close();                       
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});              
          } 
        }
        
    //TIPOS
    $scope.tipos = {
        current : {}, data:[],
        getData : function() {
            $scope.tipos.data.push({value:'', descripcion: 'Seleccione'});
            $scope.tipos.data.push({value:'E', descripcion: 'Entrada'});
            $scope.tipos.data.push({value:'S', descripcion: 'Salida'});
            $scope.tipos.current=$scope.tipos.data[0];
        }        
    }   
    
   //INICIALIZAMOS EL MAPA   
    
    //CARGAMOS LISTADO
    $scope.tipos.getData();        
    if(registroUbicacion.id==0){   
       
            $scope.registroUbicacionModal.model.longitude=objUbi.long;
            $scope.registroUbicacionModal.model.latitude=objUbi.lat;
            $scope.registroUbicacionModal.model.accuracy=objUbi.accu;
            $scope.registroUbicacionModal.model.contratoId=registroUbicacion.contratoId;
            $scope.registroUbicacionModal.model.funcionarioId=registroUbicacion.funcionarioId;  
            $scope.registroUbicacionModal.model.fechaInicio=new Date();             
			
    }
    else{ 
         
         usuarioHttp.getRegistroUbicacion({}, { id : registroUbicacion.id} , function (data) { 
            
            $scope.registroUbicacionModal.model = data;	             
            $scope.registroUbicacionModal.model.id=registroUbicacion.id;
            $scope.tipos.current = $filter('filter')($scope.tipos.data, {value : $scope.registroUbicacionModal.model.tipo})[0];
            
            $scope.registroUbicacionModal.model.fecha=new Date(parseFloat(data.fecha));
            $scope.registroUbicacionModal.model.fechaInicio=new Date(parseFloat(data.fechaInicio));  
             
             setUbicacion($scope.registroUbicacionModal.model.latitude,$scope.registroUbicacionModal.model.longitude);
                
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });  
           
      
    }   
      
}
    
    
    


    })();

/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.usuario')
    .controller('registroUbicacionListController', registroUbicacionListController);

  registroUbicacionListController.$inject = ['$scope', '$filter', '$state','$modal', 'ngDialog', 'usuarioHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION','$interval'];
    


  
    function registroUbicacionListController($scope, $filter, $state,$modal, ngDialog, usuarioHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION,$interval) {       
        
        
                
         
        var options = {
              enableHighAccuracy: true,
              timeout: 5000,
              maximumAge: 0
            };        
        var ubicacion={
            long:0,
            lat:0,
            accu:0,            
            load:false            
        };       
               
      
       
        
            
    $scope.registroUbicacion={
        //listado           
     paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
          
        },
        open : function($event) {
          $scope.registroUbicacion.fechaInicial.isOpen = true;
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.registroUbicacion.fechaFinal.isOpen = true;
        }
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreregistroUbicacion:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.registroUbicacion.selectedAll = !$scope.registroUbicacion.selectedAll; 
        for (var key in $scope.registroUbicacion.selectedItems) {
          $scope.registroUbicacion.selectedItems[key].check = $scope.registroUbicacion.selectedAll;
        }
      },
      add : function() {          
          
            if(!ubicacion.load){message.show("warning", "Debe cargar su ubicación");return;}
            if($scope.procesoAsociado.current.id==-1){message.show("warning", "Debe seleccionar un contrato");return;}     
            if($scope.responsableProceso.current.funcionarioId==-1){message.show("warning", "Debe seleccionar un responsable");return;}
              
         var  registroUbicacion = {
                id:0,
                contratoId:$scope.procesoAsociado.current.id,
                funcionarioId:$scope.responsableProceso.current.funcionarioId,
                fecha: '',
                tipo: '',
                comentario: '',
                longitude : 0,
                latitude : 0,
                accuracy : 0
		}
          
        var parameter= { objUbicacion : ubicacion,
                          registroUbicacion:registroUbicacion }
         
         var modalInstance = $modal.open({
          templateUrl: 'app/views/usuario/registroUbicacion_modal.html',
          controller: 'ModalGmapController',
          size: 'lg',
            resolve: {
                parameters: function () { return parameter; }
            }
        });
          
        modalInstance.result.then(function (parameters) {
            
            $scope.btnRegistroUbicacion=false; 
            $scope.registroUbicacion.getData();
            
              ubicacion.long=0;            
              ubicacion.lat=0,
              ubicacion.accu=0,            
              ubicacion.load=false;            
          
        });
                      
      },
      edit : function(item) {
          
          //message.show("warning", "Por el momento no se puede editar la ubicación");
          
        var modalInstance = $modal.open({
          templateUrl: 'app/views/usuario/registroUbicacion_modal.html',
          controller: 'ModalGmapController',
          size: 'lg',
          resolve: { parameters: { objUbicacion : ubicacion,registroUbicacion: item}         }
        });
        modalInstance.result.then(function (parameters) {
            $scope.registroUbicacion.getData();
              
              ubicacion.long=0;            
              ubicacion.lat=0,
              ubicacion.accu=0,            
              ubicacion.load=false;            
             $scope.registroUbicacion.obtenerUbicacion()            
        });         
      
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.registroUbicacion.filterText,
          "precision": false
        }];
        $scope.registroUbicacion.selectedItems = $filter('arrayFilter')($scope.registroUbicacion.dataSource, paramFilter);
        $scope.registroUbicacion.paginations.totalItems = $scope.registroUbicacion.selectedItems.length;
        $scope.registroUbicacion.paginations.currentPage = 1;
        $scope.registroUbicacion.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.registroUbicacion.paginations.currentPage == 1 ) ? 0 : ($scope.registroUbicacion.paginations.currentPage * $scope.registroUbicacion.paginations.itemsPerPage) - $scope.registroUbicacion.paginations.itemsPerPage;
        $scope.registroUbicacion.data = $scope.registroUbicacion.selectedItems.slice(firstItem , $scope.registroUbicacion.paginations.currentPage * $scope.registroUbicacion.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.registroUbicacion.data = [];
        $scope.registroUbicacion.loading = true;
        $scope.registroUbicacion.noData = false;
          var objF={};
          
            if(!$scope.responsableProceso.current){message.show("warning", "Debe seleccionar un responsable");return;}
            if($scope.registroUbicacion.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
				else{objF.fechaInicial=Date.parse(new Date($scope.registroUbicacion.fechaInicial.value));}	
          
            if($scope.registroUbicacion.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
				else{objF.fechaFinal=Date.parse(new Date($scope.registroUbicacion.fechaFinal.value));}	  
              
              objF.idResponsable=$scope.responsableProceso.current.funcionarioId;              
              objF.idReferencia=$scope.procesoAsociado.current.id;    
          
        usuarioHttp.getRegistrosUbicacion({}, objF,function(response) {
          $scope.registroUbicacion.selectedItems = response;
          $scope.registroUbicacion.dataSource = response;
          for(var i=0; i<$scope.registroUbicacion.dataSource.length; i++){
            $scope.registroUbicacion.nombreregistroUbicacion.push({id: i, nombre: $scope.registroUbicacion.dataSource[i]});
          }
          $scope.registroUbicacion.paginations.totalItems = $scope.registroUbicacion.selectedItems.length;
          $scope.registroUbicacion.paginations.currentPage = 1;
          $scope.registroUbicacion.changePage();
          $scope.registroUbicacion.loading = false;
          ($scope.registroUbicacion.dataSource.length < 1) ? $scope.registroUbicacion.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }, 
        obtenerUbicacion : function() {                
            
            if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition( $scope.registroUbicacion.setUbicacion);
                  $scope.btnRegistroUbicacion=true; 
                } else {
                   message.show("error", "No se puede cargar la ubicación!");  
                   $scope.btnRegistroUbicacion=false; 
                }                
        },
        setUbicacion : function(pos) {            
            
             var crd = pos.coords;  
              
              ubicacion.lat=crd.latitude;
              ubicacion.long=crd.longitude;
              ubicacion.accu= crd.accuracy;
              ubicacion.load= true;               
              message.show("success", "Ubicación cargada con exito!");        
              
        }        
    }
        
    //RESPONSABLE
    $scope.responsableProceso = {
      current : {}, data:[],
      getData : function() {        
          
          if($scope.procesoAsociado.current){              
              
                var obj= {
                    idProceso : $scope.procesoAsociado.current.id,
                    proceso : "CONTRATO"
                }                
                $rootScope.loadingVisible = true;
                usuarioHttp.getResponsablesProceso({}, obj, function(response) {                  
                    
                    if(response.length==1){      
                       $scope.responsableProceso.data = response ;   
                       $scope.responsableProceso.current = $scope.responsableProceso.data[0];
                    }
                    else{
                        
                        $scope.responsableProceso.data = response ;   
                        $scope.responsableProceso.data.push({funcionarioId:(-1), nombre:"Todos"});
                        $scope.responsableProceso.data=$filter('orderBy')($scope.responsableProceso.data, 'id'); 
                        $scope.responsableProceso.current = $scope.responsableProceso.data[0];
                        }
                    
                    $rootScope.loadingVisible = false;
                    
                    
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });
          }
      }
    } 
    
    $scope.procesoAsociado = {
      current : {}, data:[],
      getData : function() {
          
              
                $rootScope.loadingVisible = true;
                usuarioHttp.getProcesosAsociado({}, {proceso:"CONTRATO"}, function(response) {                       
                     
                    
                  if(response.length==1){     
                        $scope.procesoAsociado.data = response ;   
                        $scope.procesoAsociado.current = $scope.procesoAsociado.data[0];
                        $scope.responsableProceso.getData();
                      
                      
                    }
                    else{

                         $scope.procesoAsociado.data = response ;   
                         $scope.procesoAsociado.data.push({id:(-1), descripcion:"Todos",proceso:""});
                         $scope.procesoAsociado.data=$filter('orderBy')($scope.procesoAsociado.data, 'id');  
                         $scope.procesoAsociado.current=$scope.procesoAsociado.data[0];  
                         $scope.responsableProceso.getData();
                    }
                    
                    $rootScope.loadingVisible = false;
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });          
      },
        setProcesoAsociado: function() {             
           $scope.responsableProceso.getData();            
      }
     }
    
    //CARGAMOS LISTADO
    $scope.procesoAsociado.getData();
    $scope.registroUbicacion.obtenerUbicacion();
    // INICIALIZAMOS FECHAS
    var date = new Date();    
    $scope.registroUbicacion.fechaInicial.value=new Date(date.getFullYear(), date.getMonth(), 1);
    $scope.registroUbicacion.fechaFinal.value=new Date(date.getFullYear(), date.getMonth() + 1, 0);        
    
        
        
        
    }
  
  
  })();
/**=========================================================
 * Module: app.forma30.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.usuario')
    .service('usuarioHttp', usuarioHttp);

  usuarioHttp.$inject = ['$resource', 'END_POINT'];


  function usuarioHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      getRegistrosUbicacion : {
        method : 'POST',
        isArray : true
      },
      getRegistroUbicacion : {
        method : 'GET',
        url : END_POINT + '/Cliente.svc/registroUbicacion/:id'
      },      
      addRegistroUbicacion: {
        method:'POST',
        url : END_POINT + '/Cliente.svc/addRegistroUbicacion'
      },
      editRegistroUbicacion: {
        method:'PUT',
        url : END_POINT + '/Cliente.svc/editRegistroUbicacion'
      },getProcesosAsociado: {
          method : 'GET',
          isArray : true,
          params : {
          proceso : '@proceso'           
          },
          'url' : END_POINT + '/General.svc/procesoAsociado/:proceso'
      }, 
        'getResponsablesProceso': {
          'method' : 'POST',
          'isArray' : true,          
          'url' : END_POINT + '/General.svc/procesoAsociado/responsable'
        }
      
        
    };

    return $resource(END_POINT + '/Cliente.svc/registroUbicacion', paramDefault, actions);
  }

})();

/**=========================================================
 * Module: animate-enabled.js
 * Enable or disables ngAnimate for element with directive
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('animateEnabled', animateEnabled);

    animateEnabled.$inject = ['$animate'];
    function animateEnabled ($animate) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
          scope.$watch(function () {
            return scope.$eval(attrs.animateEnabled, scope);
          }, function (newValue) {
            $animate.enabled(!!newValue, element);
          });
        }
    }

})();

/**=========================================================
 * Module: browser.js
 * Browser detection
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .service('Browser', Browser);

    Browser.$inject = ['$window'];
    function Browser($window) {
      return $window.jQBrowser;
    }

})();

/**=========================================================
 * Module: clear-storage.js
 * Removes a key from the browser storage via element click
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('resetKey', resetKey);

    resetKey.$inject = ['$state', '$localStorage'];
    function resetKey ($state, $localStorage) {
        var directive = {
            link: link,
            restrict: 'A',
            scope: {
              resetKey: '@'
            }
        };
        return directive;

        function link(scope, element) {
          element.on('click', function (e) {
              e.preventDefault();

              if(scope.resetKey) {
                delete $localStorage[scope.resetKey];
                $state.go($state.current, {}, {reload: true});
              }
              else {
                $.error('No storage key specified for reset.');
              }
          });
        }
    }

})();

(function() {
    'use strict';
    angular.module('app.utils').
      directive('customOnChange', function() {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          var onChangeFunc = scope.$eval(attrs.customOnChange);
          element.bind('change', onChangeFunc);
        }
      };
    });
  })();
/**=========================================================
 * Module: flot.js
 * Initializes the Flot chart plugin and handles data refresh
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('flot', flot);

    flot.$inject = ['$http', '$timeout'];
    function flot ($http, $timeout) {

        var directive = {
          restrict: 'EA',
          template: '<div></div>',
          scope: {
            dataset: '=?',
            options: '=',
            series: '=',
            callback: '=',
            src: '='
          },
          link: link
        };
        return directive;

        function link(scope, element, attrs) {
          var height, plot, plotArea, width;
          var heightDefault = 220;
          //debugger;
          plot = null;

          width = attrs.width || '100%';
          height = attrs.height || heightDefault;

          plotArea = $(element.children()[0]);
          plotArea.css({
            width: width,
            height: height
          });

          function init() {
            var plotObj;
            if(!scope.dataset || !scope.options) return;
            plotObj = $.plot(plotArea, scope.dataset, scope.options);
            scope.$emit('plotReady', plotObj);
            if (scope.callback) {
              scope.callback(plotObj, scope);
            }

            return plotObj;
          }

          function onDatasetChanged(dataset) {
            if (plot) {
              plot.setData(dataset);
              plot.setupGrid();
              return plot.draw();
            } else {
              plot = init();
              onSerieToggled(scope.series);
              return plot;
            }
          }
          scope.$watchCollection('dataset', onDatasetChanged, true);

          function onSerieToggled (series) {
            if( !plot || !series ) return;
            var someData = plot.getData();
            for(var sName in series) {
              angular.forEach(series[sName], toggleFor(sName));
            }
            
            plot.setData(someData);
            plot.draw();
            
            function toggleFor(sName) {
              return function (s, i){
                if(someData[i] && someData[i][sName])
                  someData[i][sName].show = s;
              };
            }
          }
          scope.$watch('series', onSerieToggled, true);
          
          function onSrcChanged(src) {

            if( src ) {

              $http.get(src)
                .success(function (data) {

                  $timeout(function(){
                    scope.dataset = data;
                  });

              }).error(function(){
                $.error('Flot chart: Bad request.');
              });
              
            }
          }
          scope.$watch('src', onSrcChanged);

        }
    }


})();
/**=========================================================
 * Module: fullscreen.js
 * Toggle the fullscreen mode on/off
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('toggleFullscreen', toggleFullscreen);

    toggleFullscreen.$inject = ['Browser'];
    function toggleFullscreen (Browser) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element) {
          // Not supported under IE
          if( Browser.msie ) {
            element.addClass('hide');
          }
          else {
            element.on('click', function (e) {
                e.preventDefault();

                if (screenfull.enabled) {
                  
                  screenfull.toggle();
                  
                  // Switch icon indicator
                  if(screenfull.isFullscreen)
                    $(this).children('em').removeClass('fa-expand').addClass('fa-compress');
                  else
                    $(this).children('em').removeClass('fa-compress').addClass('fa-expand');

                } else {
                  $.error('Fullscreen not enabled');
                }

            });
          }
        }
    }


})();

/**=========================================================
 * Module: load-css.js
 * Request and load into the current page a css file
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('loadCss', loadCss);

    function loadCss () {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
          element.on('click', function (e) {
              if(element.is('a')) e.preventDefault();
              var uri = attrs.loadCss,
                  link;

              if(uri) {
                link = createLink(uri);
                if ( !link ) {
                  $.error('Error creating stylesheet link element.');
                }
              }
              else {
                $.error('No stylesheet location defined.');
              }

          });
        }
        
        function createLink(uri) {
          var linkId = 'autoloaded-stylesheet',
              oldLink = $('#'+linkId).attr('id', linkId + '-old');

          $('head').append($('<link/>').attr({
            'id':   linkId,
            'rel':  'stylesheet',
            'href': uri
          }));

          if( oldLink.length ) {
            oldLink.remove();
          }

          return $('#'+linkId);
        }
    }

})();

/**=========================================================
 * Module: now.js
 * Provides a simple way to display the current time formatted
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('now', now);

    now.$inject = ['dateFilter', '$interval'];
    function now (dateFilter, $interval) {
        var directive = {
            link: link,
            restrict: 'EA'
        };
        return directive;

        function link(scope, element, attrs) {
          var format = attrs.format;

          function updateTime() {
            var dt = dateFilter(new Date(), format);
            element.text(dt);
          }

          updateTime();
          var intervalPromise = $interval(updateTime, 1000);

          scope.$on('$destroy', function(){
            $interval.cancel(intervalPromise);
          });

        }
    }

})();

(function() {
  'use strict';

  angular
    .module('app.utils')
    .directive('onlyDigits', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var digits = val.replace(/[^0-9]/g, '');

            if (digits !== val) {
              ctrl.$setViewValue(digits);
              ctrl.$render();
            }
            return parseInt(digits,10);
          }
          return undefined;
        }            
        ctrl.$parsers.push(inputValue);
      }
    };
  });    


})();




/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
  angular
    .module('app.utils')
    .filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});
/**=========================================================
 * Module: table-checkall.js
 * Tables check all checkbox
 =========================================================*/
(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('checkAll', checkAll);

    function checkAll () {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element) {
          element.on('change', function() {
            var $this = $(this),
                index= $this.index() + 1,
                checkbox = $this.find('input[type="checkbox"]'),
                table = $this.parents('table');
            // Make sure to affect only the correct checkbox column
            table.find('tbody > tr > td:nth-child('+index+') input[type="checkbox"]')
              .prop('checked', checkbox[0].checked);

          });
        }
    }

})();

/**=========================================================
 * Module: trigger-resize.js
 * Triggers a window resize event from any element
 =========================================================*/
(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('triggerResize', triggerResize);

    triggerResize.$inject = ['$window', '$timeout'];
    function triggerResize ($window, $timeout) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element) {
          element.on('click', function(){
            $timeout(function(){
              $window.dispatchEvent(new Event('resize'));
            });
          });
        }
    }

})();

/**=========================================================
 * Module: utils.js
 * Utility library to use across the theme
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .service('Utils', Utils);

    Utils.$inject = ['$window', 'APP_MEDIAQUERY'];
    function Utils($window, APP_MEDIAQUERY) {

        var $html = angular.element('html'),
            $win  = angular.element($window),
            $body = angular.element('body');

        return {
          // DETECTION
          support: {
            transition: (function() {
                    var transitionEnd = (function() {

                        var element = document.body || document.documentElement,
                            transEndEventNames = {
                                WebkitTransition: 'webkitTransitionEnd',
                                MozTransition: 'transitionend',
                                OTransition: 'oTransitionEnd otransitionend',
                                transition: 'transitionend'
                            }, name;

                        for (name in transEndEventNames) {
                            if (element.style[name] !== undefined) return transEndEventNames[name];
                        }
                    }());

                    return transitionEnd && { end: transitionEnd };
                })(),
            animation: (function() {

                var animationEnd = (function() {

                    var element = document.body || document.documentElement,
                        animEndEventNames = {
                            WebkitAnimation: 'webkitAnimationEnd',
                            MozAnimation: 'animationend',
                            OAnimation: 'oAnimationEnd oanimationend',
                            animation: 'animationend'
                        }, name;

                    for (name in animEndEventNames) {
                        if (element.style[name] !== undefined) return animEndEventNames[name];
                    }
                }());

                return animationEnd && { end: animationEnd };
            })(),
            requestAnimationFrame: window.requestAnimationFrame ||
                                   window.webkitRequestAnimationFrame ||
                                   window.mozRequestAnimationFrame ||
                                   window.msRequestAnimationFrame ||
                                   window.oRequestAnimationFrame ||
                                   function(callback){ window.setTimeout(callback, 1000/60); },
            /*jshint -W069*/
            touch: (
                ('ontouchstart' in window && navigator.userAgent.toLowerCase().match(/mobile|tablet/)) ||
                (window.DocumentTouch && document instanceof window.DocumentTouch)  ||
                (window.navigator['msPointerEnabled'] && window.navigator['msMaxTouchPoints'] > 0) || //IE 10
                (window.navigator['pointerEnabled'] && window.navigator['maxTouchPoints'] > 0) || //IE >=11
                false
            ),
            mutationobserver: (window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver || null)
          },
          // UTILITIES
          isInView: function(element, options) {
              /*jshint -W106*/
              var $element = $(element);

              if (!$element.is(':visible')) {
                  return false;
              }

              var window_left = $win.scrollLeft(),
                  window_top  = $win.scrollTop(),
                  offset      = $element.offset(),
                  left        = offset.left,
                  top         = offset.top;

              options = $.extend({topoffset:0, leftoffset:0}, options);

              if (top + $element.height() >= window_top && top - options.topoffset <= window_top + $win.height() &&
                  left + $element.width() >= window_left && left - options.leftoffset <= window_left + $win.width()) {
                return true;
              } else {
                return false;
              }
          },
          
          langdirection: $html.attr('dir') === 'rtl' ? 'right' : 'left',

          isTouch: function () {
            return $html.hasClass('touch');
          },

          isSidebarCollapsed: function () {
            return $body.hasClass('aside-collapsed');
          },

          isSidebarToggled: function () {
            return $body.hasClass('aside-toggled');
          },

          isMobile: function () {
            return $win.width() < APP_MEDIAQUERY.tablet;
          }

        };
    }
})();

/**=========================================================
 * Module: flot.js
 * Initializes the Flot chart plugin and handles data refresh
 =========================================================*/
(function() {
  'use strict';

  angular
    .module('app.utils')
    .directive('validation', validation);


  validation.$inject = ['$parse'];


  function validation ($parse) {

   return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, element, attr, ctrl) {


        angular.element(element).on("keypress", function(e) {
          var pattern = new RegExp(scope.$eval(attr['validation']));
          var newInput = this.value + String.fromCharCode(e.charCode);
          if (pattern.test(newInput) == false) {
            e.preventDefault();
          };
        });


        angular.element(element).on("paste", function(e) {
          setTimeout(function () {
            var pattern = new RegExp(scope.$eval(attr['validation']));
            var newText = angular.element(element).val();
            if (pattern.test(newText) == false) {
              e.preventDefault();
                var model = $parse(attr.ngModel);
                console.log(model(scope));
                model.assign(scope,'');
                scope.$apply();
            };
          }, 5);
        });

      }
    };
  }
})();