/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .controller('agendaFormController', agendaFormController);

  agendaFormController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'agendaHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function agendaFormController($scope, $filter, $state, LDataSource, agendaHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      

     
      var agenda = $state.params.agenda;
      var referenciaId = agenda.referenciaId;
      var tipoReferencia = agenda.tipoReferencia;
 
      
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.agenda = {
		model : {
            id : 0,
            tipoId : 0,
            referenciaId : 0,
            tipoReferencia : '',
            asunto : '',
            descripcion : '',
            fechaInicial: '',
            fechaFinal: '',
            estado: '',          
            fechaAct: '',
            usuarioAct: '',
            fechaReg: '',
            usuarioReg: '',
            responsables : [],
            consecutivos : []	
		}, 
      fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
          
        },
        open : function($event) {
          $scope.agenda.fechaInicial.isOpen = true;
        }
      },
        horaInicial : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        horaFinal : {
        ismeridian : true,
        mstep:10,
        hstep:1,            
        value:'',
        
        changed : function($event) {
          
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.agenda.fechaFinal.isOpen = true;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.agenda', params : { filters : {objFiltro:agenda.objFiltro}, data : []} });
        $state.go('app.agenda');
          
      },
      save : function() {		  
       
          if($scope.agenda.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
            else{$scope.agenda.model.fechaInicial=Date.parse(new Date($scope.agenda.fechaInicial.value));}	

            if($scope.agenda.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
            else{$scope.agenda.model.fechaFinal=Date.parse(new Date($scope.agenda.fechaFinal.value));}	
            
            if($scope.agenda.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
            if($scope.agenda.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
            
            if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
            else{$scope.agenda.model.estado= $scope.estados.current.value;}

            if(!$scope.tipoAgenda.current){message.show("warning", "Tipo agenda requerido");return;}
            else{$scope.agenda.model.tipoId= $scope.tipoAgenda.current.id;}	
            
            if($scope.agenda.model.responsables.length==0){message.show("warning", "Debe especificar un responsable");return;}
            
            
             $scope.agenda.model.referenciaId = referenciaId;
             $scope.agenda.model.tipoReferencia = tipoReferencia;
            
            
            var _http = new agendaHttp($scope.agenda.model);
          
             if(  $scope.agenda.model.id==0){
                 
                  _http.$addAgenda(function(response){                  
                      message.show("success", "Agenda creada satisfactoriamente!!"); 
                      $scope.modoCreacion=true;
                      $scope.agenda.model=response;
                      
                      
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editAgenda(function(response){                  
                      message.show("success", "Agenda actualizada satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }
             
        
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
          seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.agenda.model.id,
                         referencia : 'AGENDA' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        cancel : function(){
            
             var   model = {        
                id : 0,              
                asunto : '',
                tipoId : 0,
                descripcion : '',
                fechaInicial: '',
                referenciaId : referenciaId,
                tipoReferencia : tipoReferencia,
                fechaFinal: '',
                estado: '',          
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: '',
                responsables : [],
                consecutivos : []
            };
            
           $scope.agenda.fechaInicial.value='';
           $scope.agenda.horaInicial.value='';
            
            $scope.agenda.model=model;
            //$scope.modoEdicion= false;
           
            
            $scope.estados.current=$scope.estados.data[0]; 
            $scope.tipoAgenda.current=$scope.tipoAgenda.data[0]; 
            
        },
        removeResponsable: function(item){          
                    
            
            for (var i = 0; i < $scope.agenda.model.responsables.length; i++) { 
                
                    if($scope.agenda.model.responsables[i].funcionarioId ==item.funcionarioId){
                        
                        $scope.agenda.model.responsables.splice(i, 1);
                        break;
                    }
                
                }
        }  ,        
        addResponsable: function(){  
          
         if(!$scope.funcionarios.current){message.show("warning", "Resposable requerido");return;}
            
            var guarda=0;
            for (var i = 0; i < $scope.agenda.model.responsables.length; i++) { 
                
                    if($scope.agenda.model.responsables[i].funcionarioId ==$scope.funcionarios.current.id){
                        
                      guarda++;
                    }
                
                }
            
            if(guarda==0){
                
                var nuevoResponsable={
                    agendaId:$scope.agenda.model.id,
                    funcionarioId: $scope.funcionarios.current.funcionarioId,
                    nombre: $scope.funcionarios.current.nombre                    
                }
                
                $scope.agenda.model.responsables.push(nuevoResponsable);
                
                
            }else{                
                message.show("warning", "El responsable ya se encuentra asociado en la agenda");
            }
        },          
        removeConsecutivo: function(item){          
                    
            
            for (var i = 0; i < $scope.agenda.model.consecutivos.length; i++) { 
                
                    if($scope.agenda.model.consecutivos[i].consecutivoId ==item.consecutivoId){
                        
                        $scope.agenda.model.consecutivos.splice(i, 1);
                        break;
                    }
                
                }
        }  ,        
        addConsecutivo: function(){  
          
         if(!$scope.consecutivos.current){message.show("warning", "Consecutivo requerido");return;}
           if(!$scope.agenda.model.consecutivos){$scope.agenda.model.consecutivos=[]} 
            var guarda=0;
            for (var i = 0; i < $scope.agenda.model.consecutivos.length; i++) { 
                
                    if($scope.agenda.model.consecutivos[i].consecutivoId ==$scope.consecutivos.current.consecutivoId){
                        
                      guarda++;
                    }
                
                }
            
            if(guarda==0){
                
                var nuevoConsecutivo={
                    agendaId:$scope.agenda.model.id,
                    consecutivoId: $scope.consecutivos.current.consecutivoId,
                    consecutivo: $scope.consecutivos.current.consecutivo                    
                }
                
                $scope.agenda.model.consecutivos.push(nuevoConsecutivo);
                
                
            }else{                
                message.show("warning", "El consecutivo ya se encuentra asociado en la agenda");
            }
        }
          
        
      }
   
         //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'A', descripcion: 'Abierta'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrada'});
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelada'}); 
            $scope.estados.data.push({value:'AN', descripcion: 'Anulada'});  
            
            if(!$scope.agenda.model){$scope.estados.current = $scope.estados.data[0];}
            else{$scope.estados.current = $filter('filter')($scope.estados.data, { id : $scope.agenda.model.estado })[0];}
                        
        }        
    }
    
        
    //TIPO AGENDA
    $scope.tipoAgenda = {
      current : {},
      data:[],
      getData : function() {
            $scope.tipoAgenda.data=agenda.tipoAgenda;          
         
                for (var i = 0; i < $scope.tipoAgenda.data.length; i++) { 

                    if($scope.tipoAgenda.data[i].codigo ==-1){
                        $scope.tipoAgenda.data.splice(i, 1);
                        break;
                    }

                }
      },
      setTipo : function(){
          $scope.agenda.model.agenda=$scope.tipoAgenda.current.id;
    }}
    
    
     //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $scope.funcionarios.data = agenda.participantes;
          
           for (var i = 0; i < $scope.funcionarios.data.length; i++) { 

                    if($scope.funcionarios.data[i].id ==-1){
                        $scope.funcionarios.data.splice(i, 1);
                        break;
                    }

                }
      }
    } 
    
       //FUNCIONARIOS
    $scope.consecutivos = {
      current : {}, data:[],
      getData : function() {
                      
           $rootScope.loadingVisible = true;
            agendaHttp.getConsecutivos({}, {}, function(response) {                    
            $scope.consecutivos.data = response ;                  
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }   
    
    
    $scope.tipoAgenda.getData();
    $scope.funcionarios.getData();
    $scope.estados.getData();
      $scope.consecutivos.getData();
    
	//CARGAMOS LOS DATOS DEL agenda	
	
	if(agenda.id==0){
    
        $scope.estados.current=$scope.estados.data[0]; 
        $scope.tipoAgenda.current = $scope.tipoAgenda.data[0];
        $scope.funcionarios.current= $scope.funcionarios.data[0];        
        $scope.modoCreacion=false;
        
        
      //$scope.objetoagenda.current = $scope.objetoagenda.data[0];
      //$scope.tipoProceso.current= $filter('filter')($scope.tipoProceso.data, {value : agenda.tipo})[0]; 
      //$scope.agenda.model.tipo=agenda.tipo;    
        
        
    }
    else{   
        $scope.modoCreacion=true;
        $rootScope.loadingVisible = true;        
        //$scope.divParticipanteagenda=true;
		agendaHttp.getAgenda({},$state.params.agenda, function (data) { 
            $scope.agenda.model = data;	
            $scope.funcionarios.current= $scope.funcionarios.data[0];

            if($scope.agenda.model.fechaInicial){

                    $scope.agenda.fechaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
                    $scope.agenda.horaInicial.value=new Date(parseFloat($scope.agenda.model.fechaInicial));
                }    
                if($scope.agenda.model.fechaFinal){

                    $scope.agenda.fechaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
                    $scope.agenda.horaFinal.value=new Date(parseFloat($scope.agenda.model.fechaFinal));
                }  

            $scope.estados.current = $filter('filter')($scope.estados.data, { value : $scope.agenda.model.estado })[0];
            $scope.tipoAgenda.current = $filter('filter')($scope.tipoAgenda.data, { id : $scope.agenda.model.tipoId })[0];   
            
                var obj= {
                    idProceso : data.referenciaId,
                    proceso : data.tipoReferencia
                }  
              
                $rootScope.loadingVisible = true;
                agendaHttp.getResponsablesProceso({}, obj, function(response) {                      
                    
                    
                    if(response){                        
                        
                        agenda.participantes = response;                          
                        $scope.funcionarios.getData();              
                    
                    }
                    else{
                            message.show("warning", "El proceso asociado no tiene responsables posibles");  
                    }
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });  
            
       
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      
    
  }
})();