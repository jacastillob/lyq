/**=========================================================
 * Module: app.contrato.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.calendar')
    .service('calendarHttp', calendarHttp);

  calendarHttp.$inject = ['$resource', 'END_POINT'];


  function calendarHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {  
      'getAgendas' : {
        'method' : 'GET',
        'isArray' : true
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/agenda/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/agenda'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Cliente.svc/agenda'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/agenda/:id'
      },
        'getContratoBusqueda' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Cliente.svc/contratoBusqueda'
      }
        
     
    };
    return $resource( END_POINT + '/Cliente.svc/agenda', {}, actions, {}); 
  }

})();