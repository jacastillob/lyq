/**=========================================================
 * Module: app.compromiso.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.compromiso')
    .service('compromisoRolHttp', compromisoRolHttp);

  compromisoRolHttp.$inject = ['$resource', 'END_POINT'];


  function compromisoRolHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getListCompromiso' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : {
            tipo : '@tipo'           
          },
        'url' : END_POINT + '/General.svc/compromisos/:tipo'
      },
      'getCompromisoRol' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/compromiso/:id/rol'
      },
      'getRol' : {
        'method' : 'GET',
        'isArray' : true,
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/rol'
      },
      'updateCompromiso' : {///
        'method' : 'PUT',
        'url' : END_POINT + '/General.svc/compromiso'
      },
      'removeCompromiso':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/General.svc/compromiso/:id'
      },
      'newCompromiso':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/compromiso/'
      },
      'deleteCompromiso': {
          'method':'DELETE',
          'isArray' : true,
          'params' : {
            compromisoId : '@compromisoId'
          },
          'url' : END_POINT + '/General.svc/compromiso/:compromisoId'
      },
      'deleteRol': {
          'method':'DELETE',
          'params' : {
            rolId : '@rolId'
          },
          'url' : END_POINT + '/General.svc/rol/:rolId'
      },
      'assignRol':   {
        'method':'POST',
        'url' : END_POINT + '/General.svc/compromiso/rol'
      },
      'unassignRol':   {
        'method':'PUT',
        'url' : END_POINT + '/General.svc/compromiso/rol'
      }
    };
    return $resource( END_POINT + '/General.svc/compromiso', {}, actions, {}); 
  }

})();