(function() {
  'use strict';
  angular
    .module('app.core', [
    'ngRoute',
    'ngAnimate',
    'ngStorage',
    'ngCookies',
    'pascalprecht.translate',
    'ui.bootstrap',
    'ui.router',
    'oc.lazyLoad',
    'cfp.loadingBar',
    'ngSanitize',
    'ngResource',
    'tmh.dynamicLocale',
    'ui.utils',
    'app.fileManager',
    'app.routes',  
    'app.security',
    'app.mailbox',  
    'ngImgCrop',    
    'app.setMenu',
	'app.funcionario',
    'app.tercero',
    'app.oportunidad',
    'app.contrato',
    'app.fileManager',
    'app.seguimiento',
    'app.producto',
    'app.calendar',
    'app.agenda',
    'app.consecutivo',
    'app.rol',
    'app.setMenu',   
    'app.factura',
    'app.mensajeria',
    'app.usuario',
    'app.reporte',
    'app.compromiso'
  ]);

})();