(function() {
  'use strict';

  angular
    .module('app.core')
    .run(appRun);

  appRun.$inject = ['$rootScope', '$state', '$stateParams',  '$window', '$templateCache', 'Colors', 'parametersOfState', 'TO_STATE'];

  function appRun($rootScope, $state, $stateParams, $window, $templateCache, Colors, parametersOfState, TO_STATE) {


    // Set reference to access them from any scope
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$storage = $window.localStorage;

    // Uncomment this to disable template cache
    /*$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
          if (typeof(toState) !== 'undefined'){
            $templateCache.remove(toState.templateUrl);
          }
      });*/

    // Allows to use branding color with interpolation
    // {{ colorByName('primary') }}
    $rootScope.colorByName = Colors.byName;

    // cancel click event easily
    $rootScope.cancel = function($event) {                                                                                             $event.stopPropagation();
                                         };

    // Hooks Example
    // ----------------------------------- 

    // Hook not found
    $rootScope.$on('$stateNotFound',
                   function(event, unfoundState/*, fromState, fromParams*/) {
      console.log(unfoundState.to); // "lazy.state"
      console.log(unfoundState.toParams); // {a:1, b:2}
      console.log(unfoundState.options); // {inherit:false} + default options
    });
    // Hook error
    $rootScope.$on('$stateChangeError',
                   function(event, toState, toParams, fromState, fromParams, error){
    });
    // Hook success


    /*
    $rootScope.$on('$stateChangeSuccess',
                   function(event, toState, toParams, fromState, fromParams) {
      debugger;
    });
*/
    TO_STATE = '';
    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
      if (parametersOfState.get(toState) != undefined) {
    //    debugger;
        if (TO_STATE != toState.name) {
          TO_STATE = toState.name;
          var params = parametersOfState.get(toState);
          $state.go(toState.name, params );
          event.preventDefault();  
        } else {
          TO_STATE = "";
        }
      }
    });

  }



})();

