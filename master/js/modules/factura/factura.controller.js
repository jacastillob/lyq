/**=========================================================
 * Module: app.factura.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.factura')
    .controller('facturaController', facturaController);

  facturaController.$inject = ['$scope', '$filter', '$state', 'LDataSource','base64', 'facturaHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION','SERVER_URL','$window'];


  function facturaController($scope, $filter, $state, LDataSource,base64, facturaHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION,SERVER_URL,$window) {
      
     
      var factura = $state.params.factura;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.factura = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,
              viaContactoId: 0,
              tipo: '',
              consecutivo: '',
			  asunto: '',
			  estado: '',
			  descripcion: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: '',
              fechaEmision: '',
              fechaVencimiento: ''
              
		},  
        fechaEmision : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.factura.fechaEmision.isOpen = true;
        }
      },  
        fechaVencimiento : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.factura.fechaVencimiento.isOpen = true;
        }
      },
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.factura.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.factura.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.factura', params : { filters : {procesoSeleccionado:factura.tipo}, data : []} });
        $state.go('app.factura');
          
      },
      save : function() {		  
       
		        if($scope.factura.fechaEmision.value==""){message.show("warning", "Fecha de emisión requerida");return;}
				else{$scope.factura.model.fechaEmision=Date.parse(new Date($scope.factura.fechaEmision.value));}	
          
                if($scope.factura.fechaVencimiento.value==""){message.show("warning", "Fecha de vencimiento requerida");return;}
				else{$scope.factura.model.fechaVencimiento=Date.parse(new Date($scope.factura.fechaVencimiento.value));}	
                
                if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.factura.model.terceroId = $scope.tercerosOp.current.id;}
          
          		if($scope.factura.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.factura.model.estado= $scope.estados.current.value;}
			
          
			//INSERTAR
            if($scope.factura.model.id==0){
				
				$rootScope.loadingVisible = true;
				facturaHttp.save({}, $scope.factura.model, function (data) { 
					
						facturaHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.factura.model=data;
							factura.id=$scope.factura.model.id; 
							if($scope.factura.model.fechaReg){$scope.factura.fechaReg.value=new Date(parseFloat($scope.factura.model.fechaReg));}    
							if($scope.factura.model.fechaAct){$scope.factura.fechaAct.value=new Date(parseFloat($scope.factura.model.fechaAct));} 		
                            
                                $scope.btnAdjunto=true;
                                $scope.divProductosfactura=true;
								
							message.show("success", "Información registrada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.factura.model.id){
					$state.go('app.factura');
				}else{
					
					facturaHttp.update({}, $scope.factura.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.factura.model=data;
						if($scope.factura.model.fechaReg){$scope.factura.fechaReg.value=new Date(parseFloat($scope.factura.model.fechaReg));}    
						if($scope.factura.model.fechaAct){$scope.factura.fechaAct.value=new Date(parseFloat($scope.factura.model.fechaAct));}  
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                     $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : data.contactoId })[0]; 
                    message.show("success", "Información actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.id,
                         referencia : 'factura' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/factura/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.factura.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.factura.model.id,
                         referencia : 'factura' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      } ,imprimir : function() {            
        	facturaHttp.getFormato({},{ id : $scope.factura.model.id,formato:'FACTURA'}, function (data) {
							
                            var urlDownload = SERVER_URL + base64.decode(data.url);
                            $window.open(urlDownload);                           
											
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
          
          
          
      },
          verCliente : function() {            
            var modalInstance = $modal.open({
              templateUrl: 'app/views/tercero/consultaTercero_form.html',
              controller: 'consultaTerceroController',
              size: 'lg',
              resolve: {
                parameters: { id : $scope.factura.model.terceroId,
                              referencia : 'CLIENTE'}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          }, 
   
          
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'PA', descripcion: 'Pagada'});             
        }        
    }

    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            facturaHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.factura.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.factura.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.factura.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.factura.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }
    //CONTACTOS
    $scope.contactosOp = {
      current : {}, data:[],
      getData : function() {                  
          
               $rootScope.loadingVisible = true;
                facturaHttp.getContactosEmpresa({}, { terceroId: $scope.factura.model.terceroId }, function(response) {   
                    
             $scope.contactosOp.data = response ;
                                                                                                                     
              if($scope.factura.model ){ 
                    
                    $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.factura.model.contactoId })[0];
               }
                else{
                    $scope.contactosOp.current=$scope.contactosOp.data[0];                     
                }
                                                                                                                   
                                                                                                                       
                    
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
            });
          
      },
      'setContacto' : function() {
        
          $scope.factura.model.contactoId=$scope.contactosOp.current.id;
      }
    }
    
    
      //PRODUCTOS
    $scope.productosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            facturaHttp.getProductos({}, {}, function(response) {   
                
                $scope.productosOp.data = response ;                 
                $scope.productosOp.current=$scope.productosOp.data[0];
                $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }
    
    
    $scope.productofactura = {
    model : {  
            productoId: 0,
            oportunidadId :0,
            total :0,
            totalPendiente :0,
            valor :0,
            cantidad :0,
            valorIva :0,
            producto: '',
            comentario: ''
			
		},
      current : {},
      data:[],
      getData : function() {
          
          
          if(factura){
              
            if($scope.factura.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              facturaHttp.getProductosfactura({}, { facturaId: $scope.factura.model.id }, function(response) {
                  
                  $scope.productofactura.data = response;
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },        
        add: function(){           
            
              
            if($scope.factura.model.id!=0){
                if($scope.productosOp.current){
                    $rootScope.loadingVisible = true;                    
                    
                    var data ={
                         productoId: $scope.productosOp.current.id,
                         oportunidadId :$scope.factura.model.id,
                         total :  $scope.productosOp.current.valor,
                         totalPendiente :  $scope.productosOp.current.valor,
                         valor :$scope.productosOp.current.valor,
                         cantidad :1,
                         valorIva :0,
                         producto: $scope.productosOp.current.nombre,
                         comentario: ''
                        
                    } ;
                      facturaHttp.addProductofactura({}, data, function(response) {

                          $scope.productofactura.getData();
                          $rootScope.loadingVisible = false;
                          message.show("success", "Producto agregado satisfactoriamente");	
                      }, function(faild) {
                          $rootScope.loadingVisible = false;
                          message.show("error", faild.Message);
                      });    
                }
                else{message.show("warning", "Debe seleccionar un producto");}

            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
          
        }
        ,        
        update: function(data){
            
            
            if($scope.factura.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              facturaHttp.editProductofactura({}, data, function(response) {
                  
                  $scope.productofactura.getData();
                  $rootScope.loadingVisible = false;
                  message.show("success", "Producto actualizado satisfactoriamente");	
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
        }
        ,delete: function(data){            
            
            $rootScope.loadingVisible = true;    
            debugger;
            facturaHttp.removeProductofactura({}, {oportunidadId: $scope.factura.model.id,productoId: data.productoId }, function(response){
             $scope.productofactura.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
        }
    }
	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();	    
    $scope.tercerosOp.getData();   
    $scope.productosOp.getData();
	
	//CARGAMOS LOS DATOS DEL factura	
	
	if(factura.id==0){
        
      $scope.estados.current=$scope.estados.data[0];             
      $scope.factura.model.tipo=factura.tipo;             
        
    }
    else{   
        $rootScope.loadingVisible = true;
		
            facturaHttp.read({},$state.params.factura, function (data) { 
                
            $scope.factura.model = data;	
            $scope.btnAdjunto=true;
            $scope.divProductosfactura=true;
               
			if($scope.factura.model.fechaReg){$scope.factura.fechaReg.value=new Date(parseFloat($scope.factura.model.fechaReg));}    
			if($scope.factura.model.fechaAct){$scope.factura.fechaAct.value=new Date(parseFloat($scope.factura.model.fechaAct));}  
            
                
            if($scope.factura.model.fechaEmision){$scope.factura.fechaEmision.value=new Date(parseFloat($scope.factura.model.fechaEmision));}    
			if($scope.factura.model.fechaVencimiento){$scope.factura.fechaVencimiento.value=new Date(parseFloat($scope.factura.model.fechaVencimiento));}     
                
                
                
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.factura.model.terceroId })[0];  
            $scope.contactosOp.getData();
            $scope.productofactura.getData();                
            $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.factura.model.contactoId })[0];    
                
			//$scope.estados.current=$scope.factura.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.factura.model.estado})[0];
               
                    
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      

      
        
	
    
    
  }
})();