/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.factura')
    .service('facturaHttp', facturaHttp);

  facturaHttp.$inject = ['$resource', 'END_POINT'];


  function facturaHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            proceso : '@proceso',
            estado : '@estado'
          },
          'url' : END_POINT + '/Comercial.svc/oportunidad/proceso/:proceso/estado/:estado'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:id/proceso'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/oportunidad/proceso'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Comercial.svc/oportunidad/proceso'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:id/proceso'
      },
        'getViaContacto' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/catalogo/VIACONTACTO'
      },        
      'getContactosEmpresa' : {
       'params' : {
            terceroId : '@terceroId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroOportunidad'
      },
       'getProductos' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/productoBusqueda'
      },
       'getProductosfactura' : {
       'params' : {
            facturaId : '@facturaId'           
          },
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/oportunidad/:facturaId/producto'
      },
        'addProductofactura': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/oportunidad/producto'
      },
        'editProductofactura': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/oportunidad/producto'
      },
      'removeProductofactura':  {
        'params' : {
            oportunidadId : '@oportunidadId',
            productoId : '@productoId'   
          },
        'method':'DELETE',        
        'url' : END_POINT + '/Comercial.svc/oportunidad/:oportunidadId/producto/:productoId'
      },'getFormato':  {
        'params' : {
            id : '@id',
            formato : '@formato'   
          },
        'method':'GET',        
        'url' : END_POINT + '/Comercial.svc/formato/:formato/oportunidad/:id'
      }
     
    };
    return $resource( END_POINT + '/Comercial.svc/oportunidad/proceso', {}, actions, {}); 
  }

})();