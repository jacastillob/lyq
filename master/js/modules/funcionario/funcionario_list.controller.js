/**=========================================================
 * Module: app.funcionario.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.funcionario')
    .controller('funcionarioListController', funcionarioListController);

  funcionarioListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'funcionarioHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function funcionarioListController($scope, $filter, $state, ngDialog, tpl, funcionarioHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
		
	$scope.ESTADO_INACTIVO = 'I';
    $scope.ESTADO_ACTIVO = 'A';
    $scope.ESTADO_ELIMINADO = 'D';
	
	
	//$rootScope.loadingVisible = true;

    $scope.funcionarios = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrefuncionarios:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.funcionarios.selectedAll = !$scope.funcionarios.selectedAll; 
        for (var key in $scope.funcionarios.selectedItems) {
          $scope.funcionarios.selectedItems[key].check = $scope.funcionarios.selectedAll;
        }
      },
      add : function() {
          var func = {
              id: 0,
              nombre: '',
              empresaId: 0,
			  matricula: '',
			  email: '',
			  documento: '',
			  direccion: '',
			  estado: '',
			  fechaIngreso: '',
			  codigo: '',
			  telefono: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: '',
			  usuario: '',
			  contrasenia: '',
			  tipoAuth: ''
          };
        parametersOfState.set({ name : 'app.funcionario_add', params : { funcionario: func } });
        $state.go('app.funcionario_add');
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.funcionario_edit', params : { funcionario: item } });
        $state.go('app.funcionario_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            funcionarioHttp.remove({}, { id: id }, function(response) {
                $scope.funcionarios.getData();
                message.show("success", "Funcionario eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.funcionarios.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    funcionarioHttp.remove({}, { id: id }, function(response) {
                        $scope.funcionarios.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.funcionarios.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.funcionarios.filterText,
          "precision": false
        }];
        $scope.funcionarios.selectedItems = $filter('arrayFilter')($scope.funcionarios.dataSource, paramFilter);
        $scope.funcionarios.paginations.totalItems = $scope.funcionarios.selectedItems.length;
        $scope.funcionarios.paginations.currentPage = 1;
        $scope.funcionarios.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.funcionarios.paginations.currentPage == 1 ) ? 0 : ($scope.funcionarios.paginations.currentPage * $scope.funcionarios.paginations.itemsPerPage) - $scope.funcionarios.paginations.itemsPerPage;
        $scope.funcionarios.data = $scope.funcionarios.selectedItems.slice(firstItem , $scope.funcionarios.paginations.currentPage * $scope.funcionarios.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.funcionarios.data = [];
        $scope.funcionarios.loading = true;
        $scope.funcionarios.noData = false;
        funcionarioHttp.getList(function(response) {
          $scope.funcionarios.selectedItems = response;
          $scope.funcionarios.dataSource = response;
          for(var i=0; i<$scope.funcionarios.dataSource.length; i++){
            $scope.funcionarios.nombrefuncionarios.push({id: i, nombre: $scope.funcionarios.dataSource[i]});
          }
          $scope.funcionarios.paginations.totalItems = $scope.funcionarios.selectedItems.length;
          $scope.funcionarios.paginations.currentPage = 1;
          $scope.funcionarios.changePage();
          $scope.funcionarios.loading = false;
          ($scope.funcionarios.dataSource.length < 1) ? $scope.funcionarios.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
	//CARGAMOS LOS LISTADOS
    $scope.funcionarios.getData();
	}
  
  
  })();