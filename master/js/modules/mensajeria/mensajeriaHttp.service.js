/**=========================================================
 * Module: app.oportunidad.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.mensajeria')
    .service('mensajeriaHttp', facturaHttp);

  facturaHttp.$inject = ['$resource', 'END_POINT'];


  function facturaHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,       
          'url' : END_POINT + '/Cliente.svc/mensajeria/:id'          
      },
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/mensajeria/:id'
      },
      'save':   {
        'method':'POST',
        'url' : END_POINT + '/Cliente.svc/mensajeria'
      },
      'update' : {
        'method' : 'PUT',
        'url' : END_POINT + '/Cliente.svc/mensajeria'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Cliente.svc/mensajeria/:id'
      }, 
       'getTerceros' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Comercial.svc/terceroBusqueda'
      },
        'addTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/terceroOportunidad'
      }
     
    };
    return $resource( END_POINT + '/Cliente.svc/mensajeria', {}, actions, {}); 
  }

})();