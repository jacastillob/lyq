/**=========================================================
 * Module: app.oportunidad.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .controller('oportunidadController', oportunidadController);

  oportunidadController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'oportunidadHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function oportunidadController($scope, $filter, $state, LDataSource, oportunidadHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var oportunidad = $state.params.oportunidad;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.oportunidad = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,
              viaContactoId: 0,
              padreId: 0,
              tipo: '',
              consecutivo: '',
			  asunto: '',
			  estado: '',
			  descripcion: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: '',
              fechaEmision: '',
              fechaVencimiento: ''
            
		},   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.oportunidad.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.oportunidad.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.oportunidad', params : { filters : {procesoSeleccionado:oportunidad.tipo}, data : []} });
        $state.go('app.oportunidad');
          
      },
      save : function() {		  
       
		  
                
                if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.oportunidad.model.terceroId = $scope.tercerosOp.current.id;}
          
          		if($scope.oportunidad.model.asunto ==""){message.show("warning", "Asunto requerido");return;}
				if($scope.oportunidad.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.oportunidad.model.estado= $scope.estados.current.value;}
				
				if(!$scope.tipoProceso.current){message.show("warning", "Tipo proceso requerido");return;}
				else{$scope.oportunidad.model.tipo= $scope.tipoProceso.current.value;}	
          
                if(!$scope.viaContacto.current){message.show("warning", "Vía de contacto requerido");return;}
				else{$scope.oportunidad.model.viaContactoId= $scope.viaContacto.current.codigo;}	
          
			//INSERTAR
            if($scope.oportunidad.model.id==0){
				
				$rootScope.loadingVisible = true;
				oportunidadHttp.save({}, $scope.oportunidad.model, function (data) { 
					
						oportunidadHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.oportunidad.model=data;
							oportunidad.id=$scope.oportunidad.model.id; 
							if($scope.oportunidad.model.fechaReg){$scope.oportunidad.fechaReg.value=new Date(parseFloat($scope.oportunidad.model.fechaReg));}    
							if($scope.oportunidad.model.fechaAct){$scope.oportunidad.fechaAct.value=new Date(parseFloat($scope.oportunidad.model.fechaAct));} 		
								
							message.show("success", "Información registrada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.oportunidad.model.id){
					$state.go('app.oportunidad');
				}else{
					
					oportunidadHttp.update({}, $scope.oportunidad.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.oportunidad.model=data;
						if($scope.oportunidad.model.fechaReg){$scope.oportunidad.fechaReg.value=new Date(parseFloat($scope.oportunidad.model.fechaReg));}    
						if($scope.oportunidad.model.fechaAct){$scope.oportunidad.fechaAct.value=new Date(parseFloat($scope.oportunidad.model.fechaAct));}  
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                     $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : data.contactoId })[0]; 
                    message.show("success", "Información actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.oportunidad.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.oportunidad.model.id,
                         referencia : 'OPORTUNIDAD' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/oportunidad/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.oportunidad.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.oportunidad.model.id,
                         referencia : 'OPORTUNIDAD' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },verCliente : function() {            
            var modalInstance = $modal.open({
              templateUrl: 'app/views/tercero/consultaTercero_form.html',
              controller: 'consultaTerceroController',
              size: 'lg',
              resolve: {
                parameters: { id : $scope.oportunidad.model.terceroId,
                              referencia : 'CLIENTE'}
              }
            });
            modalInstance.result.then(function (parameters) {
            });
          },          
        verSeguimientoHistorico : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.oportunidad.model.padreId,
                         referencia : 'OPORTUNIDAD' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
          
      }
    }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrado'}); 
            $scope.estados.data.push({value:'CA', descripcion: 'Cancelado'}); 
        }        
    }
	//TIPO PROCESO
      $scope.tipoProceso = {
            current: {},
            data: [],
            getData: function() {
                $scope.tipoProceso.data.push({value:'O', descripcion: 'Oportunidad'});
                $scope.tipoProceso.data.push({value:'POR', descripcion: 'Portafolio'});
                $scope.tipoProceso.data.push({value:'PRO', descripcion: 'Propuesta'});  
                    
            }
        }
      
    //VIA CONTACTO
    $scope.viaContacto = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        oportunidadHttp.getViaContacto({}, {}, function(response) {
            $scope.viaContacto.data = $filter('orderBy')(response, 'codigo');
            
            if(!$scope.oportunidad.model){$scope.viaContacto.current = $scope.viaContacto.data[0];}
            else{$scope.viaContacto.current = $filter('filter')($scope.viaContacto.data, { codigo : $scope.oportunidad.model.viaContactoId })[0];}
            
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setViaContacto : function(){
          $scope.oportunidad.model.viaContactoId=$scope.viaContacto.current.codigo;
    }}
    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            oportunidadHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.oportunidad.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.oportunidad.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.oportunidad.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.oportunidad.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }
    //CONTACTOS
    $scope.contactosOp = {
      current : {}, data:[],
      getData : function() {                  
          
               $rootScope.loadingVisible = true;
                oportunidadHttp.getContactosEmpresa({}, { terceroId: $scope.oportunidad.model.terceroId }, function(response) {   
                    
             $scope.contactosOp.data = response ;
                                                                                                                     
              if($scope.oportunidad.model ){ 
                    
                    $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.oportunidad.model.contactoId })[0];
               }
                else{
                    $scope.contactosOp.current=$scope.contactosOp.data[0];                     
                }
                                                                                                                   
                                                                                                                       
                    
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
            });
          
      },
      'setContacto' : function() {
        
          $scope.oportunidad.model.contactoId=$scope.contactosOp.current.id;
      }
    }
    
    
      //PRODUCTOS
    $scope.productosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            oportunidadHttp.getProductos({}, {}, function(response) {   
                
                $scope.productosOp.data = response ;                 
                $scope.productosOp.current=$scope.productosOp.data[0];
                $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    }
    
    
    $scope.productoOportunidad = {
    model : {  
            productoId: 0,
            oportunidadId :0,
            total :0,
            valor :0,
            cantidad :0,
            valorIva :0,
            producto: '',
            comentario: ''
			
		},
      current : {},
      data:[],
      getData : function() {
          
          
          if(oportunidad){
              
            if($scope.oportunidad.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              oportunidadHttp.getProductosOportunidad({}, { oportunidadId: $scope.oportunidad.model.id }, function(response) {
                  
                  $scope.productoOportunidad.data = response;
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },        
        add: function(){           
            
              
            if($scope.oportunidad.model.id!=0){
                if($scope.productosOp.current){
                    $rootScope.loadingVisible = true;                    
                    
                    var data ={
                         productoId: $scope.productosOp.current.id,
                         oportunidadId :$scope.oportunidad.model.id,
                         total :  $scope.productosOp.current.valor,
                         valor :$scope.productosOp.current.valor,
                         cantidad :1,
                         valorIva :0,
                         producto: $scope.productosOp.current.nombre,
                         comentario: ''
                        
                    } ;
                      oportunidadHttp.addProductoOportunidad({}, data, function(response) {

                          $scope.productoOportunidad.getData();
                          $rootScope.loadingVisible = false;
                          message.show("success", "Producto agregado satisfactoriamente");	
                      }, function(faild) {
                          $rootScope.loadingVisible = false;
                          message.show("error", faild.Message);
                      });    
                }
                else{message.show("warning", "Debe seleccionar un producto");}

            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
          
        }
        ,        
        update: function(data){
            
            
            if($scope.oportunidad.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              oportunidadHttp.editProductoOportunidad({}, data, function(response) {
                  
                  $scope.productoOportunidad.getData();
                  $rootScope.loadingVisible = false;
                  message.show("success", "Producto actualizado satisfactoriamente");	
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
            else{
                message.show("warning", "Debe guardar el encabezado");
            }
        }
        ,delete: function(data){            
            
            $rootScope.loadingVisible = true;          
            oportunidadHttp.removeProductoOportunidad({}, {oportunidadId: data.oportunidadId,productoId: data.productoId }, function(response){
             $scope.productoOportunidad.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
        }
    }
	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();
	$scope.tipoProceso.getData();	
    $scope.viaContacto.getData();
    $scope.tercerosOp.getData();   
    $scope.productosOp.getData();
      
   
  
	
	//CARGAMOS LOS DATOS DEL OPORTUNIDAD	
	
	if(oportunidad.id==0){
        
      $scope.estados.current=$scope.estados.data[0]; 
      $scope.tipoProceso.current= $filter('filter')($scope.tipoProceso.data, {value : oportunidad.tipo})[0]; 
      $scope.viaContacto.current=  $scope.viaContacto[0];   
      $scope.oportunidad.model.tipo=oportunidad.tipo;             
        
    }
    else{   
        $rootScope.loadingVisible = true;
		
            oportunidadHttp.read({},$state.params.oportunidad, function (data) { 
                
            $scope.oportunidad.model = data;	
            $scope.btnAdjunto=true;
            $scope.divProductosOportunidad=true;
               
			if($scope.oportunidad.model.fechaReg){$scope.oportunidad.fechaReg.value=new Date(parseFloat($scope.oportunidad.model.fechaReg));}    
			if($scope.oportunidad.model.fechaAct){$scope.oportunidad.fechaAct.value=new Date(parseFloat($scope.oportunidad.model.fechaAct));}  
            
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.oportunidad.model.terceroId })[0];  
            $scope.contactosOp.getData();
            $scope.productoOportunidad.getData();                
            $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.oportunidad.model.contactoId })[0];    
                
			//$scope.estados.current=$scope.oportunidad.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.oportunidad.model.estado})[0];
            $scope.tipoProceso.current = $filter('filter')($scope.tipoProceso.data, {value : $scope.oportunidad.model.tipo})[0];
            $scope.viaContacto.current = $filter('filter')($scope.viaContacto.data, {codigo : $scope.oportunidad.model.viaContactoId})[0];
                
             if  ($scope.oportunidad.model.padreId!=0){
                 
                 $scope.btnHistorico=true;
             }     
                
                    
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      

      
        
	
    
    
  }
})();