/**=========================================================
 * Module: app.producto.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.producto')
    .controller('productoController', productoController);

  productoController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'productoHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function productoController($scope, $filter, $state, LDataSource, productoHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var producto = $state.params.producto;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.producto = {
		model : {
              id: 0,                            
			  nombre: '',
              descripcion: '',
              valor:0,			
              estado: '',
              fechaAct: '',
              usuarioAct: '',
              fechaReg: '',
              usuarioReg: ''			
		}, 
        fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.producto.fechaInicial.isOpen = true;
        }
      },  
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.producto.fechaFinal.isOpen = true;
        }
      },
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.producto.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.producto.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.producto', params : { filters : {procesoSeleccionado:producto.tipo}, data : []} });
        $state.go('app.producto');
          
      },
      save : function() {		  
       
		  
        
          		if($scope.producto.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
				if($scope.producto.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}				
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.producto.model.estado= $scope.estados.current.value;}
				
			 
			//INSERTAR
            if($scope.producto.model.id==0){
				
				$rootScope.loadingVisible = true;
				productoHttp.save({}, $scope.producto.model, function (data) { 
					
						productoHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.producto.model=data;
							producto.id=$scope.producto.model.id; 
							if($scope.producto.model.fechaReg){$scope.producto.fechaReg.value=new Date(parseFloat($scope.producto.model.fechaReg));}    
							if($scope.producto.model.fechaAct){$scope.producto.fechaAct.value=new Date(parseFloat($scope.producto.model.fechaAct));} 
                            
                                 
								
							message.show("success", "producto creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.producto.model.id){
					$state.go('app.producto');
				}else{
					
					productoHttp.update({}, $scope.producto.model, function (data) {                           
					  $rootScope.loadingVisible = false;
					       $scope.producto.model=data;
						if($scope.producto.model.fechaReg){$scope.producto.fechaReg.value=new Date(parseFloat($scope.producto.model.fechaReg));}    
						if($scope.producto.model.fechaAct){$scope.producto.fechaAct.value=new Date(parseFloat($scope.producto.model.fechaAct));}   		
                                     
					   message.show("success", "producto actualizado satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		}
      }
        
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'A', descripcion: 'Activo'});
            $scope.estados.data.push({value:'I', descripcion: 'Inactivo'});
            $scope.estados.data.push({value:'E', descripcion: 'Eliminado'}); 
        }       
    }      
    //Estados
    $scope.estados.getData();	
	//CARGAMOS LOS DATOS DEL producto	
	
	if(producto.id==0){
      $scope.estados.current=$scope.estados.data[0]; 
       
        
    }
    else{   
        $rootScope.loadingVisible = true;       
        
		
        productoHttp.read({},$state.params.producto, function (data) { 
        $scope.producto.model = data;		

        if($scope.producto.model.fechaReg){$scope.producto.fechaReg.value=new Date(parseFloat($scope.producto.model.fechaReg));}    
        if($scope.producto.model.fechaAct){$scope.producto.fechaAct.value=new Date(parseFloat($scope.producto.model.fechaAct));}              
       
        //$scope.estados.current=$scope.producto.model.estado;	            
        $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.producto.model.estado})[0];
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
    
      

	
    
    
  }
})();