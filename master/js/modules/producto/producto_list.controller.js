/**=========================================================
 * Module: app.producto.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.producto')
    .controller('productoListController', productoListController);

  productoListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'productoHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function productoListController($scope, $filter, $state, ngDialog, tpl, productoHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {   
	
		
	$scope.ESTADO_ACTIVO = 'A';
    $scope.ESTADO_INACTIVO = 'I';
    $scope.ESTADO_ELIMINADO= 'E';	
    	

    $scope.productos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreproductos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.productos.selectedAll = !$scope.productos.selectedAll; 
        for (var key in $scope.productos.selectedItems) {
          $scope.productos.selectedItems[key].check = $scope.productos.selectedAll;
        }
      },
      add : function() {  
              var cont = {
                      id: 0,                    
                      nombre: '',
                      descripcion: '',
                      valor: 0,
                      estado: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.producto_add', params : { producto: cont } });
            $state.go('app.producto_add');  
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.producto_edit', params : { producto: item } });
        $state.go('app.producto_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            productoHttp.remove({}, { id: id }, function(response) {
                $scope.productos.getData();
                message.show("success", "producto eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.productos.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    productoHttp.remove({}, { id: id }, function(response) {
                        $scope.productos.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.productos.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.productos.filterText,
          "precision": false
        }];
        $scope.productos.selectedItems = $filter('arrayFilter')($scope.productos.dataSource, paramFilter);
        $scope.productos.paginations.totalItems = $scope.productos.selectedItems.length;
        $scope.productos.paginations.currentPage = 1;
        $scope.productos.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.productos.paginations.currentPage == 1 ) ? 0 : ($scope.productos.paginations.currentPage * $scope.productos.paginations.itemsPerPage) - $scope.productos.paginations.itemsPerPage;
        $scope.productos.data = $scope.productos.selectedItems.slice(firstItem , $scope.productos.paginations.currentPage * $scope.productos.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.productos.data = [];
        $scope.productos.loading = true;
        $scope.productos.noData = false;      
         productoHttp.getList(function(response) {
          $scope.productos.selectedItems = response;
          $scope.productos.dataSource = response;
          for(var i=0; i<$scope.productos.dataSource.length; i++){
            $scope.productos.nombreproductos.push({id: i, nombre: $scope.productos.dataSource[i]});
          }
          $scope.productos.paginations.totalItems = $scope.productos.selectedItems.length;
          $scope.productos.paginations.currentPage = 1;
          $scope.productos.changePage();
          $scope.productos.loading = false;
          ($scope.productos.dataSource.length < 1) ? $scope.productos.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }  
    
	//CARGAMOS DATA    
    $scope.productos.getData();  
        
 
	}
  
  
  })();