/**=========================================================
 * Module: app.reporte.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.reporte')
    .service('reporteHttp', reporteHttp);

  reporteHttp.$inject = ['$resource', 'END_POINT'];


  function reporteHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            modulo : '@modulo'           
          },
          'url' : END_POINT + '/General.svc/reporte/:modulo'
          
      }  
    };
    return $resource( END_POINT + '/General.svc/reporte', {}, actions, {}); 
  }

})();