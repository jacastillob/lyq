/**=========================================================
 * Module: app.reporte.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.reporte')
    .controller('reporteListController', reporteListController);

  reporteListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'reporteHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function reporteListController($scope, $filter, $state, ngDialog, tpl, reporteHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
        

    var moduloSeleccionado= $state.params.filters.moduloSeleccionado;
	
		
	$scope.ESTADO_PROCESO = 'P';
    $scope.ESTADO_ANULADO = 'A';
    $scope.ESTADO_CERRADO= 'C';	
    $scope.ESTADO_CANCELADO= 'CA';		

    $scope.reportes = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrereportes:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.reportes.selectedAll = !$scope.reportes.selectedAll; 
        for (var key in $scope.reportes.selectedItems) {
          $scope.reportes.selectedItems[key].check = $scope.reportes.selectedAll;
        }
      },
      add : function() {
        
        if($scope.modulo.current.value){        
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      tipo: $scope.modulo.current.value,
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.reporte_add', params : { reporte: oport } });
            $state.go('app.reporte_add');
          
          }
          else{
              
              message.show("warning", "Debe seleccionar el tipo de proceso!!")
          }
          
      },
      edit : function(item) {
          
          //ngDialog.open(item.url);  
          //alert("hola");
          
          window.open(item.url);
        //parametersOfState.set({ name : 'app.reporte_edit', params : { reporte: item } });
        //$state.go('app.reporte_edit');
          
          
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            reporteHttp.remove({}, { id: id }, function(response) {
                $scope.reportes.getData();
                message.show("success", "reporte eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.reportes.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    reporteHttp.remove({}, { id: id }, function(response) {
                        $scope.reportes.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.reportes.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.reportes.filterText,
          "precision": false
        }];
        $scope.reportes.selectedItems = $filter('arrayFilter')($scope.reportes.dataSource, paramFilter);
        $scope.reportes.paginations.totalItems = $scope.reportes.selectedItems.length;
        $scope.reportes.paginations.currentPage = 1;
        $scope.reportes.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.reportes.paginations.currentPage == 1 ) ? 0 : ($scope.reportes.paginations.currentPage * $scope.reportes.paginations.itemsPerPage) - $scope.reportes.paginations.itemsPerPage;
        $scope.reportes.data = $scope.reportes.selectedItems.slice(firstItem , $scope.reportes.paginations.currentPage * $scope.reportes.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.reportes.data = [];
        $scope.reportes.loading = true;
        $scope.reportes.noData = false;
        var parametros= {
            "modulo":$scope.modulo.current.value                
        };
          
        reporteHttp.getList({}, parametros,function(response) {
          $scope.reportes.selectedItems = response;
          $scope.reportes.dataSource = response;
          for(var i=0; i<$scope.reportes.dataSource.length; i++){
            $scope.reportes.nombrereportes.push({id: i, nombre: $scope.reportes.dataSource[i]});
          }
          $scope.reportes.paginations.totalItems = $scope.reportes.selectedItems.length;
          $scope.reportes.paginations.currentPage = 1;
          $scope.reportes.changePage();
          $scope.reportes.loading = false;
          ($scope.reportes.dataSource.length < 1) ? $scope.reportes.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
      //CARGAMOS PROCESOS
    $scope.modulo = {
        current : {}, data:[],
        getData : function() {
            $scope.modulo.data.push({value:'CL', descripcion: 'Cliente'});                                
            $scope.modulo.current =$scope.modulo.data[0];
        }        
    }	   
    
	//CARGAMOS LOS TIPOS DE PROCESO
    $scope.modulo.getData();
    $scope.reportes.getData();  
        
        if(moduloSeleccionado){

            $scope.modulo.current = $filter('filter')($scope.modulo.data, {value : moduloSeleccionado})[0];        
            $scope.reportes.getData();        
        }
        
	}
  
  
  })();