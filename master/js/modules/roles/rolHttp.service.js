/**=========================================================
 * Module: app.report.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.rol')
    .service('roltHttp', roltHttp);

  roltHttp.$inject = ['$resource', 'END_POINT'];


  function roltHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/General.svc/rol'
      },
      'editRol' : {
        'method' : 'PUT',
        'url' : END_POINT + '/General.svc/rol'
      },
      'newrol' : {
        'method' : 'POST',
        'url' : END_POINT + '/General.svc/rol'
      },
      'removeRol': {
        'method':'DELETE',
        'params' : {
           id : '@id'
        },
        'url' : END_POINT + '/General.svc/rol/:id'
      }
    };

    return $resource(END_POINT + '/General.svc/rol', paramDefault, actions);
  }

})();