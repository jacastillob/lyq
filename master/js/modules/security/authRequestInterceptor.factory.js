(function() {
  'use strict';
  angular
    .module('app.security')
    .service('authRequestInterceptor', authRequestInterceptor);

  authRequestInterceptor.$inject = ['tokenManager'];


  function authRequestInterceptor(tokenManager) {
    return {
      request: function (config) {

        if (config.headers.Authorization == undefined) {
          config.headers = config.headers || {};
          if ( tokenManager.get() !== undefined || tokenManager.get() !== null ) {
            config.headers.Authorization = tokenManager.get();        
          }
        }

        return config;
      }
    }
  }

})();