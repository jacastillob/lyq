/**=========================================================
 * Module: app.cuadro4.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.security')
    .controller('RecoverController', RecoverController);

  RecoverController.$inject = ['$scope', '$rootScope', '$state', 'securityHttp','message'];


  function RecoverController($scope, $rootScope, $state, securityHttp, message) {

    $scope.recover = {
      model : {
        email : ''
      },
      resetPassword : function() {
        $rootScope.loadingVisible = true;
        securityHttp.resetPassword({},{ email : $scope.recover.model.email}, function() {
          message.show("info","Hemos enviado un correo electronico para continuar con el proceso");
          $state.go('page.login');
          $rootScope.loadingVisible = false;
        }, function(faild) {
          $state.go('page.login');
          message.show("info","Ocurrio un error al intentar enviar el mensaje de confirmación.");
          $rootScope.loadingVisible = false;
        });
      }
    }


  }

})();