(function() {
    'use strict';

    angular
        .module('app.security')
        .config(securityConfig);

    securityConfig.$inject = ['$httpProvider'];
    function securityConfig($httpProvider) {
      
      $httpProvider.interceptors.push('authRequestInterceptor');
      $httpProvider.interceptors.push('authResponseInterceptor');
      
    }

})();