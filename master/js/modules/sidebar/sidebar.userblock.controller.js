(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .controller('UserBlockController', UserBlockController);

    UserBlockController.$inject = ['$scope','$rootScope', '$modal'];
    function UserBlockController($scope,$rootScope,$modal) {

        activate();

        ////////////////

        function activate() {
          $rootScope.user = {
            name:     'John',
            job:      'ng-developer',
            picture:  'app/img/user/02.jpg'
          };

          // Hides/show user avatar on sidebar
          $rootScope.toggleUserBlock = function(){
            $rootScope.$broadcast('toggleUserBlock');
          };

          $rootScope.userBlockVisible = true;
          
          $rootScope.$on('toggleUserBlock', function(/*event, args*/) {

            $rootScope.userBlockVisible = ! $rootScope.userBlockVisible;
            
          });
        }
      $rootScope.changeImg = function(){
        var modalInstance = $modal.open({
          templateUrl: 'app/views/partials/user-photo.html',
          controller: 'userPhotoController',
          size: 'lg',
          resolve: {
            parameters: function () { //return parameter; 
            }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }
      
      
      $rootScope.myImage='';
    $rootScope.myCroppedImage='';
      
    $rootScope.setEditing=function(varEditing) {
      $scope.editing =varEditing;
    };
      
     $rootScope.handleFileSelect=function() {
    
      var file=event.target.files[0];
      var reader = new FileReader();
      reader.onload = function (event) {
        $rootScope.$apply(function($scope){
          debugger
          $scope.myImage=event.target.result;
        
          
        });
      };
         $scope.editing = true;
      reader.readAsDataURL(file);
    };
      
    }
})();
