/**=========================================================
 * Module: app.report.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.sidebar')
    .service('sidebarHttp', sidebarHttp);

  sidebarHttp.$inject = ['$resource', 'END_POINT'];


  function sidebarHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      'getMenu' : {
        'method' : 'GET',
        'isArray' : true,
        'url' : END_POINT + '/Administracion.svc/menu'
      }
    };

    return $resource(END_POINT + '/Administracion.svc/autmenuhUser', paramDefault, actions);
  }

})();