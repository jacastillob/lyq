/**=========================================================
 * Module: modals.js
 * Provides a simple way to implement bootstrap modals from templates
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.usuario')
        .controller('ModalGmapController', ModalGmapController);

    ModalGmapController.$inject =     
    ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'usuarioHttp',   'parameters', 'message', 'parametersOfState','$rootScope', 'REGULAR_EXPRESION','$timeout'];
      
    function ModalGmapController($scope, $filter, $state, $modalInstance, LDataSource, usuarioHttp, parameters, message, parametersOfState,$rootScope, REGULAR_EXPRESION,$timeout) { 
        
        var  objUbi = parameters.objUbicacion;
        var registroUbicacion = parameters.registroUbicacion;   
        var vm = this;       
        
        $scope.registroUbicacionModal={               
                     
            model : {
                id:0,
                contratoId:0,
                funcionarioId:0,
                fecha: '',
                fechaInicio: '',
                tipo: '',
                comentario: '',
                longitude : 0,
                latitude : 0,
                accuracy : 0
		},             
            
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          save : function() {
                             
                if($scope.tipos.current.value==''){message.show("warning", "Tipo registro requerido");return;}
                else{$scope.registroUbicacionModal.model.tipo= $scope.tipos.current.value;}   
                $scope.registroUbicacionModal.model.fechaInicio= Date.parse(new Date($scope.registroUbicacionModal.model.fechaInicio));
               
                    
                $rootScope.loadingVisible = true;
				usuarioHttp.addRegistroUbicacion({}, $scope.registroUbicacionModal.model, function (data) { 
					
						usuarioHttp.getRegistroUbicacion({},{ id : data.id}, function (data) {                           
							$scope.registroUbicacionModal.model=data;                         
								
							message.show("success", "Ubicación Registrada satisfactoriamente!!");
                             $modalInstance.close();                       
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});              
          } 
        }
        
    //TIPOS
    $scope.tipos = {
        current : {}, data:[],
        getData : function() {
            $scope.tipos.data.push({value:'', descripcion: 'Seleccione'});
            $scope.tipos.data.push({value:'E', descripcion: 'Entrada'});
            $scope.tipos.data.push({value:'S', descripcion: 'Salida'});
            $scope.tipos.current=$scope.tipos.data[0];
        }        
    }   
    
   //INICIALIZAMOS EL MAPA   
    
    //CARGAMOS LISTADO
    $scope.tipos.getData();        
    if(registroUbicacion.id==0){   
       
            $scope.registroUbicacionModal.model.longitude=objUbi.long;
            $scope.registroUbicacionModal.model.latitude=objUbi.lat;
            $scope.registroUbicacionModal.model.accuracy=objUbi.accu;
            $scope.registroUbicacionModal.model.contratoId=registroUbicacion.contratoId;
            $scope.registroUbicacionModal.model.funcionarioId=registroUbicacion.funcionarioId;  
            $scope.registroUbicacionModal.model.fechaInicio=new Date();             
			
    }
    else{ 
         
         usuarioHttp.getRegistroUbicacion({}, { id : registroUbicacion.id} , function (data) { 
            
            $scope.registroUbicacionModal.model = data;	             
            $scope.registroUbicacionModal.model.id=registroUbicacion.id;
            $scope.tipos.current = $filter('filter')($scope.tipos.data, {value : $scope.registroUbicacionModal.model.tipo})[0];
            
            $scope.registroUbicacionModal.model.fecha=new Date(parseFloat(data.fecha));
            $scope.registroUbicacionModal.model.fechaInicio=new Date(parseFloat(data.fechaInicio));  
             
             setUbicacion($scope.registroUbicacionModal.model.latitude,$scope.registroUbicacionModal.model.longitude);
                
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });  
           
      
    }   
      
}
    
    
    


    })();
