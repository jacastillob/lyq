/**=========================================================
 * Module: app.forma30.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.usuario')
    .service('usuarioHttp', usuarioHttp);

  usuarioHttp.$inject = ['$resource', 'END_POINT'];


  function usuarioHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      getRegistrosUbicacion : {
        method : 'POST',
        isArray : true
      },
      getRegistroUbicacion : {
        method : 'GET',
        url : END_POINT + '/Cliente.svc/registroUbicacion/:id'
      },      
      addRegistroUbicacion: {
        method:'POST',
        url : END_POINT + '/Cliente.svc/addRegistroUbicacion'
      },
      editRegistroUbicacion: {
        method:'PUT',
        url : END_POINT + '/Cliente.svc/editRegistroUbicacion'
      },getProcesosAsociado: {
          method : 'GET',
          isArray : true,
          params : {
          proceso : '@proceso'           
          },
          'url' : END_POINT + '/General.svc/procesoAsociado/:proceso'
      }, 
        'getResponsablesProceso': {
          'method' : 'POST',
          'isArray' : true,          
          'url' : END_POINT + '/General.svc/procesoAsociado/responsable'
        }
      
        
    };

    return $resource(END_POINT + '/Cliente.svc/registroUbicacion', paramDefault, actions);
  }

})();
