/**=========================================================
 * Module: flot.js
 * Initializes the Flot chart plugin and handles data refresh
 =========================================================*/
(function() {
  'use strict';

  angular
    .module('app.utils')
    .directive('validation', validation);


  validation.$inject = ['$parse'];


  function validation ($parse) {

   return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, element, attr, ctrl) {


        angular.element(element).on("keypress", function(e) {
          var pattern = new RegExp(scope.$eval(attr['validation']));
          var newInput = this.value + String.fromCharCode(e.charCode);
          if (pattern.test(newInput) == false) {
            e.preventDefault();
          };
        });


        angular.element(element).on("paste", function(e) {
          setTimeout(function () {
            var pattern = new RegExp(scope.$eval(attr['validation']));
            var newText = angular.element(element).val();
            if (pattern.test(newText) == false) {
              e.preventDefault();
                var model = $parse(attr.ngModel);
                console.log(model(scope));
                model.assign(scope,'');
                scope.$apply();
            };
          }, 5);
        });

      }
    };
  }
})();